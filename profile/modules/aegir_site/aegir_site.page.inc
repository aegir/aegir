<?php

declare(strict_types=1);

/**
 * @file
 * Contains aegir_site.page.inc.
 *
 * Page callback for Ægir Site entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ægir site templates.
 *
 * Default template: aegir_site.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_aegir_site(array &$variables): void {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
