<?php

declare(strict_types=1);

namespace Drupal\aegir_site\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;

/**
 * Interface for Ægir Site type entities.
 */
interface SiteTypeInterface extends AegirEntityTypeInterface {}
