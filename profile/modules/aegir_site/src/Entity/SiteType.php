<?php

declare(strict_types=1);

namespace Drupal\aegir_site\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeBase;
use Drupal\aegir_site\Entity\SiteTypeInterface;

/**
 * Defines the Ægir Site type configuration entity.
 *
 * @ConfigEntityType(
 *   id               = "aegir_site_type",
 *   label            = @Translation("Site type"),
 *   label_collection = @Translation("Site types"),
 *   label_singular   = @Translation("site type"),
 *   label_plural     = @Translation("site types"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count site type",
 *     plural   = "@count site types",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityTypeListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "delete" = "Drupal\aegir_api\Form\AegirEntityTypeDeleteConfirmForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix    = "aegir_site_type",
 *   admin_permission = "administer aegir site types",
 *   bundle_of        = "aegir_site",
 *   entity_keys = {
 *     "id"     = "id",
 *     "label"  = "label",
 *     "uuid"   = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   links = {
 *     "canonical"    = "/admin/aegir/sites/types/{aegir_site_type}",
 *     "add-form"     = "/admin/aegir/sites/types/add",
 *     "edit-form"    = "/admin/aegir/sites/types/{aegir_site_type}/edit",
 *     "delete-form"  = "/admin/aegir/sites/types/{aegir_site_type}/delete",
 *     "collection"   = "/admin/aegir/sites/types",
 *   },
 * )
 */
class SiteType extends AegirEntityTypeBase implements SiteTypeInterface {}
