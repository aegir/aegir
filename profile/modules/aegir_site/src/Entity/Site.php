<?php

declare(strict_types=1);

namespace Drupal\aegir_site\Entity;

use Drupal\aegir_api\Entity\AegirEntityBase;
use Drupal\aegir_site\Entity\SiteInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Ægir Site entity.
 *
 * @ingroup aegir_site
 *
 * @ContentEntityType(
 *   id               = "aegir_site",
 *   label            = @Translation("Site"),
 *   label_collection = @Translation("Sites"),
 *   label_singular   = @Translation("site"),
 *   label_plural     = @Translation("sites"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count site",
 *     plural   = "@count sites",
 *   ),
 *   bundle_label = @Translation("Site type"),
 *   handlers     = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-delete" = "Drupal\Core\Entity\Form\RevisionDeleteForm",
 *       "revision-revert" = "Drupal\Core\Entity\Form\RevisionRevertForm",
 *     },
 *     "access"         = "Drupal\aegir_api\Entity\AegirEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html"     = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     },
 *     "translation"  = "Drupal\content_translation\ContentTranslationHandler",
 *     "views_data"   = "Drupal\views\EntityViewsData",
 *   },
 *   base_table       = "aegir_site",
 *   data_table       = "aegir_site_field_data",
 *   revision_table   = "aegir_site_revision",
 *   revision_data_table = "aegir_site_field_revision",
 *   translatable     = true,
 *   show_revision_ui = true,
 *   admin_permission = "administer aegir site entities",
 *   entity_keys      = {
 *     "id"         = "id",
 *     "revision"   = "revision_id",
 *     "bundle"     = "type",
 *     "label"      = "name",
 *     "uuid"       = "uuid",
 *     "owner"      = "user_id",
 *     "langcode"   = "langcode",
 *     "published"  = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"        = "/admin/aegir/sites/{aegir_site}",
 *     "add-page"         = "/admin/aegir/sites/add",
 *     "add-form"         = "/admin/aegir/sites/add/{aegir_site_type}",
 *     "edit-form"        = "/admin/aegir/sites/{aegir_site}/edit",
 *     "delete-form"      = "/admin/aegir/sites/{aegir_site}/delete",
 *     "version-history"  = "/admin/aegir/sites/{aegir_site}/revisions",
 *     "revision"         = "/admin/aegir/sites/{aegir_site}/revisions/{aegir_site_revision}/view",
 *     "revision-revert-form" = "/admin/aegir/sites/{aegir_site}/revisions/{aegir_site_revision}/revert",
 *     "revision-delete-form" = "/admin/aegir/sites/{aegir_site}/revisions/{aegir_site_revision}/delete",
 *     "collection"       = "/admin/aegir/sites",
 *   },
 *   bundle_entity_type   = "aegir_site_type",
 *   field_ui_base_route  = "entity.aegir_site_type.canonical",
 * )
 */
class Site extends AegirEntityBase implements SiteInterface {

  public static function baseFieldDefinitions(EntityTypeInterface $entityType) {

    $fields = parent::baseFieldDefinitions($entityType);

    $fields[$entityType->getKey('label')]
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
        'region' => 'content',
      ]);

    return $fields;

  }

}
