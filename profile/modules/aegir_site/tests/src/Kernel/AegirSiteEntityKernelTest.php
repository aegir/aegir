<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_site\Kernel;

use Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ægir Site entity kernel tests.
 *
 * @group aegir
 *
 * @group aegir_site
 */
class AegirSiteEntityKernelTest extends AegirEntityBasicKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_site',
    'aegir_test_site',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_site');

  }

  /**
   * Data provider for AegirEntityBasicKernelTestBase::testAegirEntityBasics().
   *
   * @return array
   *
   * @see \Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase::testAegirEntityBasics()
   */
  public static function aegirEntityBasicsProvider(): array {

    return [
      [
        'entityTypeId'  => 'aegir_site',
        'managerRole'   => 'aegir_site_manager',

        'values' => [
          // Use one of the bundles exported to the 'aegir_test_site'
          // module.
          'type'    => 'test_site_type_1',
          'name'    => 'TEST_SITE_A',
          'status'  => 1,
        ],
      ],
    ];

  }

}
