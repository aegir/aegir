@sites @access @api
Feature: Access to Aegir sites and types
  In order to define sites that can be combined into applications,
  as a site manager,
  I need to be able to access Aegir site entities and types.

  Scenario: Anonymous users should not have access to sites configuration.
    Given I am not logged in
     When I am on "admin/aegir"
     Then I should be on "user/login"
     Then I should see "Forgot your password?"

  Scenario: Authenticated users should not have access to sites configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Sites"

  Scenario: Site managers should have access to sites configuration.
    Given I am logged in as a "Site manager"
     When I am on "admin/aegir"
     Then I should see the link "Sites"
      And I should see the text "Ægir site entities and bundles"
     When I click "Sites" in the "content" region
     Then I should be on "admin/aegir/sites"
      And I should see the heading "Sites" in the "header" region
      And I should see the link "Add Ægir site"
      And I should see the link "Site types"
     When I click "Site types"
     Then I should be on "admin/aegir/sites/types"
      And I should see the heading "Site types" in the "header" region
      And I should see the link "Add Ægir site type"
