@sites @types @api
Feature: Create and manage Aegir site types
  In order to define site types that can be combined into applications,
  as a site manager,
  I want to be able to manage Aegir site bundles.

  Background:
    Given I am logged in as a "Site manager"
      And I am on "admin/aegir/sites/types"


  Scenario: Create site types
     When I click "Add Ægir site type"
      And I fill in "Name" with "TEST_SITE_TYPE_NEW"
      And I fill in "Machine-readable name" with "test_site_type_new"
      And I press the "Save" button
     Then I should be on "admin/aegir/sites/types"
      And I should see the success message "Created the TEST_SITE_TYPE_NEW site type."

  Scenario: Update site types
     When I click "Edit" in the "TEST_SITE_TYPE_NEW" row
     Then I should see the heading "Edit site type" in the "header" region
     When I fill in "Name" with "TEST_SITE_TYPE_CHANGED"
      And I press the "Save" button
     Then I should be on "admin/aegir/sites/types"
     Then I should see the success message "Saved the TEST_SITE_TYPE_CHANGED site type."

  Scenario: Delete site types
     Then I should see the link "TEST_SITE_TYPE_CHANGED"
     When I click "Edit" in the "TEST_SITE_TYPE_CHANGED" row
     Then I should see the heading "Edit site type" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete the site type TEST_SITE_TYPE_CHANGED?" in the "header" region
     When I press the "Delete" button
     Then I should be on "admin/aegir/sites/types"
     Then I should see the success message "The site type TEST_SITE_TYPE_CHANGED has been deleted."

