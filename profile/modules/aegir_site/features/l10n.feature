@sites @l10n @api @javascript
Feature: Translate Aegir sites
  In order to allow usage in multiple languages,
  as a site manager,
  I want to be able to translate Aegir site entities.

  Background:
    Given I am logged in as a "Site manager"

  Scenario: Enable a new language (French)
    Given I run "drush -y pm:install aegir_test_site"
      And I am logged in as an "Administrator"
      And I am on "admin/config/regional/language"
     When I click "Add language" in the "content" region
     Then I should see the heading "Add language"
     When I select "French" from "Language name"
      And I press the "Add language" button
      And I wait for the batch job to finish
     Then I should see the success message "The language French has been created and can now be used."
     When I am on "admin/config/regional/content-language"
      And I check the box "Site"
      # The "Site" form wrapper is a <details> that starts closed, so we have to
      # open it. The <summary> has role="button" which why the below works.
      And I press the "Site" button
      And I check the box "settings[aegir_site][test_site_type_1][translatable]"
      And I check the box "settings[aegir_site][test_site_type_1][fields][user_id]"
      And I check the box "settings[aegir_site][test_site_type_1][settings][language][language_alterable]"
      And I press the "Save configuration" button
     Then I should see the success message "Settings successfully updated."

  Scenario: Create English site
    Given I am on "admin/aegir/sites/add/test_site_type_1"
     When I fill in "Name" with "TEST_SITE_ENG"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_SITE_ENG site."

  Scenario: Translate site
    Given I am on "admin/aegir/sites"
     Then I should see the link "TEST_SITE_ENG" in the "content" region
     When I click "TEST_SITE_ENG"
     Then I should see the link "Translate" in the "tabs" region
     When I click "Translate"
      And I click "Add" in the "French" row
     Then I should see the heading "Traduire TEST_SITE_ENG en French"
     When I fill in "Nom" with "TEST_SITE_FR"
      And I click "Informations sur les versions"
      And for "Message du journal de version (toutes les langues)" I enter "Première message."
      And I press the "Enregistrer" button
     Then I should see the success message "Saved the TEST_SITE_FR site."
      And I am on "fr/admin/aegir/sites"

  Scenario: Update the translated site to create some revisions
    Given I am on "fr/admin/aegir/sites"
     When I click "Traduire" in the "TEST_SITE_ENG" row
      And I click "TEST_SITE_FR" in the "content" region
     Then the url should match "fr/admin/aegir/sites/[0-9]+?"
     When I click "Modifier" in the "tabs" region
     Then I should see the heading "TEST_SITE_FR [traduction French]" in the "header" region
      And I click "Informations sur les versions"
      And for "Message du journal de version (toutes les langues)" I enter "Deuxième message."
      And I press the "Enregistrer" button
     Then I should see the success message "Saved the TEST_SITE_FR site."
      And I am on "fr/admin/aegir/sites"

  Scenario: Revert a translated site revision
    Given I am on "fr/admin/aegir/sites"
     When I click "Traduire" in the "TEST_SITE_ENG" row
      And I click "TEST_SITE_FR" in the "content" region
      And I click "Versions"
     Then I should see the text "Première message." in the "content" region
      And I should see the text "Deuxième message." in the "content" region
     When I click "Rétablir" in the "Première message." row
     Then I should see the text "Êtes-vous sûr(e) de vouloir rétablir la version du" in the "header" region
     When I press "Rétablir"
     Then I should see the success message "TEST_SITE_TYPE_1 TEST_SITE_FR a été rétabli(e) à la version du"

  Scenario: Delete site translation
    Given I am on "admin/aegir/sites"
     When I click "TEST_SITE_ENG"
      And I click "Translate"
      And I click "Delete" in the "French" row
     Then I should see the heading "Êtes-vous certain de vouloir supprimer la traduction French de l'entité site TEST_SITE_FR ?"
     When I press "Supprimer la traduction French"
     Then I should see the success message "La traduction French de l'entité site TEST_SITE_FR a été supprimée."
