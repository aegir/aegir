@sites @entities @api
Feature: Create and manage Aegir sites
  In order to define sites that can be combined into applications,
  as a site manager,
  I want to be able to manage Aegir site entities.

  Background:
    Given I run "drush -y pm:install aegir_test_site"
      And I am logged in as a "Site manager"
      And I am on "admin/aegir/sites"

  Scenario: Create sites
     When I click "Add Ægir site"
     Then I should be on "admin/aegir/sites/add"
      And I should see the heading "Add site" in the "header" region
      And I should see the link "TEST_SITE_TYPE_1"
      And I should see the link "TEST_SITE_TYPE_2"
     When I click "TEST_SITE_TYPE_1" in the "content" region
     Then I should be on "admin/aegir/sites/add/test_site_type_1"
     When I fill in "Name" with "TEST_SITE_A"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_SITE_A site."
     When I click "TEST_SITE_A" in the "content" region
      And I should see the link "View" in the "tabs" region
      And I should see the link "Edit" in the "tabs" region
      And I should see the link "Revisions" in the "tabs" region
      And I should see the link "Delete" in the "tabs" region

  Scenario: Update sites
    Given I should see the link "TEST_SITE_A" in the "content" region
      And I click "TEST_SITE_A"
      And I click "Edit" in the "tabs" region
      And for "Revision log message" I enter "Test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_SITE_A site."

  Scenario: Delete sites
    Given I click "TEST_SITE_A" in the "content" region
     When I click "Delete" in the "tabs" region
     Then I should see the heading "Are you sure you want to delete the site TEST_SITE_A?" in the "header" region
     When I press the "Delete" button
     Then I should see the success message "The site TEST_SITE_A has been deleted."
      And I should be on "admin/aegir/sites"
      And I should not see the link "TEST_SITE_A"
