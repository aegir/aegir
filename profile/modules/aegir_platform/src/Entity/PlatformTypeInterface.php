<?php

declare(strict_types=1);

namespace Drupal\aegir_platform\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;

/**
 * Interface for Ægir Platform type entities.
 */
interface PlatformTypeInterface extends AegirEntityTypeInterface {}
