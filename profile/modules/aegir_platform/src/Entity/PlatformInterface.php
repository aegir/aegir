<?php

declare(strict_types=1);

namespace Drupal\aegir_platform\Entity;

use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateChildrenInterface;

/**
 * Interface for Ægir Platform entities.
 */
interface PlatformInterface extends AegirEntityInterface, AegirEntityWithAutoCreateChildrenInterface {}
