<?php

declare(strict_types=1);

namespace Drupal\aegir_platform\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeBase;
use Drupal\aegir_platform\Entity\PlatformTypeInterface;

/**
 * Defines the Ægir Platform type configuration entity.
 *
 * @ConfigEntityType(
 *   id               = "aegir_platform_type",
 *   label            = @Translation("Platform type"),
 *   label_collection = @Translation("Platform types"),
 *   label_singular   = @Translation("platform type"),
 *   label_plural     = @Translation("platform types"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count platform type",
 *     plural   = "@count platform types",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityTypeListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "delete" = "Drupal\aegir_api\Form\AegirEntityTypeDeleteConfirmForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix    = "aegir_platform_type",
 *   admin_permission = "administer aegir platform types",
 *   bundle_of        = "aegir_platform",
 *   entity_keys = {
 *     "id"     = "id",
 *     "label"  = "label",
 *     "uuid"   = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   links = {
 *     "canonical"    = "/admin/aegir/platforms/types/{aegir_platform_type}",
 *     "add-form"     = "/admin/aegir/platforms/types/add",
 *     "edit-form"    = "/admin/aegir/platforms/types/{aegir_platform_type}/edit",
 *     "delete-form"  = "/admin/aegir/platforms/types/{aegir_platform_type}/delete",
 *     "collection"   = "/admin/aegir/platforms/types",
 *   },
 * )
 */
class PlatformType extends AegirEntityTypeBase implements PlatformTypeInterface {}
