<?php

declare(strict_types=1);

namespace Drupal\aegir_platform\Entity;

use Drupal\aegir_api\Entity\AegirEntityBase;
use Drupal\aegir_platform\Entity\PlatformInterface;

/**
 * Defines the Ægir Platform entity.
 *
 * @ingroup aegir_platform
 *
 * @ContentEntityType(
 *   id               = "aegir_platform",
 *   label            = @Translation("Platform"),
 *   label_collection = @Translation("Platforms"),
 *   label_singular   = @Translation("platform"),
 *   label_plural     = @Translation("platforms"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count platform",
 *     plural   = "@count platforms",
 *   ),
 *   bundle_label = @Translation("Platform type"),
 *   handlers     = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-delete" = "Drupal\Core\Entity\Form\RevisionDeleteForm",
 *       "revision-revert" = "Drupal\Core\Entity\Form\RevisionRevertForm",
 *     },
 *     "access"         = "Drupal\aegir_api\Entity\AegirEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html"     = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     },
 *     "translation"  = "Drupal\content_translation\ContentTranslationHandler",
 *     "views_data"   = "Drupal\views\EntityViewsData",
 *   },
 *   base_table       = "aegir_platform",
 *   data_table       = "aegir_platform_field_data",
 *   revision_table   = "aegir_platform_revision",
 *   revision_data_table = "aegir_platform_field_revision",
 *   translatable     = true,
 *   show_revision_ui = true,
 *   admin_permission = "administer aegir platform entities",
 *   entity_keys      = {
 *     "id"         = "id",
 *     "revision"   = "revision_id",
 *     "bundle"     = "type",
 *     "label"      = "name",
 *     "uuid"       = "uuid",
 *     "owner"      = "user_id",
 *     "langcode"   = "langcode",
 *     "published"  = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"        = "/admin/aegir/platforms/{aegir_platform}",
 *     "add-page"         = "/admin/aegir/platforms/add",
 *     "add-form"         = "/admin/aegir/platforms/add/{aegir_platform_type}",
 *     "edit-form"        = "/admin/aegir/platforms/{aegir_platform}/edit",
 *     "delete-form"      = "/admin/aegir/platforms/{aegir_platform}/delete",
 *     "version-history"  = "/admin/aegir/platforms/{aegir_platform}/revisions",
 *     "revision"         = "/admin/aegir/platforms/{aegir_platform}/revisions/{aegir_platform_revision}/view",
 *     "revision-revert-form" = "/admin/aegir/platforms/{aegir_platform}/revisions/{aegir_platform_revision}/revert",
 *     "revision-delete-form" = "/admin/aegir/platforms/{aegir_platform}/revisions/{aegir_platform_revision}/delete",
 *     "collection"       = "/admin/aegir/platforms",
 *   },
 *   bundle_entity_type   = "aegir_platform_type",
 *   field_ui_base_route  = "entity.aegir_platform_type.canonical",
 * )
 */
class Platform extends AegirEntityBase implements PlatformInterface {}
