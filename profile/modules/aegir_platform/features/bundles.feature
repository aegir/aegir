@platforms @types @api
Feature: Create and manage Aegir platform types
  In order to define platform types that can be combined into applications,
  as a platform manager,
  I want to be able to manage Aegir platform bundles.

  Background:
    Given I am logged in as a "Platform manager"
      And I am on "admin/aegir/platforms/types"

  Scenario: Create platform types
     When I click "Add Ægir platform type"
      And I fill in "Name" with "TEST_PLATFORM_TYPE_NEW"
      And I fill in "Machine-readable name" with "test_platform_type_new"
      And I press the "Save" button
     Then I should be on "admin/aegir/platforms/types"
      And I should see the success message "Created the TEST_PLATFORM_TYPE_NEW platform type."

  Scenario: Update platform types
     When I click "Edit" in the "TEST_PLATFORM_TYPE_NEW" row
     Then I should see the heading "Edit platform type" in the "header" region
     When I fill in "Name" with "TEST_PLATFORM_TYPE_CHANGED"
      And I press the "Save" button
     Then I should be on "admin/aegir/platforms/types"
     Then I should see the success message "Saved the TEST_PLATFORM_TYPE_CHANGED platform type."

  Scenario: Delete platform types
     Then I should see the link "TEST_PLATFORM_TYPE_CHANGED"
     When I click "Edit" in the "TEST_PLATFORM_TYPE_CHANGED" row
     Then I should see the heading "Edit platform type" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete the platform type TEST_PLATFORM_TYPE_CHANGED?" in the "header" region
     When I press the "Delete" button
     Then I should be on "admin/aegir/platforms/types"
     Then I should see the success message "The platform type TEST_PLATFORM_TYPE_CHANGED has been deleted."
