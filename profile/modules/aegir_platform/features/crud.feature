@platforms @entities @api
Feature: Create and manage Aegir platforms
  In order to define platforms that can be combined into applications,
  as a platform manager,
  I want to be able to manage Aegir platform entities.

  Background:
    Given I run "drush -y pm:install aegir_test_platform"
      And I am logged in as a "Platform manager"
      And I am on "admin/aegir/platforms"

  Scenario: Create platforms
     When I click "Add Ægir platform"
     Then I should be on "admin/aegir/platforms/add"
      And I should see the heading "Add platform" in the "header" region
      And I should see the link "TEST_PLATFORM_TYPE_1"
      And I should see the link "TEST_PLATFORM_TYPE_2"
     When I click "TEST_PLATFORM_TYPE_1" in the "content" region
     Then I should be on "admin/aegir/platforms/add/test_platform_type_1"
     When I fill in "Name" with "TEST_PLATFORM_A"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_PLATFORM_A platform."
     When I click "TEST_PLATFORM_A" in the "content" region
      And I should see the link "View" in the "tabs" region
      And I should see the link "Edit" in the "tabs" region
      And I should see the link "Revisions" in the "tabs" region
      And I should see the link "Delete" in the "tabs" region

  Scenario: Update platforms
    Given I should see the link "TEST_PLATFORM_A" in the "content" region
      And I click "TEST_PLATFORM_A"
     When I click "Edit" in the "tabs" region
      And for "Revision log message" I enter "Test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_PLATFORM_A platform."

  Scenario: Delete platforms
    Given I click "TEST_PLATFORM_A" in the "content" region
     When I click "Delete" in the "tabs" region
     Then I should see the heading "Are you sure you want to delete the platform TEST_PLATFORM_A?" in the "header" region
     When I press the "Delete" button
     Then I should see the success message "The platform TEST_PLATFORM_A has been deleted."
      And I should be on "admin/aegir/platforms"
      And I should not see the link "TEST_PLATFORM_A"
