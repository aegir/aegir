@platforms @access @api
Feature: Access to Aegir platforms and types
  In order to define platforms that can be combined into applications,
  as a platform manager,
  I need to be able to access Aegir platform entities and types.

  Scenario: Anonymous users should not have access to platforms configuration.
    Given I am not logged in
     When I am on "admin/aegir"
     Then I should be on "user/login"
     Then I should see "Forgot your password?"

  Scenario: Authenticated users should not have access to platforms configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Platforms"

  Scenario: Platform managers should have access to platforms configuration.
    Given I am logged in as a "Platform manager"
     When I am on "admin/aegir"
     Then I should see the link "Platforms" in the "content" region
      And I should see the text "Ægir platform entities and bundles" in the "content" region
     When I click "Platforms" in the "content" region
     Then I should be on "admin/aegir/platforms"
      And I should see the heading "Platforms" in the "header" region
      And I should see the link "Add Ægir platform"
      And I should see the link "Platform types"
     When I click "Platform types"
     Then I should be on "admin/aegir/platforms/types"
      And I should see the heading "Platform types" in the "header" region
      And I should see the link "Add Ægir platform type"

