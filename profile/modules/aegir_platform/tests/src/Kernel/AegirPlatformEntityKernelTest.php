<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_platform\Kernel;

use Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ægir Platform entity kernel tests.
 *
 * @group aegir
 *
 * @group aegir_platform
 */
class AegirPlatformEntityKernelTest extends AegirEntityBasicKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_platform',
    'aegir_test_platform',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_platform');

  }

  /**
   * Data provider for AegirEntityBasicKernelTestBase::testAegirEntityBasics().
   *
   * @return array
   *
   * @see \Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase::testAegirEntityBasics()
   */
  public static function aegirEntityBasicsProvider(): array {

    return [
      [
        'entityTypeId'  => 'aegir_platform',
        'managerRole'   => 'aegir_platform_manager',

        'values' => [
          // Use one of the bundles exported to the 'aegir_test_platform'
          // module.
          'type'    => 'test_platform_type_1',
          'name'    => 'TEST_PLATFORM_A',
          'status'  => 1,
        ],
      ],
    ];

  }

}
