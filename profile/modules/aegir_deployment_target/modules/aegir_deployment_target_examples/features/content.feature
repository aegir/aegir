@deployment_target @content @api
Feature: Create and manage Aegir deployment targets
  In order to define deployment targets on which will run applications
  as a deployment target manager,
  I need to be able to administrate Aegir deployment targets content

  Background:
    Given I run "drush -y pm:install aegir_deployment_target_examples"
      And I am logged in as a "Deployment Target manager"

  Scenario: Manage the example deployment target.
    #Create new deployment target.
     When I am on "admin/aegir/deployment-targets"
     When I click "Add Deployment Target"
     Then I should see "Example" in the "content" region
     When I click "Example" in the "content" region
      And I fill in "Name" with "Test deployment target"
      And I press "Save"
      And I should see "Test deployment target" in the "Example" row
    #List deployment targets.
     When I go to "admin/aegir/deployment-targets"
     Then I should see "Deployment Targets"
      And I should see "Test deployment target" in the "Example" row
    #Update deployment targets.
      And I click "Edit" in the "Test deployment target" row
      And I should see "Edit Test deployment target"
      And I should see "Name"
      And I fill in "Name" with "Test deployment target updated"
      And I press "Save"
      And I should see "Saved the Test Deployment Target updated deployment target."
    #Delete deployment targets.
     When I go to "admin/aegir/deployment-targets"
      And I click "Delete" in the "Test deployment target updated" row
      And I should see "Are you sure you want to delete the deployment target Test deployment target updated?"
      And I press "Delete"
      And I should see "Deployment Targets"
     Then I should see the success message "The deployment target Test deployment target updated has been deleted."
