<?php

declare(strict_types=1);

namespace Drupal\aegir_deployment_target\Form;

use Drupal\aegir_api\Form\AegirEntityTypeForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Deployment Target entity type form.
 */
class DeploymentTargetTypeForm extends AegirEntityTypeForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form, FormStateInterface $form_state,
  ): array {

    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\aegir_deployment_target\Entity\DeploymentTargetTypeInterface */
    $entity = $this->getEntity();

    /** @var \Drupal\Core\Entity\EntityTypeInterface|null */
    $entityType = $this->entityTypeManager->getDefinition(
      $entity->getEntityTypeId(),
    );

    $form['description'] = [
      '#type'           => 'textarea',
      '#title'          => $this->t('Description'),
      '#default_value'  => $entity->getDescription(),
      '#description'    => $this->t('Description for this %entityType.', [
        '%entityType' => $entityType->getSingularLabel(),
      ]),
    ];

    return $form;

  }

}
