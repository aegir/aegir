<?php

declare(strict_types=1);

namespace Drupal\aegir_deployment_target\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeBase;
use Drupal\aegir_deployment_target\Entity\DeploymentTargetTypeInterface;

/**
 * Defines the Ægir Deployment Target type configuration entity.
 *
 * @ConfigEntityType(
 *   id               = "aegir_deployment_target_type",
 *   label            = @Translation("Deployment Target type"),
 *   label_collection = @Translation("Deployment Target types"),
 *   label_singular   = @Translation("deployment target type"),
 *   label_plural     = @Translation("deployment target types"),
 *   label_count      = @PluralTranslation(
 *     singular   = "@count deployment target type",
 *     plural     = "@count deployment target types",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_deployment_target\Entity\DeploymentTargetTypeListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_deployment_target\Form\DeploymentTargetTypeForm",
 *       "edit"   = "Drupal\aegir_deployment_target\Form\DeploymentTargetTypeForm",
 *       "delete" = "Drupal\aegir_api\Form\AegirEntityTypeDeleteConfirmForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix    = "aegir_deployment_target_type",
 *   admin_permission = "administer aegir deployment target types",
 *   bundle_of        = "aegir_deployment_target",
 *   entity_keys      = {
 *     "id"     = "id",
 *     "label"  = "label",
 *     "uuid"   = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description",
 *   },
 *   links = {
 *     "canonical"    = "/admin/aegir/deployment-targets/types/{aegir_deployment_target_type}",
 *     "add-form"     = "/admin/aegir/deployment-targets/types/add",
 *     "edit-form"    = "/admin/aegir/deployment-targets/types/{aegir_deployment_target_type}/edit",
 *     "delete-form"  = "/admin/aegir/deployment-targets/types/{aegir_deployment_target_type}/delete",
 *     "collection"   = "/admin/aegir/deployment-targets/types",
 *   },
 * )
 */
class DeploymentTargetType extends AegirEntityTypeBase implements DeploymentTargetTypeInterface {

  /**
   * The deployment target type description.
   *
   * @var string
   */
  protected string $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description): void {
    $this->description = $description;
  }

}
