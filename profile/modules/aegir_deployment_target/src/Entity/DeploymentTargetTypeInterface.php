<?php

declare(strict_types=1);

namespace Drupal\aegir_deployment_target\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;

/**
 * Interface for Ægir Deployment Target type configuration entities.
 */
interface DeploymentTargetTypeInterface extends AegirEntityTypeInterface {

  /**
   * Get the description of this Deployment Target type.
   *
   * @return string
   */
  public function getDescription(): string;

  /**
   * Set the description of this Deployment Target type.
   *
   * @param string $description
   */
  public function setDescription(string $description): void;

}
