<?php

declare(strict_types=1);

namespace Drupal\aegir_deployment_target\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Ægir Deployment Target type entity list builder.
 */
class DeploymentTargetTypeListBuilder extends AegirEntityTypeListBuilder {

  /**
   * Adds the description column and sets a provided value.
   *
   * @param array $row
   *   Existing/parent row.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $description
   *   The value to set as the description.
   *
   * @return array
   *   The new row.
   */
  protected function addDescriptionColumn(
    array $row, TranslatableMarkup|string $description,
  ): array {

    $newValues = [];

    foreach ($row as $key => $value) {

      if ($key === 'operations') {
        $newValues['description'] = $description;
      }

      $newValues[$key] = $value;

    }

    return $newValues;

  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {

    return $this->addDescriptionColumn(
      parent::buildHeader(), $this->t('Description'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {

    return $this->addDescriptionColumn(
      parent::buildRow($entity), $entity->getDescription(),
    );

    $parentValues = parent::buildRow($entity);

  }

}
