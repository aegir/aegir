<?php

declare(strict_types=1);

namespace Drupal\aegir_deployment_target\Entity;

use Drupal\aegir_api\Entity\AegirEntityBase;
use Drupal\aegir_deployment_target\Entity\DeploymentTargetInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use function t;

/**
 * Defines the Ægir Deployment Target entity.
 *
 * @ingroup aegir_deployment_target
 *
 * @ContentEntityType(
 *   id               = "aegir_deployment_target",
 *   label            = @Translation("Deployment Target"),
 *   label_collection = @Translation("Deployment Targets"),
 *   label_singular   = @Translation("deployment target"),
 *   label_plural     = @Translation("deployment targets"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count deployment target",
 *     plural   = "@count deployment targets",
 *   ),
 *   bundle_label = @Translation("Deployment Target type"),
 *   handlers     = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-delete" = "Drupal\Core\Entity\Form\RevisionDeleteForm",
 *       "revision-revert" = "Drupal\Core\Entity\Form\RevisionRevertForm",
 *     },
 *     "access"         = "Drupal\aegir_api\Entity\AegirEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html"     = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     },
 *     "translation"  = "Drupal\content_translation\ContentTranslationHandler",
 *     "views_data"   = "Drupal\views\EntityViewsData",
 *   },
 *   base_table       = "aegir_deployment_target",
 *   data_table       = "aegir_deployment_target_field_data",
 *   revision_table   = "aegir_deployment_target_revision",
 *   revision_data_table = "aegir_deployment_target_field_revision",
 *   translatable     = true,
 *   show_revision_ui = true,
 *   admin_permission = "administer aegir deployment target entities",
 *   entity_keys      = {
 *     "id"         = "id",
 *     "revision"   = "revision_id",
 *     "bundle"     = "type",
 *     "label"      = "label",
 *     "uuid"       = "uuid",
 *     "owner"      = "user_id",
 *     "langcode"   = "langcode",
 *     "published"  = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"        = "/admin/aegir/deployment-targets/{aegir_deployment_target}",
 *     "add-page"         = "/admin/aegir/deployment-targets/add",
 *     "add-form"         = "/admin/aegir/deployment-targets/add/{aegir_deployment_target_type}",
 *     "edit-form"        = "/admin/aegir/deployment-targets/{aegir_deployment_target}/edit",
 *     "delete-form"      = "/admin/aegir/deployment-targets/{aegir_deployment_target}/delete",
 *     "version-history"  = "/admin/aegir/deployment-targets/{aegir_deployment_target}/revisions",
 *     "revision"         = "/admin/aegir/deployment-targets/{aegir_deployment_target}/revisions/{aegir_deployment_target_revision}/view",
 *     "revision-revert-form" = "/admin/aegir/deployment-targets/{aegir_deployment_target}/revisions/{aegir_deployment_target_revision}/revert",
 *     "revision-delete-form" = "/admin/aegir/deployment-targets/{aegir_deployment_target}/revisions/{aegir_deployment_target_revision}/delete",
 *     "collection"       = "/admin/aegir/deployment-targets",
 *   },
 *   bundle_entity_type   = "aegir_deployment_target_type",
 *   field_ui_base_route  = "entity.aegir_deployment_target_type.canonical",
 * )
 */
class DeploymentTarget extends AegirEntityBase implements DeploymentTargetInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entityType,
  ): array {

    $fields = parent::baseFieldDefinitions($entityType);

    $fields['label']->setDescription(t(
      'A descriptive name for the deployment target.',
    ));

    return $fields;

  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(
    EntityTypeInterface $entityType, $bundle, array $baseFieldDefinitions,
  ) {

    $definitions = [];

    // This exists to allow the 'description' field on
    // 'aegir_deployment_target_type' to be configurable via the field UI.
    //
    // Note that even though this is defined for bundles, we can still use
    // BaseFieldDefinition and core won't complain; using
    // \Drupal\Core\Field\FieldDefinition doesn't work and doesn't seem to have
    // all the methods we need.
    //
    // @see \hook_entity_bundle_field_info()
    //   Could also be used.
    //
    // @see https://www.noreiko.com/blog/defining-bundle-fields-code
    //
    // @see https://mglaman.dev/blog/creating-fields-programmatically-and-not-through-field-configuration
    $definitions['description'] = BaseFieldDefinition::create('string_long')
      ->setName('description')
      ->setTargetEntityTypeId($entityType->id())
      ->setTargetBundle($bundle)

      ->setLabel(t('Description'))
      ->setDescription(t('A description for the deployment target.'))
      ->setTranslatable(true)
      ->setRevisionable(true)
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
      ])
      ->setDisplayOptions('view', [
        'region' => 'content',
      ]);

    return $definitions;

  }

}
