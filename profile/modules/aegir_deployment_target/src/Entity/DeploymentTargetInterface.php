<?php

declare(strict_types=1);

namespace Drupal\aegir_deployment_target\Entity;

use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateChildrenInterface;

/**
 * Interface for Ægir Deployment Target entities.
 */
interface DeploymentTargetInterface extends AegirEntityInterface, AegirEntityWithAutoCreateChildrenInterface {}
