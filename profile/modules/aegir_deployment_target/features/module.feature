@aegir @deployment_target @module
Feature: A module to manage aegir deployment target
  In order to manage aegir deployment targets
  As an aegir administrator
  I want to be able to enable the aegir_deployment_target module 

  Scenario: Enable a module to make sure we have deployment target.
    When I run "drush pm-list --type=module --status=enabled --no-core --package=Ægir"
    Then I should get:
    """
    Ægir      Ægir: Deployment Target (aegir_deployment_target)
    """
