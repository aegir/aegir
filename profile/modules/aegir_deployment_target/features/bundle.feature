@aegir @deployment_target @bundle @api
Feature: Create and manage Aegir deployment target bundles
  In order to define deployment target bundles that can host applications,
  as a deployment target manager,
  I want to be able to manage Aegir deployment target bundles.

  Background:
    Given I run "drush -y pm:install aegir_deployment_target_examples"
      And I am logged in as a "Deployment Target manager"

  Scenario: Create deployment target bundles
      When I am on "admin/aegir/deployment-targets/types"
       And I should see "Deployment Target types"
      When I click "Add Deployment Target type"
       And I fill in "Name" with "TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1"
       # Note: machine name has a max length.
       And I fill in "Machine-readable name" with "test_deployment_target_bundle_ne"
       And I fill in "Description" with "Test deployment target description"
       And I press the "Save" button
      Then I should be on "admin/aegir/deployment-targets/types"
       And I should see the success message "Created the TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1 deployment target type."

  Scenario: List deployment target bundles
      When I am on "admin/aegir/deployment-targets/types/add"
       And I fill in "Name" with "TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1"
       # Note: machine name has a max length.
       And I fill in "Machine-readable name" with "test_deployment_target_bundle_ne"
       And I fill in "Description" with "Test deployment target description"
       And I press the "Save" button
      When I am on "admin/aegir/deployment-targets/types/add"
       And I fill in "Name" with "TEST_DEPLOYMENT_TARGET_BUNDLE_NEW2"
       # Note: machine name has a max length.
       And I fill in "Machine-readable name" with "test_deployment_target_bundle_n2"
       And I fill in "Description" with "Test deployment target description2"
       And I press the "Save" button
      When I am on "admin/aegir/deployment-targets/types"
      Then I should see "Test deployment target description" in the "TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1" row
      Then I should see "Test deployment target description2" in the "TEST_DEPLOYMENT_TARGET_BUNDLE_NEW2" row

  Scenario: Update deployment target bundles
      When I am on "admin/aegir/deployment-targets/types"
      When I click "Edit" in the "TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1" row
      Then I should see "Edit TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1"
      When I fill in "Description" with "Test deployment target description update"
       And I press "Save"
      Then I should see the success message "Saved the TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1 deployment target type."
       And I should see "Test deployment target description update" in the "TEST_DEPLOYMENT_TARGET_BUNDLE_NEW1" row

   Scenario: Delete deployment target bundles
      When I am on "admin/aegir/deployment-targets/types/add"
       And I fill in "Name" with "TEST_DEPLOYMENT_TARGET_BUNDLE_FIRST"
       # Note: machine name has a max length.
       And I fill in "Machine-readable name" with "test_deployment_target_bundle_fi"
       And I press the "Save" button
      When I am on "admin/aegir/deployment-targets/types/add"
       And I fill in "Name" with "TEST_DEPLOYMENT_TARGET_BUNDLE_SECOND"
       # Note: machine name has a max length.
       And I fill in "Machine-readable name" with "test_deployment_target_bundle_se"
       And I press the "Save" button
      When I am on "admin/aegir/deployment-targets/types"
       And I click "Delete" in the "TEST_DEPLOYMENT_TARGET_BUNDLE_FIRST" row
      Then I should see "Are you sure you want to delete the deployment target type TEST_DEPLOYMENT_TARGET_BUNDLE_FIRST?"
      When I press "Delete"
      Then I should see the success message "The deployment target type TEST_DEPLOYMENT_TARGET_BUNDLE_FIRST has been deleted."
       And I should not see "TEST_DEPLOYMENT_TARGET_BUNDLE_FIRST" in the "content" region
       And I should see "TEST_DEPLOYMENT_TARGET_BUNDLE_SECOND" in the "content" region
      When I click "Edit" in the "TEST_DEPLOYMENT_TARGET_BUNDLE_SECOND" row
       And I click "Delete"
      Then I should see "Are you sure you want to delete the deployment target type TEST_DEPLOYMENT_TARGET_BUNDLE_SECOND?"
       And I press "Delete"
      Then I should see the success message "The deployment target type TEST_DEPLOYMENT_TARGET_BUNDLE_SECOND has been deleted."
       And I should not see "TEST_DEPLOYMENT_TARGET_BUNDLE_SECOND" in the "content" region
