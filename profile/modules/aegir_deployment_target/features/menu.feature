@deployment_target @menu @api
Feature: Menu of Aegir deployment targets and bundles
  In order to find existing deployment target bundles
  as a deployment target manager,
  I need to be able to view list of deployment targets and bundles

  Scenario: Make sure Ægir administrator can access Deployment Target menu items.
     Given I am logged in as a "Ægir administrator"
      #Base Deployment Targets
       And I should see the link "Deployment Targets" in the "aegir_menu" region
      When I click "Deployment Targets" in the "aegir_menu" region
      Then I should be on "admin/aegir/deployment-targets"
       And I should see "Deployment Targets" in the "header" region
      #Deployment Target types
       And I should see the link "Deployment Target types" in the "aegir_menu" region
      When I click "Deployment Target types" in the "aegir_menu" region
       And I should see "Deployment Target types" in the "header" region
      Then I should see "Add Deployment Target type"

  Scenario: Make sure Deployment Target manager can access Deployment Target menu and authorized menu items.
     Given I am logged in as a "Deployment Target manager"
      #Base Deployment Targets
       And I should see the link "Deployment Targets" in the "aegir_menu" region
      When I click "Deployment Targets" in the "aegir_menu" region
      Then I should be on "admin/aegir/deployment-targets"
       And I should see "Deployment Targets" in the "header" region
      #Deployment Target types
       And I should see the link "Deployment Target types" in the "aegir_menu" region
      When I click "Deployment Target types" in the "aegir_menu" region
       And I should see "Deployment Target types"
      Then I should see "Add Deployment Target type"
