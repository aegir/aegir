@deployment-target @api @javascript @disable-on-ci
Feature: Deployment Target setup and run Debug Operation
  In order to manage Debug Operations
  as an Ægir administrator,
  I need to be able to create Deployment Targets

  Background:
    Given I am logged in as an "Ægir administrator"
      And I am on "admin/aegir/deployment-targets"

  Scenario: Create new VM Deployment Target.
    Given I click "Add Deployment Target"
     Then I should be on "admin/aegir/deployment-targets/add/vm"
      And I should see "Add deployment target"
      And I should see "Target name (host)"
      And I should see "Routable address (hostname or IP)"
     When I enter "Test VM" for "Name"
      And I enter "test-vm" for "Target name"
      And I enter "127.0.0.1" for "Routable address"
      And I press "Save"
     Then I should see "Created the Test VM deployment target."
     When I go to "aegir/deployment-targets"
     Then I should see "test-vm"
     Then I should see "Test VM" in the "test-vm" row
      And I click "Test VM" in the "content" region
     Then I should see "Deployment Target Debug Operation" in the "content" region
     When I run the "Deployment Target Debug Operation" operation
      # Wait for the operation to finish
      And I wait "10" seconds
      And I view the "Deployment Target Debug Operation" operation
      And I wait for AJAX to finish
     Then I should see "DEPLOYMENT TARGET DEBUG MESSAGE" in the "drupal_modal" region
      And I should see "field_target_name: test-vm" in the "drupal_modal" region
      And I should see "field_routable_address: 127.0.0.1" in the "drupal_modal" region
      And I should see "Target name: test-vm - Address: 127.0.0.1" in the "drupal_modal" region
