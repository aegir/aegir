<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_deployment_target\Kernel;

use Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ægir Deployment Target entity kernel tests.
 *
 * @group aegir
 *
 * @group aegir_deployment_target
 */
class AegirDeploymentTargetEntityKernelTest extends AegirEntityBasicKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_deployment_target',
    'aegir_deployment_target_examples',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_deployment_target');

  }

  /**
   * Data provider for AegirEntityBasicKernelTestBase::testAegirEntityBasics().
   *
   * @return array
   *
   * @see \Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase::testAegirEntityBasics()
   */
  public static function aegirEntityBasicsProvider(): array {

    return [
      [
        'entityTypeId'  => 'aegir_deployment_target',
        'managerRole'   => 'aegir_deployment_target_manager',

        'values' => [
          // Use one of the bundles exported to
          // the 'aegir_deployment_target_examples' module.
          'type'    => 'example',
          'label'   => 'TEST_DEPLOYMENT_TARGET',
          'status'  => 1,
        ],
      ],
    ];

  }

}
