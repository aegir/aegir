@queue @access @api
Feature: Access to Aegir task queue configuration
  In order to allow tasks to be securely added to the task queue,
  as an Aegir administrator,
  I need to be able to access the task queue configuration form.

  Scenario: Anonymous users should not have access to task queue configuration.
    Given I am not logged in
     When I am on "admin/aegir/queue"
     Then I should see the heading "Log in"
      # @todo Should we verify the Username and Password fields are on the page?
      And I should see the text "Username"
      And I should see the text "Password"
      And I should see the link "Forgot your password?"
      And the url should match "user\/login"

  Scenario: Authenticated users should not have access to task queue configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Task queue"
     When I am on "admin/aegir/queue"
     Then I should see "Access denied"

  Scenario: Ægir administrators should have access to task queue configuration.
    Given I am logged in as an "Ægir administrator"
     When I am on "admin/aegir"
     Then I should see the link "Task queue"
      And I should see the text "Ægir task queue"
     When I click "Task queue"
     Then I should be on "admin/aegir/queue"
      And I should see the heading "Task queue configuration" in the "header" region
