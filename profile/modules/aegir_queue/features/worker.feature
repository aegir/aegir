@queue @queue-worker @api
Feature: Aegir task queue worker service
  In order to allow tasks to be run on servers,
  as an Aegir administrator,
  I need to be able to run the task queue runner service.

  Scenario: The task queue worker service is running.
    # Important: make sure to use the "ww" modifier for unlimited line lengths
    # or grep will often not match since the default behaviour of ps is to
    # concatenate the output to the available width of your terminal.
    Given I run "ps auxww | grep [r]elayd"
     Then I should get:
      """
      /usr/bin/python3 /usr/local/bin/celery --app=relayd worker
      """
      And I run "supervisorctl status relayd"
      # Note that there are an arbitrary number of formatting spaces between
      # "relayd" and "RUNNING" which may change at any point, so rather than
      # have this break randomly in different environments or points in time,
      # we're just checking for the presence of "RUNNING" and the absence of
      # "STOPPED" in the output.
      #
      # @todo Implement a Behat step to abstract this so it can handle variable
      #   white-space and allow this to be reused, or use grep to achieve the
      #   same.
     Then I should get:
      """
      RUNNING
      """
      And I should not get:
      """
      STOPPED
      """

  Scenario: The task queue worker can accept and process tasks.
     When I run "drush aegir:echo SomeTestString"
     Then I should get:
      """
      SomeTestString
      """
     When I run "drush aegir:echo AnotherTestString"
     Then I should get:
      """
      AnotherTestString
      """
