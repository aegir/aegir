<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\TaskQueue;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\aegir_queue\Connector\ConnectorInterface;

/**
 * Class AbstractTaskQueue.
 *
 * @package Drupal\aegir_queue
 */
abstract class AbstractTaskQueue implements TaskQueueInterface {

  use StringTranslationTrait;

  /**
   * Constructs a basic TaskQueue object.
   *
   * @param Drupal\aegir_queue\Connector\ConnectorInterface $connector
   *   The connector to task the queue.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly ConnectorInterface $connector,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->stringTranslation = $stringTranslation;

  }

  /**
   * Retrieve the form for the connector.
   */
  public function getConnectorForm(): array {
    return $this->connector->getForm();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigNames(): array {
    return [
      $this->connector->getConfigName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function checkConnectorSettings(FormStateInterface $formState): bool {
    $this->connector->setConfigFromFormState($formState);
    return $this->testConnection();
  }

  /**
   * Save the form values for the connector.
   */
  public function saveConnectorSettings(FormStateInterface $formState): void {
    $this->connector->saveConfigFromFormState($formState);
  }

  /**
   * {@inheritdoc}
   */
  abstract public function addTask(string $type, array $args, string $queue);

  /**
   * Test that a connection to the task queue works.
   */
  abstract protected function testConnection(): bool;

}
