<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\TaskQueue;

use Drupal\aegir_queue\Connector\ConnectorInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface TaskQueueInterface.
 *
 * @package Drupal\aegir_queue
 */
interface TaskQueueInterface {

  /**
   * Get the configuration names for the task queue configuration form.
   *
   * @return string[]
   */
  public function getConfigNames(): array;

  /**
   * Get the configuration form for the queue connector.
   *
   * @return array
   *
   * @see \Drupal\aegir_queue\Connector\ConnectorInterface::getForm()
   */
  public function getConnectorForm(): array;

  /**
   * Validate connector settings from configuration form.
   *
   * Check that form values for the connector will allow a connection to the
   * task queue.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The FormState passed from the TaskQueueConfigForm.
   */
  public function checkConnectorSettings(FormStateInterface $formState): bool;

  /**
   * Add a task to the queue.
   *
   * @param string $type
   *   The type of the task to add to the queue.
   * @param array $args
   *   An array of arguments for the task.
   * @param string $queue
   *   The name of a Celery queue to which to connect.
   */
  public function addTask(string $type, array $args, string $queue);

}
