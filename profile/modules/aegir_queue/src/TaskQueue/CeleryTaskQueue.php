<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\TaskQueue;

use Celery\AsyncResult;
use Celery\CeleryPublishException;
use Drupal\aegir_api\Logger\AegirLoggerTrait;

/**
 * Class TaskQueue.
 *
 * @package Drupal\aegir_queue
 */
class CeleryTaskQueue extends AbstractTaskQueue implements TaskQueueInterface {

  use AegirLoggerTrait;

  /**
   * Test connectivity to queue and worker daemons.
   */
  protected function testConnection(): bool {
    if (!$this->testWorkerConnection('dispatcherd')) return FALSE;
    if (!$this->testWorkerConnection('relayd')) return FALSE;
    $this->log()->display('The task queue is properly configured.');
    return TRUE;
  }

  /**
   * Create an 'echo' task to confirm connectivity with the queue and worker daemon.
   *
   * @TODO Clean this up. Displayed messages should be handled in the validateForm method.
   */
  protected function testWorkerConnection($worker): bool {
    $success_message = sprintf("Connection to %s worker succeeded.", $worker);

    $task_name = $worker . '.echo';
    $task = $this->addTask($task_name, [$this->t($success_message)], $worker);
    if (!$task) return FALSE;

    $this->log()->info('Added test task %task_name for %worker.', [
        '%task_name' => $task_name,
        '%worker' => $worker,
    ]);

    $increment = 100000;
    $counter = 0;
    $timeout = 5000000;
    while (!$task->isReady()) {
      usleep($increment);
      $counter += $increment;
      if ($counter >= $timeout) {
        $this->log()->error('Task %task_name execution exceeded timeout of :timeout seconds for %worker. Check %path on the %worker server for more details.', [
          '%task_name' => $task_name,
          ':timeout' => $timeout / 1000000,
          '%worker' => $worker,
          '%path' => '/var/log/aegir/' . $worker . '.error.log',
        ]);
        return FALSE;
      }
    }

    if ($task->isSuccess()) {
      $this->log()->display($task->getResult(), [], 'success');
      $this->log()->info('Test task execution took :count seconds for %worker.', [
        ':count' => round($counter / 1000000, 1),
        '%worker' => $worker,
      ]);
      return TRUE;
    }
    else {
      // @codeCoverageIgnoreStart
      $this->log()->error($task->getTraceback());
      // @codeCoverageIgnoreEnd
      return FALSE;
    }

  }

  /**
   * {@inheritdoc}
   *
   * @return \Celery\AsyncResult|false
   *   The asynchronous result of the Celery task, or false if there was an
   *   error connecting or Celery threw an exception.
   */
  public function addTask(
    string $type, array $args = [], string $queue = 'dispatcherd',
  ): AsyncResult|false {
    try {
      $this->connector->setConfigAttribute('exchange', $queue);
      $connection = $this->connector->connect();
      if (!$connection) return FALSE;

      $result = $connection->PostTask($type, $args, TRUE, $queue);
      return $result;
    }
    catch (CeleryPublishException $e) {
      $this->log()->error('Failed to add task to the %host task queue. Error message: %error', [
        '%host'  => $this->config['host'],
        '%error' => $e->getMessage(),
      ]);
      return FALSE;
    }
  }

}
