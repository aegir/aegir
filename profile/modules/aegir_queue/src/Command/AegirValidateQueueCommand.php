<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Class AegirValidateQueueCommand.
 */
#[AsCommand(
  name:         'aegir:validate_queue',
  description:  'Validate the state of the task queue.',
)]
class AegirValidateQueueCommand extends Command {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value storage service.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly StateInterface $state,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->stringTranslation = $stringTranslation;

    parent::__construct();

  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    # @TODO: this method seems to trigger an error like:
    # relayd.queue_valid[]: Command output: Warning: mkdir(): Permission denied in /var/www/html/vendor/drupal/console-core/src/Utils/ConfigurationManager.php on line 261
    # in /var/log/aegir/relayd.error.log on the relayd/web container. Fix this!
    $this
      ->setHidden(true)
      ->addArgument(
        'worker',
        InputArgument::REQUIRED,
        (string) $this->t(
          'The name of the queue worker to call (relayd or dispatcherd).',
        )
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {
    $io = new SymfonyStyle($input, $output);
    $args = $input->getArguments();

    # Set flag in Drupal state storage to indicate the worker queue is valid.
    $state_var = sprintf('aegir.%s.queue_valid', $args['worker']);
    $this->state->set($state_var, TRUE);

    $io->info((string) $this->t(
      'Ægir task queue validation command sent successfully.',
    ));

    return Command::SUCCESS;

  }

}
