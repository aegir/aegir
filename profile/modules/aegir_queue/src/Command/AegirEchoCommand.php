<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Drupal\aegir_queue\TaskQueue\TaskQueueInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Class AegirEchoCommand.
 */
#[AsCommand(
  name:         'aegir:echo',
  description:  'Print text after a round-trip through the task queue.',
)]
class AegirEchoCommand extends Command {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\aegir_queue\TaskQueue\TaskQueueInterface $queue
   *   The Ægir task queue.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly TaskQueueInterface $queue,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->stringTranslation = $stringTranslation;

    parent::__construct();

  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this
      ->setHidden(true)
      ->addArgument(
        'string',
        InputArgument::OPTIONAL,
        (string) $this->t(
          'A string that will be echoed back by the queue worker.',
        ),
        'echo'
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {
    $io = new SymfonyStyle($input, $output);

    $result = $this->queue->AddTask('dispatcherd.echo', [$input->getArgument('string')]);

    while (!$result->isReady()) {
      usleep(100000);
      echo '.';
    }

    if ($result->isSuccess()) {
      $io->info($result->getResult());

      return Command::SUCCESS;

    }
    else {
      // @codeCoverageIgnoreStart
      echo "ERROR";
      echo $result->getTraceback();
      // @codeCoverageIgnoreEnd

      return Command::FAILURE;

    }

  }

}
