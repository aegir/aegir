<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\Connector;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface ConnectorInterface.
 *
 * @package Drupal\aegir_queue
 */
interface ConnectorInterface {

  /**
   * Returns the name of the configuration used by this connector.
   */
  public function getConfigName(): string;

  /**
   * Returns the queue config form.
   */
  public function getForm(): array;

  /**
   * Save configuration from the current form state.
   */
  public function saveConfigFromFormState(FormStateInterface $formState): void;

  /**
   * Set current configuration from the current form state.
   */
  public function setConfigFromFormState(FormStateInterface $formState): void;

}
