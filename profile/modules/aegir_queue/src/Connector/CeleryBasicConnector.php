<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\Connector;

use Celery\CeleryAdvanced;
use Celery\CeleryConnectionException;
use PhpAmqpLib\Exception\AMQPExceptionInterface;

/**
 * Class CeleryBasicConnector.
 *
 * @package Drupal\aegir_queue
 */
class CeleryBasicConnector extends AbstractConnector implements ConnectorInterface {

  /**
   * {@inheritdoc}
   */
  public function getConfigName(): string {
    return 'aegir_queue.celery_task_queue_connector';
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(): array {
    $form = [];
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('The server on which the task queue is running.'),
      '#default_value' => $this->config['host'],
    ];
    $form['login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login'),
      '#description' => $this->t('The queue login.'),
      '#default_value' => $this->config['login'],
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The queue password.'),
      // Password fields don't support '#default_value', so we have to set it
      // directly.
      '#attributes' => ['value' => $this->config['password']],
    ];
    return $form;
  }

  /**
   * Returns an instantiated queue object.
   */
  public function connect(): CeleryAdvanced|false {
    try {
      $queue = new CeleryAdvanced($this->getBroker(), $this->getBackend());
      return $queue;
    }
    catch (CeleryConnectionException|AMQPExceptionInterface $e) {
      $this->log()->error('Failed to connect to the %host task queue. Error message: %error', [
        '%host'  => $this->config['host'],
        '%error' => $e->getMessage(),
      ]);
      return FALSE;
    }
  }

  /**
   * Return a configuration array for a Celery broker.
   */
  protected function getBroker(): array {
    $broker = [
      'host' => $this->config['host'],
      'login' => $this->config['login'],
      'password' => $this->config['password'],
      'vhost' => '/',
      'exchange' => $this->config['exchange'],
    ];
    return $broker;
  }

  /**
   * Return a configuration array for a Celery backend.
   */
  protected function getBackend(): array {
    $backend = [
      // @TODO Make these configurable.
      #'host' => $this->config['host'],
      'host' => 'redis',
      'login' => '',
      'password' => '',
      'vhost' => '0',
      'port' => 6379,
      'connector' => 'redis',
    ];
    return $backend;
  }

}
