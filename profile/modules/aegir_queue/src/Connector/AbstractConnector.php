<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\Connector;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\aegir_api\Logger\AegirLoggerTrait;

/**
 * Class AbstractConnector.
 *
 * @package Drupal\aegir_queue
 */
abstract class AbstractConnector implements ConnectorInterface {

  use AegirLoggerTrait, StringTranslationTrait;

  /**
   * A keyed array of config values.
   *
   * @var array
   */
  protected array $config;

  /**
   * Constructs an AbstractConnector.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration object factory.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->stringTranslation = $stringTranslation;

    $this->config = $this->loadConfig()->get();

  }

  /**
   * Load current configuration.
   */
  protected function loadConfig(): Config {
    return $this->configFactory->getEditable($this->getConfigName());
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getConfigName(): string;

  /**
   * Extract configuration from a FormState.
   */
  protected function getConfigFromFormState(
    FormStateInterface $formState,
  ): array {
    $config = [];
    foreach ($this->getConfigKeys() as $key) {
      $config[$key] = $formState->getValue($key);
    }
    return $config;
  }

  /**
   * Return the keys for this connector's configuration.
   */
  protected function getConfigKeys(): array {
    return array_keys($this->loadConfig()->get());
  }

  /**
   * Update the current task queue configuration.
   */
  protected function setConfig(array $config): void {
    $this->config = $config;
  }

  /**
   * Update the current task queue configuration with an individual attribute.
   */
  public function setConfigAttribute(string $key, string $value): void {
    $this->config[$key] = $value;
  }

  /**
   * Return configuration.
   */
  protected function getConfig(): array {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function saveConfigFromFormState(FormStateInterface $formState): void {
    $this->setConfigFromFormState($formState);
    $this->saveConfig();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigFromFormState(FormStateInterface $formState): void {
    $config = $this->getConfigFromFormState($formState);
    $this->setConfig($config);
  }

  /**
   * Save the task queue configuration.
   */
  protected function saveConfig(): void {
    foreach ($this->getConfig() as $key => $value) {
      $this->loadConfig()->set($key, $value);
    }
    $this->loadConfig()->save();
  }

}
