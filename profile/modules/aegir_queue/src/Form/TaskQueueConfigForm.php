<?php

declare(strict_types=1);

namespace Drupal\aegir_queue\Form;

use Drupal\aegir_api\Logger\AegirLoggerTrait;
use Drupal\aegir_queue\TaskQueue\TaskQueueInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\config_update\ConfigRevertInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaskQueueConfigForm.
 *
 * @package Drupal\aegir_queue
 */
class TaskQueueConfigForm extends ConfigFormBase {

  use AegirLoggerTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   *
   * @param \Drupal\config_update\ConfigRevertInterface $configReverter
   *   The configuration reverter.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value storage.
   *
   * @param \Drupal\aegir_queue\TaskQueue\TaskQueueInterface $queue
   *   The task queue.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    protected readonly ConfigRevertInterface  $configReverter,
    protected readonly StateInterface         $state,
    protected readonly TaskQueueInterface     $queue,
  ) {

    parent::__construct($configFactory);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config_update.config_update'),
      $container->get('state'),
      $container->get('aegir_queue.task_queue'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return $this->queue->getConfigNames();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'aegir_queue_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {

    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Task queue connection settings'),
    ];
    foreach ($this->queue->getConnectorForm() as $name => $element) {
      $form['connection'][$name] = $element;
    };
    $form['connection']['check'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check connection settings'),
    ];
    $form['connection']['queue_valid'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check task queue'),
    ];

    $form = parent::buildForm($form, $form_state);
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset to defaults'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(
    array &$form, FormStateInterface $form_state
  ): void {
    if ($form_state->getTriggeringElement()['#id'] != 'edit-reset') {
      if ($this->queue->checkConnectorSettings($form_state) === FALSE) {
        // @TODO Add additional debug instructions (paths to backend logs, etc.)
        $form_state->setErrorByName('connection', $this->t('Verify connection settings.'));
        $this->log()->error('The configuration options have NOT been saved.');
        return;
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    switch ($form_state->getTriggeringElement()['#attributes']['data-drupal-selector']) {
      case 'edit-reset':
        $this->resetForm();
        break;

      case 'edit-submit':
        $this->queue->saveConnectorSettings($form_state);
        parent::submitForm($form, $form_state);
        break;

      case 'edit-check':
        // Check occurs in validateForm().
        break;

      case 'edit-queue-valid':
        $this->checkQueueValidity('relayd');
        $this->checkQueueValidity('dispatcherd');
        break;

      default:
        break;
    }
  }

  /**
   * Send a queue_valid task to either relayd or dispatcherd worker to validate
   * they can successfully pass data back by running "drupal aegir" commands.
   *
   * @param string $worker
   *   The name of the queue worker to send the task to (relayd or dispatcherd)
   */
  protected function checkQueueValidity(string $worker): bool {

    // Initially set the queue valid state to FALSE
    $queue_valid = false;
    $state_var = 'aegir.'. $worker .'.queue_valid';

    $this->state->set($state_var, $queue_valid);

    // Post a task to set queue valid state to TRUE
    $task_name = $worker .'.queue_valid';
    $result = $this->queue->addTask($task_name, [$worker], $worker);

    // Poll for the queue_valid state to be set
    $increment = 0.1; // .1 seconds
    $counter = 0;
    $timeout = 60; // 60 seconds
    while (!$queue_valid) {
      usleep((int) ($increment * 1000000));
      $counter += $increment;
      if ($counter >= $timeout) {
        $this->log()->error('Task %task_name execution exceeded timeout of :timeout seconds checking queue validity for %worker. Check %path on the %worker server for more details.', [
          '%task_name' => $task_name,
          ':timeout' => $timeout / 1000000,
          '%worker' => $worker,
          '%path' => '/var/log/aegir/' . $worker . '.error.log',
        ]);
        return false;
      }

      $this->state->resetCache();
      $queue_valid = $this->state->get($state_var);
      # TODO: consider cleaning up the state var here
    }

    $this->log()->success('The %worker task queue is functioning correctly. Round-trip took :seconds seconds.', [
      '%worker' => $worker,
      ':seconds' => $counter,
    ]);

    return true;

  }

  /**
   * Revert form values to defaults.
   */
  protected function resetForm(): bool {
    foreach ($this->getEditableConfigNames() as $config_name) {
      if (!$this->configReverter->revert('system.simple', $config_name)) {
        // @codeCoverageIgnoreStart
        $this->log()->error('Failed to reset configuration (:name) to default values.', [
          ':name' => $config_name,
        ]);
        return false;
        // @codeCoverageIgnoreEnd
      }
    }
    $this->log()->success('The configuration options have been reset to default values.');

    return true;

  }

}
