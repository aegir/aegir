<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_queue\Kernel;

use Drupal\aegir_queue\TaskQueue\TaskQueueInterface;
use Drupal\Tests\aegir\Kernel\AegirKernelTestBase;
use function strtr;

/**
 * Ægir queue service kernel tests.
 *
 * @group aegir
 *
 * @group aegir_queue
 */
class AegirQueueServiceKernelTest extends AegirKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_queue',
  ];

  /**
   * Tests that the service instantiates the correct interface.
   */
  public function testQueueInterface() {

    $taskQueue = $this->container->get('aegir_queue.task_queue');

    $this->assertTrue(
      $taskQueue instanceof TaskQueueInterface, strtr(
        'The queue service does not implement :interface as was expected!',
        [':interface'  => TaskQueueInterface::class],
      ),
    );

  }

}
