@tasks @types @api
Feature: Create and manage Aegir task types
  In order to define task types that can be combined into applications,
  as a task manager,
  I want to be able to manage Aegir task bundles.

  Background:
    Given I am logged in as a "Task manager"
      And I am on "admin/aegir/tasks/types"

  Scenario: Create task types
     When I click "Add Ægir task type"
      And I fill in "Name" with "TEST_TASK_TYPE_NEW"
      And I fill in "Machine-readable name" with "test_task_type_new"
      And I press the "Save" button
     Then I should be on "admin/aegir/tasks/types"
      And I should see the success message "Created the TEST_TASK_TYPE_NEW task type."

  Scenario: Update task types
     When I click "Edit" in the "TEST_TASK_TYPE_NEW" row
     Then I should see the heading "Edit TEST_TASK_TYPE_NEW" in the "header" region
     When I fill in "Name" with "TEST_TASK_TYPE_CHANGED"
      And I press the "Save" button
     Then I should be on "admin/aegir/tasks/types"
     Then I should see the success message "Saved the TEST_TASK_TYPE_CHANGED task type."

  Scenario: Delete task types
     Then I should see the link "TEST_TASK_TYPE_CHANGED"
     When I click "Edit" in the "TEST_TASK_TYPE_CHANGED" row
     Then I should see the heading "Edit TEST_TASK_TYPE_CHANGED" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete the task type TEST_TASK_TYPE_CHANGED?" in the "header" region
     When I press the "Delete" button
     Then I should be on "admin/aegir/tasks/types"
     Then I should see the success message "The task type TEST_TASK_TYPE_CHANGED has been deleted."

