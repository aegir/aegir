@tasks @access @api
Feature: Access to Aegir tasks and types
  In order to define tasks that can be combined into applications,
  as a task manager,
  I need to be able to access Aegir task entities and types.

  Scenario: Anonymous users should not have access to tasks configuration.
    Given I am not logged in
     When I am on "admin/aegir"
     Then I should be on "user/login"
     Then I should see "Forgot your password?"

  Scenario: Authenticated users should not have access to tasks configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Tasks"

  Scenario: Task managers should have access to tasks configuration.
    Given I am logged in as a "Task manager"
     When I am on "admin/aegir"
     Then I should see the link "Tasks" in the "content" region
      And I should see the text "Ægir task entities and bundles" in the "content" region
     When I click "Tasks" in the "content" region
     Then I should be on "admin/aegir/tasks"
      And I should see the heading "Tasks" in the "header" region
      And I should see the link "Add Ægir task"
      And I should see the link "Task types"
     When I click "Task types"
     Then I should be on "admin/aegir/tasks/types"
      And I should see the heading "Task types" in the "header" region
      And I should see the link "Add Ægir task type"

