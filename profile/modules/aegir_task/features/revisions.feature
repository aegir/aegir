@tasks @revisions @api
Feature: Create and manage Aegir tasks
  In order to define tasks that can be combined into applications,
  as a task manager,
  I want to be able to manage Aegir task entities.

  Background:
    Given I run "drush -y pm:install aegir_test_task"
      And I am logged in as a "Task manager"
      And I am on "admin/aegir/tasks"

  Scenario: Create a task for later scenarios
    Given I click "Add Ægir task"
      And I click "TEST_TASK_TYPE_1" in the "content" region
      And I fill in "Name" with "TEST_TASK_B"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_TASK_B task."
     And I should see the link "TEST_TASK_B" in the "content" region
     Then I click "TEST_TASK_B" in the "content" region
      And I should see the link "Revisions" in the "tabs" region

  Scenario: Update the task to create some revisions
    Given I should see the link "TEST_TASK_B" in the "content" region
      And I click "TEST_TASK_B"
     When I click "Edit" in the "tabs" region
      And I fill in "Name" with "TEST_TASK_B-changed"
      And for "Revision log message" I enter "First test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_TASK_B-changed task."
     When I click "TEST_TASK_B-changed" in the "content" region
      And I click "Edit" in the "tabs" region
      And I fill in "Name" with "TEST_TASK_B"
      And for "Revision log message" I enter "Second test log message."
      And I press the "Save" button
      And I click "TEST_TASK_B" in the "content" region
      And I click "Revisions"
     Then I should see the heading "Revisions" in the "header" region
      And I should see the text "First test log message."
      And I should see the text "Second test log message."

  Scenario: View a task revision
    Given I click "TEST_TASK_B"
      And I click "Revisions" in the "tabs" region
     When I click "view revision" in the "First test log message." row
     Then I should see "Revision of TEST_TASK_B-changed from" in the "header" region

  Scenario: Revert a task revision
    Given I click "TEST_TASK_B"
      And I click "Revisions"
     When I click "Revert" in the "First test log message." row
     Then I should see the text "Are you sure you want to revert to the revision from" in the "header" region
     When I press "Revert"
     Then I should see the success message "TEST_TASK_TYPE_1 TEST_TASK_B-changed has been reverted to the revision from"
