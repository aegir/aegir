<?php

declare(strict_types=1);

/**
 * @file
 * Contains aegir_task.page.inc.
 *
 * Page callback for Ægir Task entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ægir task templates.
 *
 * Default template: aegir_task.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_aegir_task(array &$variables): void {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
