<?php

declare(strict_types=1);

namespace Drupal\aegir_task\Plugin\Field\FieldWidget;

use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormSimple;

/**
 * Simple inline widget that applies to task reference fields.
 *
 * @FieldWidget(
 *   id = "aegir_inline_task_form",
 *   label = @Translation("Inline task form"),
 *   field_types = {
 *     "auto_create_entity_reference",
 *   },
 *   multiple_values = false
 * )
 */
class AegirInlineTaskForm extends InlineEntityFormSimple {

}
