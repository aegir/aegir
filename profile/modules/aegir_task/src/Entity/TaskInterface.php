<?php

declare(strict_types=1);

namespace Drupal\aegir_task\Entity;

use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateParentInterface;

/**
 * Interface for Ægir Task entities.
 */
interface TaskInterface extends AegirEntityInterface, AegirEntityWithAutoCreateParentInterface {}
