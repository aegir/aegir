<?php

declare(strict_types=1);

namespace Drupal\aegir_task\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeBase;
use Drupal\aegir_task\Entity\TaskTypeInterface;

/**
 * Defines the Ægir Task type configuration entity.
 *
 * @ConfigEntityType(
 *   id               = "aegir_task_type",
 *   label            = @Translation("Task type"),
 *   label_collection = @Translation("Task types"),
 *   label_singular   = @Translation("task type"),
 *   label_plural     = @Translation("task types"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count task type",
 *     plural   = "@count task types",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityTypeListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "delete" = "Drupal\aegir_api\Form\AegirEntityTypeDeleteConfirmForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix    = "aegir_task_type",
 *   admin_permission = "administer aegir task types",
 *   bundle_of        = "aegir_task",
 *   entity_keys = {
 *     "id"     = "id",
 *     "label"  = "label",
 *     "uuid"   = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   links = {
 *     "canonical"    = "/admin/aegir/tasks/types/{aegir_task_type}",
 *     "add-form"     = "/admin/aegir/tasks/types/add",
 *     "edit-form"    = "/admin/aegir/tasks/types/{aegir_task_type}/edit",
 *     "delete-form"  = "/admin/aegir/tasks/types/{aegir_task_type}/delete",
 *     "collection"   = "/admin/aegir/tasks/types",
 *   },
 * )
 */
class TaskType extends AegirEntityTypeBase implements TaskTypeInterface {}
