<?php

declare(strict_types=1);

namespace Drupal\aegir_task\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;

/**
 * Interface for Ægir Task type entities.
 */
interface TaskTypeInterface extends AegirEntityTypeInterface {}
