<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_task\Kernel;

use Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ægir Task entity kernel tests.
 *
 * @group aegir
 *
 * @group aegir_task
 */
class AegirTaskEntityKernelTest extends AegirEntityBasicKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_task',
    'aegir_test_task',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_task');

  }

  /**
   * Data provider for AegirEntityBasicKernelTestBase::testAegirEntityBasics().
   *
   * @return array
   *
   * @see \Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase::testAegirEntityBasics()
   */
  public static function aegirEntityBasicsProvider(): array {

    return [
      [
        'entityTypeId'  => 'aegir_task',
        'managerRole'   => 'aegir_task_manager',

        'values' => [
          // Use one of the bundles exported to the 'aegir_test_task' module.
          'type'    => 'test_task_type_1',
          'name'    => 'TEST_TASK_A',
          'status'  => 1,
        ],
      ],
    ];

  }

}
