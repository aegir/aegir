<?php

declare(strict_types=1);

namespace Drupal\aegir_input_command\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Class AegirInputCommand.
 *
 * @package Drupal\aegir_input_command
 */
#[AsCommand(
  name:         'aegir:input',
  description:  'Update values in the Ægir front-end.',
)]
class AegirInputCommand extends Command {

  use StringTranslationTrait;

  /**
   * Constructs a new AegirInputCommand object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly EntityRepositoryInterface $entityRepository,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->stringTranslation = $stringTranslation;

    parent::__construct();

  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this
      ->setHidden(true)
      ->addArgument(
        'type',
        InputArgument::REQUIRED,
        (string) $this->t(
          'The type of entity into which we will be inputting data.',
        ),
      )
      ->addArgument(
        'uuid',
        InputArgument::REQUIRED,
        (string) $this->t(
          'The UUID of the entity into which we will be inputting data.',
        ),
      )
      ->addArgument(
        'field',
        InputArgument::REQUIRED,
        (string) $this->t('The field into which we will be inputting data.'),
      )
      # @TODO: Validate that 'data' is a string. For now.
      ->addArgument(
        'data',
        InputArgument::REQUIRED,
        (string) $this->t('The data to input into a front-end field.'),
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {
    $io = new SymfonyStyle($input, $output);
    $args = $input->getArguments();
    foreach ($args as $key => $value) {
      $io->info($key . ': ' . $value);
    }

    $entity = $this->entityRepository->loadEntityByUuid($args['type'], $args['uuid']);

    if (is_null($entity)) return $io->error((string) $this->t(
      'No @type found with UUID @uuid',
      ['@type' => $args['type'], '@uuid' => $args['uuid']],
    ));

    if (!$entity->hasField($args['field'])) return $io->error((string) $this->t(
      'No @field field found on @type entity type.',
      ['@field' => $args['field'], '@type' => $args['type']],
    ));

    $new_value = $this->getNewValue($entity, $args);
    $entity->set($args['field'], $new_value);

    $entity->setNewRevision(false);

    $entity->save();

    // @todo Validation and return return Command::FAILURE if failure?
    return Command::SUCCESS;

  }

  /**
   * Return the new field value.
   */
  protected function getNewValue(
    FieldableEntityInterface $entity, array $args,
  ): mixed {
    # @TODO: Consider how to handle limited multi-value fields.
    # @TODO: Consider how to handle non-text fields (ie. entity references)
    $field = $entity->get($args['field']);
    if (!$this->shouldAppend($field)) return $args['data'];

    $field->appendItem($args['data']);
    return $field->getValue();
  }

  /**
   * Determine whether to append to or overwrite the field.
   */
  protected function shouldAppend(FieldItemListInterface $field): bool {
    $cardinality = $field
      ->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
    return $cardinality != 1;
  }

}
