@api @console @crud
Feature: Console commands to create, list and delete Ægir entities.
  In order to allow CLI access to Ægir,
  as an Aegir administrator (with access to the `aegir` system user),
  I need to be able to run CRUD console commands.

  Scenario: Create an Ægir platform.
    # Given I am at "/var/aegir/platforms/aegir" on the command line
    Given I am at "/var/www/html" on the command line
     When I run "drush aegir:create --yes --entity_type=aegir_platform --bundle=drupal7 --name=TEST_PLATFORM"
     Then I should get:
      """
      Created a Drupal 7 Platform named "TEST_PLATFORM".
      """
     When I run "drush aegir:list"
     Then I should get:
      """
      TEST_PLATFORM
      Platform
      Drupal 7
      """

  Scenario: Delete an Ægir platform.
    # Given I am at "/var/aegir/platforms/aegir" on the command line
    Given I am at "/var/www/html" on the command line
     When I run "drush aegir:delete --entity_type=aegir_platform --name=TEST_PLATFORM -y"
     Then I should get:
      """
      Deleted a Platform with ID
      """
     When I run "drush aegir:list"
     Then I should not get:
      """
      TEST_PLATFORM
      """
