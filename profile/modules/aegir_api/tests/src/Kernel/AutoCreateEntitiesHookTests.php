<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_api\Kernel;

use Drupal\aegir_api\Value\AutoCreateChildEntityFieldsInfo;
use Drupal\aegir_api\WrappedEntities\WrappedEntityWithAutoCreateInfoInterface;
use Drupal\Tests\aegir\Kernel\AegirKernelTestBase;
use function strtr;

/**
 * Tests for the auto create entity hooks.
 *
 * @group aegir
 *
 * @group aegir_api
 *
 * @coversDefaultClass \Drupal\aegir_api\Hooks\AutoCreateEntityHooks
 */
class AutoCreateEntitiesHookTests extends AegirKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_example_site',
    'aegir_operation',
    'aegir_platform',
    'aegir_queue',
    'aegir_site',
    'aegir_task',
    'hux',
    'typed_entity',
    'field',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_operation');
    $this->installEntitySchema('aegir_platform');
    $this->installEntitySchema('aegir_site');
    $this->installEntitySchema('aegir_task');

    $this->installConfig(['aegir_example_site', 'aegir_site']);

  }

  /**
   * Test that the auto create base fields are created for our entity types.
   *
   * @covers ::baseFieldInfo()
   */
  public function testBaseFieldCreation(): void {

    /** @var \Drupal\Core\Entity\EntityStorageInterface */
    $storage = $this->container->get(
      'entity_type.manager',
    )->getStorage('aegir_operation');

    /** @var \Drupal\Core\Entity\EntityTypeInterface */
    $entityType = $storage->getEntityType();

    /** @var \Drupal\aegir_site\Entity\SiteInterface */
    $entity = $storage->create([
      $entityType->getKey('bundle') => 'example_log_debug_data',
      $entityType->getKey('label')  => 'Test operation',
    ]);

    $fieldInfo = AutoCreateChildEntityFieldsInfo::createFromEntityType(
      $entityType,
    );

    $this->assertTrue(
      $entity->hasField($fieldInfo->getEntityIdFieldName()), strtr(
        'The Ægir :labelSingular entity does not have the parent entity ID reference field (:fieldName) as was expected!',
        [
          ':labelSingular'  => $entityType->getSingularLabel(),
          ':fieldName'      => $fieldInfo->getEntityIdFieldName(),
        ],
      ),
    );

    $this->assertTrue(
      $entity->hasField($fieldInfo->getEntityTypeFieldName()), strtr(
        'The Ægir :labelSingular entity does not have the parent entity type reference field (:fieldName) as was expected!',
        [
          ':labelSingular'  => $entityType->getSingularLabel(),
          ':fieldName'      => $fieldInfo->getEntityTypeFieldName(),
        ],
      ),
    );

  }

  /**
   * Test child entity create/delete when creating/deleting their parent entity.
   *
   * @covers ::preSave()
   * @covers ::postSave()
   * @covers ::preDelete()
   *
   * @todo Test recursive create/delete.
   */
  public function testAutoCreateAndDelete(): void {

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entityTypeManager = $this->container->get('entity_type.manager');

    /** @var \Drupal\Core\Entity\EntityStorageInterface */
    $siteStorage = $entityTypeManager->getStorage('aegir_site');

    /** @var \Drupal\Core\Entity\EntityTypeInterface */
    $siteEntityType = $siteStorage->getEntityType();

    /** @var \Drupal\typed_entity\EntityWrapperInterface */
    $typedEntityRepositoryManager = $this->container->get(
      'Drupal\typed_entity\RepositoryManager',
    );

    /** @var \Drupal\aegir_site\Entity\SiteInterface */
    $siteEntity = $siteStorage->create([
      $siteEntityType->getKey('id')     => 1,
      $siteEntityType->getKey('bundle') => 'example_site',
      $siteEntityType->getKey('label')  => 'Test site',
    ]);

    // Entity must be saved to storage for the save hooks to be invoked.
    $siteStorage->save($siteEntity);

    // Reload the new entity so that storage populates all the fields we didn't
    // when creating. This must be done so that we can assert that this entity
    // is equal to the one reported by children as being their parent.
    $siteEntity = $siteStorage->load(1);

    /** @var \Drupal\aegir_api\WrappedEntities\WrappedEntityWithAutoCreateInfoInterface */
    $siteWrapped = $typedEntityRepositoryManager->wrap($siteEntity);

    $this->assertInstanceOf(
      WrappedEntityWithAutoCreateInfoInterface::class,
      $siteWrapped,
      strtr(
        'The wrapped entity class :class for the Ægir :labelSingular entity does not implement :interface as was expected!',
        [
          ':labelSingular'  => $siteEntityType->getSingularLabel(),
          ':interface'      => WrappedEntityWithAutoCreateInfoInterface::class,
          ':class'          => $siteWrapped::class,
        ],
      ),
    );

    $this->assertTrue(
      $siteWrapped->hasAutoCreateFields(),
      strtr(
        'The wrapped entity class :class for the Ægir :labelSingular entity reports that it doesn\'t have auto create fields!',
        [
          ':labelSingular'  => $siteEntityType->getSingularLabel(),
          ':class'          => $siteWrapped::class,
        ],
      ),
    );

    /** @var \Drupal\aegir_api\WrappedEntities\WrappedEntityWithParentInterface[] */
    $siteChildren = $siteWrapped->getChildren();

    $this->assertNotEmpty(
      $siteChildren,
      strtr(
        'The wrapped entity class :class for the Ægir :labelSingular entity reports that it doesn\'t have any children!',
        [
          ':labelSingular'  => $siteEntityType->getSingularLabel(),
          ':class'          => $siteWrapped::class,
        ],
      ),
    );

    foreach ($siteChildren as $fieldName => $wrappedChild) {

      $this->assertTrue(
        $wrappedChild->hasParent(),
        strtr(
          'The wrapped entity class :class for the Ægir :labelSingular entity reports that it doesn\'t have a parent!',
          [
            ':labelSingular'  =>
              $wrappedChild->getEntityType()->getSingularLabel(),
            ':class'          => $wrappedChild::class,
          ],
        ),
      );

      /** @var \Drupal\aegir_api\WrappedEntities\WrappedEntityWithChildrenInterface */
      $parentWrapped = $wrappedChild->getParent();

      $this->assertEquals(
        $siteEntity, $parentWrapped->getEntity(),
        'The parent entity returned from the child is different from the expected entity!',
      );

    }

    $siteStorage->delete([$siteEntity]);

    foreach ($siteChildren as $fieldName => $wrappedChild) {

      /** @var \Drupal\Core\Entity\EntityStorageInterface */
      $childStorage = $entityTypeManager->getStorage(
        $wrappedChild->getEntity()->getEntityTypeId(),
      );

      $this->assertNull(
        $childStorage->load($wrappedChild->getEntity()->id()),
        strtr(
          'The child entity in the :fieldName auto create field was not deleted when its parent was deleted!',
          [':fieldName' => $fieldName],
        ),
      );

    }

  }

}
