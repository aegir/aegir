<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_api\Kernel;

use Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface;
use Drupal\Tests\aegir\Kernel\AegirKernelTestBase;
use function strtr;

/**
 * Tests for wrapped entity marshalling.
 *
 * @group aegir
 *
 * @group aegir_api
 *
 * @coversDefaultClass \Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface
 */
class WrappedEntityMarshalKernelTest extends AegirKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_example_site',
    'aegir_example_update_operation',
    'aegir_operation',
    'aegir_platform',
    'aegir_queue',
    'aegir_site',
    'aegir_task',
    'hux',
    'typed_entity',
    'field',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_operation');
    $this->installEntitySchema('aegir_platform');
    $this->installEntitySchema('aegir_site');
    $this->installEntitySchema('aegir_task');

    $this->installConfig([
      'aegir_site', 'aegir_example_site', 'aegir_example_update_operation',
    ]);

  }

  /**
   * Test basic field value marshalling.
   *
   * @covers ::marshal()
   */
  public function testBasicMarshalling(): void {

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entityTypeManager = $this->container->get('entity_type.manager');

    /** @var \Drupal\Core\Entity\EntityStorageInterface */
    $siteStorage = $entityTypeManager->getStorage('aegir_site');

    /** @var \Drupal\Core\Entity\EntityTypeInterface */
    $siteEntityType = $siteStorage->getEntityType();

    /** @var \Drupal\typed_entity\EntityWrapperInterface */
    $typedEntityRepositoryManager = $this->container->get(
      'Drupal\typed_entity\RepositoryManager',
    );

    /** @var \Drupal\aegir_site\Entity\SiteInterface */
    $siteEntity = $siteStorage->create([
      $siteEntityType->getKey('id')     => 1,
      $siteEntityType->getKey('bundle') => 'example_site_update',
      $siteEntityType->getKey('label')  => 'Test site',
      'field_example_site_debug_data'   => 'Hello',
    ]);

    // Entity must be saved to storage for the save hooks to be invoked and thus
    // auto create entity reference fields do their thing.
    $siteStorage->save($siteEntity);

    // Reload the entity from storage to ensure all field values are populated
    // and consistent.
    $siteEntity = $siteStorage->load(1);

    /** @var \Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface */
    $siteWrapped = $typedEntityRepositoryManager->wrap($siteEntity);

    $this->assertInstanceOf(
      WrappedEntityMarshalInterface::class,
      $siteWrapped,
      strtr(
        'The wrapped entity class :class for the Ægir :labelSingular entity does not implement :interface as was expected!',
        [
          ':labelSingular'  =>
            $siteWrapped->getEntityType()->getSingularLabel(),
          ':interface'      => WrappedEntityMarshalInterface::class,
          ':class'          => $siteWrapped::class,
        ],
      ),
    );

    // To help visualize the nested structure we're going to traverse:
    //
    // site entity
    // -> field_example_update_debug_data
    //    -> operation entity
    //       -> field_example_write_debug_data
    //          -> task entity
    //             -> field_example_new_data

    /** @var \Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface */
    $operationWrapped = $siteWrapped->getChildFromField(
      $siteEntity->get('field_example_update_debug_data'),
    );

    /** @var \Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface */
    $taskWrapped = $operationWrapped->getChildFromField(
      $operationWrapped->getEntity()->get('field_example_write_debug_data'),
    );

    $taskWrapped->getEntity()->set('field_example_new_data', 'Some new data');

    // This works without having to save the task entity at this point because
    // all entities are already loaded and wrapped.
    $this->assertEquals([
      'field_example_site_debug_data' => 'Hello',
      'field_example_new_data'        => 'Some new data',
    ], $siteWrapped->marshal());

    // Now we need to save the task and reload/re-wrap the site entity to verify
    // it all still works after a round trip through storage.

    $taskWrapped->save();

    $siteEntity = $siteStorage->load(1);

    /** @var \Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface */
    $siteWrapped = $typedEntityRepositoryManager->wrap($siteEntity);

    $this->assertEquals([
      'field_example_site_debug_data' => 'Hello',
      'field_example_new_data'        => 'Some new data',
    ], $siteWrapped->marshal());

  }

}
