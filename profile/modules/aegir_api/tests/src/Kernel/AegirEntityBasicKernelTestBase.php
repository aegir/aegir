<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_api\Kernel;

use Drupal\Tests\aegir\Kernel\AegirKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use function strtr;
use function time;

/**
 * Template for testing basic functionality of Ægir entities.
 *
 * @group aegir
 *
 * @group aegir_api
 */
abstract class AegirEntityBasicKernelTestBase extends AegirKernelTestBase {

  use UserCreationTrait {
    createUser as drupalCreateUser;
    createRole as drupalCreateRole;
  }

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('user');

  }

  /**
   * Tests basic functionality of Ægir entities.
   *
   * @param string $entityTypeId
   *   The entity type ID to test.
   *
   * @param string $managerRole
   *   The user role machine name for entity manager role.
   *
   * @param array $values
   *   The field values to create test entities with. Note that these should be
   *   keyed by the actual field names for the entity type as you would pass to
   *   \Drupal\Core\Entity\EntityStorageInterface::create().
   *
   * @dataProvider aegirEntityBasicsProvider
   */
  public function testAegirEntityBasics(
    string  $entityTypeId,
    string  $managerRole,
    array   $values,
  ): void {

    $storage = $this->container->get(
      'entity_type.manager',
    )->getStorage($entityTypeId);

    $entityType = $storage->getEntityType();

    $manager = $this->drupalCreateUser([]);
    $manager->addRole($managerRole);
    $manager->save();

    $plainUser = $this->drupalCreateUser([]);

    $values['created'] = time();
    $values[$entityType->getKey('owner')] = $manager->id();

    $entity = $storage->create($values);

    $this->assertEquals(
      $entity->bundle(), $values[$entityType->getKey('bundle')],
    );

    $this->assertEquals(
      $entity->label(), $values[$entityType->getKey('label')],
    );

    $renameTo = $values[$entityType->getKey('label')] . '-renamed';

    $entity->setLabel($renameTo);

    $this->assertEquals($entity->label(), $renameTo);

    $this->assertEquals($entity->getCreatedTime(), $values['created']);

    $newCreated = time() - 10;
    $entity->setCreatedTime($newCreated);

    $this->assertEquals($entity->getCreatedTime(), $newCreated);

    $this->assertEquals(
      $entity->getOwnerId(), $values[$entityType->getKey('owner')], strtr(
        'The Ægir :labelSingular entity owner does not match the expected manager user ID!',
        [':labelSingular'  => $entityType->getSingularLabel()],
      ),
    );

    $entity->setOwner($plainUser);

    $this->assertEquals(
      $entity->getOwnerId(), $plainUser->id(), strtr(
        'The Ægir :labelSingular entity owner was not updated as expected!',
        [':labelSingular'  => $entityType->getSingularLabel()],
      ),
    );

    $this->assertEquals(
      $entity->isPublished(), $values[$entityType->getKey('published')],
    );

    $entity->setUnpublished();

    $this->assertFalse(
      $entity->isPublished(), strtr(
        'The Ægir :labelSingular entity is not unpublished as was expected!',
        [':labelSingular'  => $entityType->getSingularLabel()],
      ),
    );

  }

}
