<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use function array_filter;
use function array_keys;
use function is_null;
use function method_exists;
use LogicException;

/**
 * Trait for recursively getting wrapped entity bundle field values.
 *
 * @see \Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface
 */
trait WrappedEntityMarshalTrait {

  /**
   * A flat list of values from any referenced entities.
   *
   * @var mixed[]
   */
  protected array $marshalledValues = [];

  /**
   * {@inheritdoc}
   */
  public function marshal(): array {

    foreach ($this->getMarshallableFieldDefinitions() as $name => $definition) {

      $field = $this->entity->get($name);

      $type = $definition->getType();

      $this->marshalledValues[$name] =
        $definition->getFieldStorageDefinition()->getCardinality() === 1 ?
          $this->marshalSingleFieldItem($field, $type) :
          $this->marshalMultipleFieldItems($field, $type);

    }

    // Filter null values, which is what entity references will result in.
    return array_filter($this->marshalledValues, function($value) {
      return !is_null($value);
    });

  }

  /**
   * Return a list of all marshallable fields attached to wrapped entity.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of non-base field definitions, keyed by field name.
   */
  public function getMarshallableFieldDefinitions(): array {

    return array_filter(
      $this->entity->getFieldDefinitions(),
      function($field) { return (
        !method_exists($field, 'isBaseField') || !$field->isBaseField()
      );},
    );

  }

  /**
   * {@inheritdoc}
   */
  public function marshalRoles(): array {

    $wrappedBundleEntity = $this->repositoryManager()->wrap(
      $this->getBundleEntity(),
    );

    $tasks = $wrappedBundleEntity->getOrderedTaskTypes();

    return array_keys($tasks);

  }

  /**
   * Marshal each of a multi-value field's items.
   */
  protected function marshalMultipleFieldItems(
    array $items, string $type,
  ): array {

    $values = [];

    foreach ($items as $item) {
      $values[] = $this->marshalSingleFieldItem($item, $type);
    }

    return $values;

  }

  /**
   * Dispatch to the appropriate field-type method.
   *
   * @return mixed
   */
  protected function marshalSingleFieldItem(
    FieldItemListInterface $item, string $type,
  ): mixed {

    switch ($type) {

      case 'auto_create_entity_reference':
      case 'entity_reference':

        $this->marshalEntityReferenceField($item);

        return null;

      case 'string':
      case 'string_long':
      case 'list_string':
        return $this->marshalStringFieldItem($item);

      case 'boolean':
        return $this->marshalBooleanFieldItem($item);

      // Ignore fields that are intended to be populated from the backend.
      //
      // @todo Is this the best approach?
      case 'link':
      // This should count as a base field because it's created as such but it
      // still ends up attempting to get marshalled, resulting in test failures.
      // ¯\_(ツ)_/¯
      case 'changed':
        return null;

      default:

        throw new LogicException((string) $this->t(
          '"@methodName" does not currently support "@fieldType" fields!',
          ['@fieldType' => $type, '@methodName' => __METHOD__],
        ));

    }

  }

  /**
   * Recursively marshal each referenced entity.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   An entity reference field item list.
   *
   * @todo Unlike the other marshal<type>Field() methods, this does not return
   *   a value, which feels inconsistent; should this return something or should
   *   none of them return a value?
   */
  protected function marshalEntityReferenceField(
    EntityReferenceFieldItemListInterface $items,
  ): void {

    $entities = $items->referencedEntities();

    foreach ($entities as $entity) {

      $wrappedEntity = $this->repositoryManager()->wrap($entity);

      // Skip entities that don't have the marshal trait; e.g. if this is a
      // user entity or some other entity from outside of Ægir.
      if (
        is_null($wrappedEntity) ||
        !($wrappedEntity instanceof WrappedEntityMarshalInterface)
      ) {
        continue;
      }

      // We append the referenced entity's marshalled values rather than
      // returning them which keeps the results to a flat list.
      $this->marshalledValues += $wrappedEntity->marshal();

    }

  }

  /**
   * Return the value of a string field item.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $item
   *   A string field item.
   *
   * @return string
   *
   * @todo Use the Typed Data API to get this?
   *
   * @see \Drupal\Core\TypedData\Plugin\DataType\StringData::getCastedValue()
   */
  protected function marshalStringFieldItem(
    FieldItemListInterface $item,
  ): string {

    return $item->getString();

  }

  /**
   * Return the value of a boolean field item.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $item
   *   A boolean field item.
   *
   * @return bool
   *
   * @todo Use the Typed Data API to get this?
   *
   * @see \Drupal\Core\TypedData\Plugin\DataType\BooleanData::getCastedValue()
   */
  protected function marshalBooleanFieldItem(
    FieldItemListInterface $item,
  ): bool {

    return (bool) $item->getString();

  }

}
