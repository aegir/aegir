<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;

/**
 * Trait for wrapped entities with parent entities.
 */
trait WrappedEntityWithParentTrait {

  /**
   * The parent wrapped entity, if any.
   *
   * @var \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null
   */
  protected ?WrappedEntityInterface $parentWrappedEntity;

  /**
   * {@inheritdoc}
   */
  public function setParent(WrappedEntityInterface $parentWrappedEntity): void {
    $this->parentWrappedEntity = $parentWrappedEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function getParent(): ?WrappedEntityInterface {
    return $this->hasParent() ? $this->parentWrappedEntity : null;
  }

  /**
   * {@inheritdoc}
   */
  public function hasParent(): bool {
    return isset($this->parentWrappedEntity);
  }

}
