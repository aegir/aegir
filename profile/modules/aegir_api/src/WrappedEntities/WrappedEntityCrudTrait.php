<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\WrappedEntities\WrappedEntityCrudInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use UnexpectedValueException;

/**
 * Trait for wrapped entity CRUD operations.
 */
trait WrappedEntityCrudTrait {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * @throws \UnexpectedValueException
   *   If $this->entityTypeManager is not set.
   */
  public function save(): int|null {

    if (!isset($this->entityTypeManager)) {

      throw new UnexpectedValueException((string) $this->t(
        '"$this->entityTypeManager" is required to save this entity! Please inject the "entity_type.manager" service.'
      ));

    }

    return $this->entityTypeManager->getStorage(
      $this->entity->getEntityTypeId(),
    )->save($this->entity);

  }

  /**
   * {@inheritdoc}
   *
   * @throws \UnexpectedValueException
   *   If $this->entityTypeManager is not set.
   */
  public function delete(): void {

    if (!isset($this->entityTypeManager)) {

      throw new UnexpectedValueException((string) $this->t(
        '"$this->entityTypeManager" is required to save this entity! Please inject the "entity_type.manager" service.'
      ));

    }

    $this->entityTypeManager->getStorage(
      $this->entity->getEntityTypeId(),
    )->delete([$this->entity]);

  }

}
