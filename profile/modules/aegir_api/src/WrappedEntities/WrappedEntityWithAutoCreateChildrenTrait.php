<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\Value\AutoCreateParentEntityFieldsInfo;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;
use Exception;
use function reset;
use function strtr;
use LogicException;

/**
 * Trait for wrapped entities that can auto create child entities.
 */
trait WrappedEntityWithAutoCreateChildrenTrait {

  /**
   * Generate the label of an entity referenced by an auto create field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   An auto create field to generate the entity label for.
   *
   * @return string
   *   The generated auto create entity label for the given field.
   */
  protected function generateChildLabel(
    FieldItemListInterface $field,
  ): string {

    $template = "':field_label' :child_entity_type (for ':parent_label' :parent_entity_type)";

    $targetType = $this->getReferencedEntityType($field);

    $labelKey = $targetType->getKey('label');

    $tokens = [
      ':field_label'        => $field->getFieldDefinition()->getLabel(),
      ':child_entity_type'  => $targetType->getSingularLabel(),
      ':parent_label'       => $this->label(),
      ':parent_entity_type' => $this->getEntityType()->getSingularLabel(),
    ];

    $fieldStorageDefinitions = $this->entityFieldManager->getFieldStorageDefinitions($targetType->id());

    $maxLength = $fieldStorageDefinitions[$labelKey]->getSetting('max_length');

    return Unicode::truncate(
      strtr($template, $tokens),
      $maxLength,
      false,
      // This adds an elipsis when the label gets truncated.
      true,
    );

  }

  /**
   * Return the entity type identifier the provided field references.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   An auto create field to get the entity type identifier from.
   *
   * @return string
   *   The entity type identifier.
   */
  protected function getReferencedEntityTypeId(
    FieldItemListInterface $field,
  ): string {

    /** @var \Drupal\Core\TypedData\DataDefinitionInterface */
    $definition = $field->getFieldDefinition()->getItemDefinition();

    return $definition->getSetting('target_type');

  }

  /**
   * Return the entity type definition the provided field references.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   An auto create field to get the entity type from.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *   The entity type object, or null if it can't be found.
   */
  protected function getReferencedEntityType(
    FieldItemListInterface $field,
  ): ?EntityTypeInterface {

    return $this->entityTypeManager->getDefinition(
      $this->getReferencedEntityTypeId($field),
    );

  }

  /**
   * Get the bundle identifier for an auto create reference field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   An auto create field that specifies a target bundle.
   *
   * @return string
   *   An entity bundle identifier.
   *
   * @throws \LogicException
   *   If the field does not specify a target bundle or specifies more than one
   *   target bundle.
   */
  protected function getReferencedEntityBundleId(
    FieldItemListInterface $field,
  ): string {

    /** @var \Drupal\Core\TypedData\DataDefinitionInterface */
    $definition = $field->getFieldDefinition()->getItemDefinition();

    $handlerSettings = $definition->getSetting('handler_settings');

    if (empty($handlerSettings['target_bundles'])) {

      throw new LogicException((string) $this->t(
        '"@fieldName" must specify a bundle for the "@entityType" entity type!',
        [
          '@entityType' => $this->getReferencedEntityTypeId($field),
          '@fieldName'  => $field->getFieldDefinition()->getName(),
        ],
      ));

    }

    if (count($handlerSettings['target_bundles']) > 1) {

      throw new LogicException((string) $this->t(
        '"@fieldName" must specify only a single bundle for the "@entityType" entity type!',
        [
          '@entityType' => $this->getReferencedEntityTypeId($field),
          '@fieldName'  => $field->getFieldDefinition()->getName(),
        ],
      ));

    }

    // This is keyed by the bundle name, not numerically.
    return reset($handlerSettings['target_bundles']);

  }

  /**
   * {@inheritdoc}
   */
  public function getChildren(): array {

    $children = [];

    foreach ($this->getAutoCreateFields() as $fieldName => $field) {

      if (!$this->fieldHasAutoCreateEntity($field)) {
        continue;
      }

      $children[$fieldName] = $this->getChildFromField($field);

    }

    return $children;

  }

  /**
   * {@inheritdoc}
   *
   * @throws \LogicException
   *   If the requested field is not an auto create entity reference field.
   */
  public function getChildFromField(
    FieldItemListInterface $field,
  ): ?WrappedEntityInterface {

    if (!self::isAutoCreateField($field)) {
      throw new LogicException((string) $this->t(
        '"@fieldName" is not an auto create entity reference field!',
        ['@fieldName' => $field->getFieldDefinition()->getName()],
      ));
    }

    return $this->repositoryManager()->wrap($field->entity);

  }

  /**
   * {@inheritdoc}
   *
   * @todo Can we move creating the child entity out of this parent wrapped
   *   entity and abstract it further so this parent doesn't have to know
   *   anything about the child?
   */
  public function createChildForField(
    FieldItemListInterface $field,
  ): WrappedEntityInterface {

    $entityType = $this->getReferencedEntityType($field);

    return $this->repositoryManager()->repository(
      $this->getReferencedEntityTypeId($field),
    )->createEntity([
      $entityType->getKey('bundle') => $this->getReferencedEntityBundleId(
        $field,
      ),
      $entityType->getKey('label')  => $this->generateChildLabel($field),
    ]);

  }

  /**
   * Determines if the provided field is an auto create reference field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   A field to test.
   *
   * @return bool
   *   True if the passed field is an auto create reference field; false
   *   otherwise.
   *
   * @todo Can we test for AutoCreateEntityReferenceItemInterface instead of
   *   hard-coding the field type name here?
   */
  protected static function isAutoCreateField(
    FieldItemListInterface $field,
  ): bool {

    try {

      $fieldInfo = AutoCreateParentEntityFieldsInfo::createFromEntity(
        $field->getEntity(),
      );

    } catch (Exception $exception) {

      return false;

    }

    return $field->getFieldDefinition()->getType() === $fieldInfo->getFieldType();
  }

  /**
   * {@inheritdoc}
   */
  public function hasAutoCreateFields(): bool {
    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoCreateFields(): array {

    $autocreateFields = [];

    foreach ($this->entity->getFields() as $fieldName => $field) {

      if (!self::isAutoCreateField($field)) {
        continue;
      }

      $autocreateFields[$fieldName] = $field;

    }

    return $autocreateFields;

  }

  /**
   * Determine whether an auto create field has a referenced entity.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   A field to test.
   *
   * @return bool
   *   True if the field has an entity; false if not.
   *
   * @throws \LogicException
   *   If the requested field is not an auto create entity reference field.
   */
  protected function fieldHasAutoCreateEntity(
    FieldItemListInterface $field,
  ): bool {

    if (!self::isAutoCreateField($field)) {
      throw new LogicException((string) $this->t(
        '"@fieldName" is not an auto create entity reference field!',
        ['@fieldName' => $field->getFieldDefinition()->getName()],
      ));
    }

    return !$field->isEmpty();

  }
  /**
   * {@inheritdoc}
   */
  public function createReferencedEntities(): void {

    $autocreateFields = $this->getAutoCreateFields();

    foreach ($autocreateFields as $fieldName => $field) {

      if ($this->fieldHasAutoCreateEntity($field)) {
        continue;
      }

      $wrappedChildEntity = $this->createChildForField($field);

      $wrappedChildEntity->save();

      $field->setValue(['entity' => $wrappedChildEntity->getEntity()]);

    }

  }

  /**
   * {@inheritdoc}
   */
  public function deleteReferencedEntities(): void {

    foreach ($this->getAutoCreateFields() as $fieldName => $field) {

      if (!$this->fieldHasAutoCreateEntity($field)) {
        continue;
      }

      $wrappedChildEntity = $this->getChildFromField($field);

      $wrappedChildEntity->delete();

    }

  }

  /**
   * {@inheritdoc}
   */
  public function updateReferencedEntities(): void {

    foreach ($this->getAutoCreateFields() as $fieldName => $field) {

      if (!$this->fieldHasAutoCreateEntity($field)) {
        $this->createChildForField($field);
      }

      /** @var \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null */
      $wrappedChildEntity = $this->getChildFromField($field);

      // @todo Log this.
      if ($wrappedChildEntity === null) {
        continue;
      }

      $wrappedChildEntity->setParent($this);

    }

  }

}
