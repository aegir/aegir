<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

/**
 * Interface for wrapped entity CRUD operations.
 *
 * This provides methods not already provided by the Typed Entity module.
 *
 * @see \Drupal\typed_entity\TypedRepositories\TypedRepositoryInterface::createEntity()
 *   Creates a new entity and wraps it.
 *
 * @see \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface::getEntity()
 *   Gets the entity that's wrapped.
 */
interface WrappedEntityCrudInterface {

  /**
   * Save the entity to persistent storage.
   *
   * @return int|null
   *   The return value of EntityStorageInterface::save().
   *
   * @see \Drupal\Core\Entity\EntityStorageInterface::save()
   */
  public function save(): int|null;

  /**
   * Delete the entity from its persistent storage.
   *
   * @see \Drupal\Core\Entity\EntityStorageInterface::delete()
   */
  public function delete(): void;

}
