<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\WrappedEntities\WrappedEntityCrudInterface;
use Drupal\aegir_api\WrappedEntities\WrappedEntityCrudTrait;
use Drupal\aegir_api\WrappedEntities\WrappedEntityInfoInterface;
use Drupal\aegir_api\WrappedEntities\WrappedEntityInfoTrait;
use Drupal\aegir_api\WrappedEntityVariants\AegirEntityTypeVariantCondition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Drupal\typed_entity\TypedEntityContext;
use Drupal\typed_entity\WrappedEntities\WrappedEntityBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Common class for wrapped Ægir entity type entities.
 */
class AegirWrappedEntityType extends WrappedEntityBase implements WrappedEntityCrudInterface, WrappedEntityInfoInterface {

  use WrappedEntityCrudTrait;

  use WrappedEntityInfoTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wrap.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    EntityInterface $entity,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    EntityWrapperInterface $typedRepositoryManager,
  ) {

    parent::__construct($entity);

    // WrappedEntityBase doesn't inject this and if you call
    // $this->repositoryManager() without setting this, it'll get it using the
    // \Drupal static class. Since we're in a real dependency injection context,
    // we can just inject it ourselves from the container.
    //
    // @see \Drupal\typed_entity\WrappedEntities\WrappedEntityBase::repositoryManager()
    $this->setRepositoryManager($typedRepositoryManager);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container, EntityInterface $entity,
  ) {

    return new static(
      $entity,
      $container->get('entity_type.manager'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {

    /** @var \Drupal\Core\Entity\EntityInterface */
    $entity = $context->offsetGet('entity');

    // Provide the entity definition to the variant condition. Note that since
    // we're in a static method, we can't access $this->entityTypeManager and
    // entities use the \Drupal static object to access it so not ideal in
    // terms of dependency injection but not much we can do about that here.
    $context->offsetSet('entity_definition', $entity->getEntityType());

    /** @var \Drupal\typed_entity\WrappedEntityVariants\VariantConditionInterface */
    $condition = new AegirEntityTypeVariantCondition($context);

    try {

      $result = $condition->evaluate();

    } catch (InvalidValueException $exception) {
      return false;
    }

    return $result;

  }

}
