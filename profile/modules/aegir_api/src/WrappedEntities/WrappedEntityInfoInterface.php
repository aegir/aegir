<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Interface for wrapped entity type and bundle information.
 */
interface WrappedEntityInfoInterface {

  /**
   * Get this wrapped entity's entity type definition.
   *
   * @return \Drupal\aegir_api\Entity\AegirEntityTypeInterface|\Drupal\Core\Entity\EntityTypeInterface
   */
  public function getEntityType(): AegirEntityTypeInterface|EntityTypeInterface;

  /**
   * Get this wrapped entity's bundle entity.
   *
   * @return \Drupal\aegir_api\Entity\AegirEntityTypeInterface|\Drupal\Core\Entity\EntityTypeInterface|null
   */
  public function getBundleEntity(): AegirEntityTypeInterface|EntityTypeInterface|null;

}
