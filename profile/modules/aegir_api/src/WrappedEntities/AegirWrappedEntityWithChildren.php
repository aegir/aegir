<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\WrappedEntities\AegirWrappedEntity;
use Drupal\aegir_api\WrappedEntities\WrappedEntityWithAutoCreateChildrenTrait;
use Drupal\aegir_api\WrappedEntities\WrappedEntityWithChildrenInterface;
use Drupal\aegir_api\WrappedEntityVariants\AegirEntityWithAutoCreateChildrenVariantCondition;
use Drupal\typed_entity\TypedEntityContext;

/**
 * Wraps Ægir entities that can auto create child entities.
 */
class AegirWrappedEntityWithChildren extends AegirWrappedEntity implements WrappedEntityWithChildrenInterface {

  use WrappedEntityWithAutoCreateChildrenTrait;

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {

    /** @var \Drupal\Core\Entity\EntityInterface */
    $entity = $context->offsetGet('entity');

    // Provide the entity definition to the variant condition. Note that since
    // we're in a static method, we can't access $this->entityTypeManager and
    // entities use the \Drupal static object to access it so not ideal in
    // terms of dependency injection but not much we can do about that here.
    $context->offsetSet('entity_definition', $entity->getEntityType());

    /** @var \Drupal\typed_entity\WrappedEntityVariants\VariantConditionInterface */
    $condition = new AegirEntityWithAutoCreateChildrenVariantCondition(
      $context,
    );

    try {

      $result = $condition->evaluate();

    } catch (InvalidValueException $exception) {
      return false;
    }

    return $result;

  }

}
