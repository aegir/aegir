<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;

/**
 * Interface for wrapped entities with parent entities.
 */
interface WrappedEntityWithParentInterface {

  /**
   * Set the parent wrapped entity.
   *
   * @param \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface $parentWrappedEntity
   */
  public function setParent(WrappedEntityInterface $parentWrappedEntity): void;

  /**
   * Get the parent wrapped entity, if any.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null
   *   The parent wrapped entity, or null if this is the top level.
   */
  public function getParent(): ?WrappedEntityInterface;

  /**
   * Determine whether this has a parent wrapped entity.
   *
   * @return boolean
   *   True if the wrapped entity has a parent wrapped entity; false otherwise.
   */
  public function hasParent(): bool;

}
