<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\aegir_api\WrappedEntities\WrappedEntityCrudInterface;
use Drupal\aegir_api\WrappedEntities\WrappedEntityCrudTrait;
use Drupal\aegir_api\WrappedEntities\WrappedEntityInfoInterface;
use Drupal\aegir_api\WrappedEntities\WrappedEntityInfoTrait;
use Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalInterface;
use Drupal\aegir_api\WrappedEntities\WrappedEntityMarshalTrait;
use Drupal\aegir_api\WrappedEntities\WrappedEntityWithAutoCreateInfoInterface;
use Drupal\aegir_api\WrappedEntityVariants\AegirEntityVariantCondition;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Drupal\typed_entity\TypedEntityContext;
use Drupal\typed_entity\WrappedEntities\WrappedEntityBase;
use function is_null;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Common class for wrapped Ægir entities.
 */
class AegirWrappedEntity extends WrappedEntityBase implements WrappedEntityCrudInterface, WrappedEntityInfoInterface, WrappedEntityMarshalInterface, WrappedEntityWithAutoCreateInfoInterface {

  use StringTranslationTrait;
  use WrappedEntityCrudTrait;
  use WrappedEntityInfoTrait;
  use WrappedEntityMarshalTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\aegir_api\Entity\AegirEntityInterface $entity
   *   The entity to wrap.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    AegirEntityInterface $entity,
    protected readonly EntityFieldManagerInterface  $entityFieldManager,
    protected readonly EntityTypeManagerInterface   $entityTypeManager,
    EntityWrapperInterface      $typedRepositoryManager,
    TranslationInterface        $stringTranslation,
  ) {

    parent::__construct($entity);

    // WrappedEntityBase doesn't inject this and if you call
    // $this->repositoryManager() without setting this, it'll get it using the
    // \Drupal static class. Since we're in a real dependency injection context,
    // we can just inject it ourselves from the container.
    //
    // @see \Drupal\typed_entity\WrappedEntities\WrappedEntityBase::repositoryManager()
    $this->setRepositoryManager($typedRepositoryManager);

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container, EntityInterface $entity,
  ) {

    return new static(
      $entity,
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
      $container->get('string_translation'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {

    /** @var \Drupal\Core\Entity\EntityInterface */
    $entity = $context->offsetGet('entity');

    // Provide the entity definition to the variant condition. Note that since
    // we're in a static method, we can't access $this->entityTypeManager and
    // entities use the \Drupal static object to access it so not ideal in
    // terms of dependency injection but not much we can do about that here.
    $context->offsetSet('entity_definition', $entity->getEntityType());

    /** @var \Drupal\typed_entity\WrappedEntityVariants\VariantConditionInterface */
    $condition = new AegirEntityVariantCondition($context);

    try {

      $result = $condition->evaluate();

    } catch (InvalidValueException $exception) {
      return false;
    }

    return $result;

  }

  /**
   * {@inheritdoc}
   *
   * WrappedEntityInterface::label() defines the return value as only a string,
   * but core's EntityInterface::label() can return null if a label isn't found
   * on an entity which can happen with some Ægir entities; we have to override
   * the method to ensure it never returns null but an empty string instead to
   * avoid fatal errors.
   *
   * @see \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface::label()
   *
   * @see \Drupal\Core\Entity\EntityInterface::label()
   *
   * @todo Can we open an issue about this for the Typed Entity module?
   */
  public function label(): string {

    /** @var string|\Drupal\Core\StringTranslation\TranslatableMarkup|null */
    $label = $this->getEntity()->label();

    return is_null($label) ? '' : $label;

  }

  /**
   * {@inheritdoc}
   */
  public function hasAutoCreateFields(): bool {
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoCreateFields(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createReferencedEntities(): void {}

  /**
   * {@inheritdoc}
   */
  public function deleteReferencedEntities(): void {}

  /**
   * {@inheritdoc}
   */
  public function updateReferencedEntities(): void {}

}
