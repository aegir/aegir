<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

/**
 * Trait for recursively getting wrapped entity bundle field values.
 */
interface WrappedEntityMarshalInterface {

  /**
   * Recurse through entity references, gathering all bundle field values.
   *
   * @return array
   *   A flat array of values from all field values from all descendent
   *   entities, fetched recursively.
   */
  public function marshal(): array;

  /**
   * Return an ordered list of task roles.
   *
   * @todo Move to a task order trait? This currently only works for operations
   *   and assumes this is an operation entity.
   *
   * @see \Drupal\aegir_operation\WrappedEntities\Operation
   */
  public function marshalRoles(): array;

}
