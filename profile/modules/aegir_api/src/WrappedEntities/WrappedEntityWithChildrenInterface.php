<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;

/**
 * Interface for wrapped entities with child entities.
 */
interface WrappedEntityWithChildrenInterface {

  /**
   * Get all child wrapped entities referenced in entity reference fields.
   *
   * Implementations of this method will likely filter the returned wrapped
   * entities based on their own criteria.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]
   *   Zero or more wrapped entities keyed by the machine name of the field that
   *   references it.
   *
   * @see \Drupal\typed_entity\WrappedEntities\WrappedEntityBase::wrapReferences()
   *   Can be used to unconditionally get all referenced child wrapped entities.
   */
  public function getChildren(): array;

  /**
   * Get the child wrapped entity for a given field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   An entity reference field to get the entity from.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null
   *   A child wrapped entity, or null if an entity was not found.
   */
  public function getChildFromField(
    FieldItemListInterface $field,
  ): ?WrappedEntityInterface;

  /**
   * Create a child entity for a given field and wrap it.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   An entity reference field to create the entity for. The entity type and
   *   bundle will be automatically set based on the field settings.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface
   *   A child wrapped entity.
   */
  public function createChildForField(
    FieldItemListInterface $field,
  ): WrappedEntityInterface;

}
