<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Trait for wrapped entity type and bundle information.
 */
trait WrappedEntityInfoTrait {

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): AegirEntityTypeInterface|EntityTypeInterface {

    return $this->entityTypeManager->getDefinition(
      $this->entity->getEntityTypeId(),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function getBundleEntity(): AegirEntityTypeInterface|EntityTypeInterface|null {

    return $this->entityTypeManager->getStorage(
      $this->getEntityType()->getBundleEntityType(),
    )->load(
      $this->entity->bundle(),
    );

  }

}
