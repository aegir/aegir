<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

use Drupal\aegir_api\Value\AutoCreateChildEntityFieldsInfo;
use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;
use function is_null;

/**
 * Trait for wrapped entities can be auto created by parent entities.
 */
trait WrappedEntityWithAutoCreateParentTrait {

  /**
   * The parent wrapped entity, if any.
   *
   * @var \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null
   */
  protected ?WrappedEntityInterface $parentWrappedEntity;

  /**
   * Initialize the parent wrapped entity if not already done so.
   *
   * Note that due to the fact that our reference field that references the
   * parent can technically point to any entity and the entity type it points to
   * isn't known at the time the field is defined, calling
   * EntityReferenceFieldItemList::referencedEntities() or accessing
   * $field->entity will result in Drupal loading the wrong entity type because
   * it'll use the default of 'node' if the Node module is installed or 'user'
   * otherwise.
   *
   * @return bool
   *   Returns true if the parent wrapped entity is initialized; returns false
   *   if it isn't initialized and can't be due to a missing field value or
   *   other factor.
   *
   * @see \Drupal\Core\Field\EntityReferenceFieldItemList::referencedEntities()
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem::defaultStorageSettings()
   *   Defines the default 'target_type' as 'node' or 'user'.
   *
   * @todo Implement our own field item list class that overrides
   *   EntityReferenceFieldItemList::referencedEntities() to return the correct
   *   entity?
   */
  protected function initializeParentWrappedEntity(): bool {

    if (isset($this->parentWrappedEntity)) {
      return true;
    }

    $fieldInfo = AutoCreateChildEntityFieldsInfo::createFromEntityType(
      $this->getEntityType(),
    );

    $entityIdField = $this->entity->get(
      $fieldInfo->getEntityIdFieldName(),
    );

    $entityTypefield = $this->entity->get(
      $fieldInfo->getEntityTypeFieldName(),
    );

    if ($entityIdField->isEmpty() || $entityTypefield->isEmpty()) {
      return false;
    }

    $parentEntityId = $entityIdField->first()->getString();

    $parentEntityTypeId = $entityTypefield->first()->getString();

    $parentEntity = $this->entityTypeManager->getStorage(
      $parentEntityTypeId,
    )->load($parentEntityId);

    $parentWrappedEntity = $this->repositoryManager()->wrap($parentEntity);

    if (is_null($parentWrappedEntity)) {
      return false;
    }

    $this->parentWrappedEntity = $parentWrappedEntity;

    return true;

  }

  /**
   * {@inheritdoc}
   */
  public function setParent(WrappedEntityInterface $parentWrappedEntity): void {

    $this->parentWrappedEntity = $parentWrappedEntity;

    $fieldInfo = AutoCreateChildEntityFieldsInfo::createFromEntityType(
      $this->getEntityType(),
    );

    $this->entity
      ->set(
        $fieldInfo->getEntityIdFieldName(),
        $parentWrappedEntity->getEntity(),
      )
      ->set(
        $fieldInfo->getEntityTypeFieldName(),
        $parentWrappedEntity->getEntity()->getEntityTypeId(),
      )
      ->setNewRevision(false);

    $this->save();

  }

  /**
   * {@inheritdoc}
   */
  public function getParent(): ?WrappedEntityInterface {

    $this->initializeParentWrappedEntity();

    return $this->hasParent() ? $this->parentWrappedEntity : null;

  }

  /**
   * {@inheritdoc}
   *
   * @todo Do we actually need to check the entity field and thus have the
   *   overhead of loading the entity type to do so?
   */
  public function hasParent(): bool {

    $fieldInfo = AutoCreateChildEntityFieldsInfo::createFromEntityType(
      $this->getEntityType(),
    );

    return !$this->entity->get(
      $fieldInfo->getEntityIdFieldName(),
    )->isEmpty();

  }

}
