<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntities;

/**
 * Interface for entities that may have auto create entity reference fields.
 */
interface WrappedEntityWithAutoCreateInfoInterface {

  /**
   * Whether this entity has one or more auto create entity reference fields.
   *
   * @return boolean
   *   True if at least one of the entity's fields are an auto create entity
   *   reference field; false otherwise.
   */
  public function hasAutoCreateFields(): bool;

  /**
   * Get all auto create reference fields found on the wrapped entity.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface[]
   *   Zero or more auto create reference fields.
   */
  public function getAutoCreateFields(): array;

  /**
   * Create referenced entities that do not yet exist in auto create fields.
   */
  public function createReferencedEntities(): void;

  /**
   * Delete entities referenced in auto create fields.
   */
  public function deleteReferencedEntities(): void;

  /**
   * Sync changes to any entities referenced in auto create fields.
   */
  public function updateReferencedEntities(): void;

}
