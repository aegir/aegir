<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

#[FieldWidget(
  id:               'auto_create_entity_reference',
  label:            new TranslatableMarkup('Auto create entity reference'),
  description:      new TranslatableMarkup('A disabled field widget.'),
  field_types:      ['auto_create_entity_reference'],
  multiple_values:  false
)]
class AutoCreateEntityReferenceWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items, $delta, array $element, array &$form,
    FormStateInterface $form_state,
  ): array {

    $formElement = parent::formElement(
      $items, $delta, $element, $form, $form_state,
    );

    // Disable the widget since this field is handled by the auto create logic.
    //
    // @todo Is this the best way to handle this? Is it preferable to somehow
    //   exclude this field from form handling altogether? For example,
    //   currently form_alter hooks should work, which add some flexibility, but
    //   may interfere with how the auto create logic handles these references.
    $formElement['#access'] = false;

    return $formElement;

  }

}
