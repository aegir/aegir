<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;

#[FieldFormatter(
  id:           'auto_create_entity_reference_view',
  label:        new TranslatableMarkup('Rendered auto created entity'),
  description:  new TranslatableMarkup('Render the referenced auto created entity.'),
  field_types:  ['auto_create_entity_reference'],
)]
class AutoCreateEntityReferenceFormatter extends EntityReferenceEntityFormatter {}
