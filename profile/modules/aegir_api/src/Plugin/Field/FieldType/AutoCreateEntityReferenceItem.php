<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Plugin\Field\FieldType;

use Drupal\aegir_api\Plugin\Field\FieldType\AutoCreatedEntityReferenceItemInterface;
use Drupal\aegir_api\Plugin\Field\FieldType\AutoCreateEntityReferenceItemInterface;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

#[FieldType(
  id:           'auto_create_entity_reference',
  label:        new TranslatableMarkup('Auto create entity reference'),
  description:  new TranslatableMarkup(
    'An entity field containing an entity reference which automatically creates its referenced entities if they do not exist.',
  ),
  category:           'reference',
  default_widget:     'auto_create_entity_reference',
  default_formatter:  'auto_create_entity_reference_view',
  // We only support one entity per field for the time being.
  cardinality:        1,
  list_class:         EntityReferenceFieldItemList::class,
)]
class AutoCreateEntityReferenceItem extends EntityReferenceItem implements AutoCreateEntityReferenceItemInterface {

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(
    array &$form, FormStateInterface $form_state, $has_data,
  ): array {

    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    // We only want to allow pre-determined entities to be referenced, so lock
    // this down.
    //
    // @todo Should we hide it too?

    $element['target_type']['#disabled'] = true;

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(
    array $form, FormStateInterface $form_state,
  ): array {

    $form = parent::fieldSettingsForm($form, $form_state);

    // @todo Should we also force the 'reference method', or just rely on the
    //   default being correct?
    //
    // @todo Should we also hide this config setting?
    $form['handler']['handler']['#disabled'] = true;

    return $form;

  }

}
