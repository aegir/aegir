<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItemInterface;

/**
 * Interface for entity reference fields that auto create referenced entities.
 */
interface AutoCreateEntityReferenceItemInterface extends EntityReferenceItemInterface {}
