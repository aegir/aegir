<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Plugin\Deriver;

use Drupal\aegir_api\WrappedEntityVariants\AegirEntityVariantCondition;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\typed_entity\TypedEntityContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plug-in deriver for generating wrapped Ægir entity repositories.
 *
 * @see \Drupal\aegir_api\Plugin\TypedRepositories\AegirEntityRepository
 */
class AegirEntityRepositoryDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Constructs this deriver; saves dependencies.
   *
   * @param string $basePluginId
   *   The base plug-in ID to use when generating derivatives.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Drupal entity type manager.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly string $basePluginId,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices. Unfortunately, we
    // can't do this via constructor property promotion because
    // StringTranslationTrait doesn't yet type hint the interface on the
    // property at the time of writing on 10.3.x.
    $this->setStringTranslation($stringTranslation);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    return new static(
      $basePluginId,
      $container->get('entity_type.manager'),
      $container->get('string_translation'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @see https://www.drupal.org/project/typed_entity/issues/3206921
   *   Derivative support documented here.
   *
   * @see \Drupal\aegir_api\WrappedEntityVariants\AegirEntityVariantCondition
   *   Encapsulates the logic to determine if an entity definition matches our
   *   criteria.
   */
  public function getDerivativeDefinitions($basePluginDefinition): array {

    /** @var \Drupal\Core\Entity\EntityTypeInterface[] */
    $entityDefinitions = $this->entityTypeManager->getDefinitions();

    /** @var array[] */
    $defininitions = [];

    foreach ($entityDefinitions as $entityId => $entityDefinition) {

      /** @var \Drupal\typed_entity\WrappedEntityVariants\VariantConditionInterface */
      $condition = new AegirEntityVariantCondition(
        new TypedEntityContext(['entity_definition' => $entityDefinition]),
        $this->getStringTranslation(),
      );

      $result = $condition->evaluate();

      if ($result === false) {
        continue;
      }

      $defininitions[$entityId] = [
        'entity_type_id' => $entityId,
      ] + $basePluginDefinition;

    }

    return $defininitions;

  }

}
