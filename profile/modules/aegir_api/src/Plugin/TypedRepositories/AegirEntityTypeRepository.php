<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Plugin\TypedRepositories;

use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for Ægir wrapped entity type entities.
 *
 * Note that the 'entity_type_id' must be set here so that Typed Entity
 * doesn't throw an error when parsing this annotation, but is replaced by our
 * plug-in deriver with the intended entity IDs.
 *
 * @TypedRepository(
 *   id = "aegir_entity_type",
 *   entity_type_id = "placeholder",
 *   wrappers       = @ClassWithVariants(
 *     fallback = "Drupal\aegir_api\WrappedEntities\AegirWrappedEntityType",
 *     variants = {
 *       "Drupal\aegir_api\WrappedEntities\AegirWrappedEntityType",
 *     }
 *   ),
 *   description = @Translation("The repository for Ægir wrapped entity type entities."),
 *   deriver = "Drupal\aegir_api\Plugin\Deriver\AegirEntityTypeRepositoryDeriver"
 * )
 */
class AegirEntityTypeRepository extends TypedRepositoryBase {}
