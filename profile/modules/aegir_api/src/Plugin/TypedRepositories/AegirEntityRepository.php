<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Plugin\TypedRepositories;

use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for Ægir wrapped entities.
 *
 * Note that the 'entity_type_id' must be set here so that Typed Entity
 * doesn't throw an error when parsing this annotation, but is replaced by our
 * plug-in deriver with the intended entity IDs.
 *
 * Also note that the order of the variants is important: we want the variant
 * that has both children and a parent to get a chance to match before the
 * ones that only check for children or a parent; the latter two can match an
 * entity that has both children and a parent as they make no assumptions about
 * whether an entity that has children has a parent and vice versa.
 *
 * @TypedRepository(
 *   id = "aegir_entity",
 *   entity_type_id = "placeholder",
 *   wrappers       = @ClassWithVariants(
 *     fallback = "Drupal\aegir_api\WrappedEntities\AegirWrappedEntity",
 *     variants = {
 *       "Drupal\aegir_api\WrappedEntities\AegirWrappedEntityWithChildrenAndParent",
 *       "Drupal\aegir_api\WrappedEntities\AegirWrappedEntityWithChildren",
 *       "Drupal\aegir_api\WrappedEntities\AegirWrappedEntityWithParent",
 *     }
 *   ),
 *   description = @Translation("The repository for Ægir wrapped entities."),
 *   deriver = "Drupal\aegir_api\Plugin\Deriver\AegirEntityRepositoryDeriver"
 * )
 */
class AegirEntityRepository extends TypedRepositoryBase {}
