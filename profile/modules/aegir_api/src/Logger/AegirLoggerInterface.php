<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Logger;

interface AegirLoggerInterface {

  /**
   * Display and log an informational message.
   */
  public function info($message, $variables);

  /**
   * Display and log a success message.
   */
  public function success($message, $variables);

  /**
   * Display and log a warning message.
   */
  public function warning($message, $variables);

  /**
   * Display and log an error message.
   */
  public function error($message, $variables);

  /**
   * Display and log an arbitrary message.
   */
  public function emit($message, $variables, $severity);

  /**
   * Display an arbitrary message.
   */
  public function display($message, $variables, $severity);

  /**
   * Log an arbitrary message.
   */
  public function log($message, $variables, $severity);

  /**
   * Write an arbitrary message to the Drush log.
   */
  public function drush($message, $variables = [], $severity = '');

}
