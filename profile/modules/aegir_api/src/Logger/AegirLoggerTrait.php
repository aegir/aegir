<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Logger;

trait AegirLoggerTrait {

  /**
   * Return a new logger object.
   */
  protected function log() {
    return new AegirLogger();
  }

}
