<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Value;

use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateParentInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use function t;
use InvalidArgumentException;
use ReflectionClass;

/**
 * Abstracts auto create reference field names on children.
 *
 * This allows for overriding the field names on entity classes or interfaces
 * that extend AegirEntityWithAutoCreateParentInterface, and makes this value
 * object and that interface the sources of truth for how to handle these names.
 *
 * @see \Drupal\aegir_api\Entity\AegirEntityWithAutoCreateParentInterface::AUTO_CREATE_REFERENCING_FIELD_NAME
 *
 * @see \Drupal\aegir_api\Entity\AegirEntityWithAutoCreateParentInterface::AUTO_CREATE_REFERENCING_TYPE_FIELD_NAME
 */
class AutoCreateChildEntityFieldsInfo {

  /**
   * Constructor.
   *
   * @param string $entityIdFieldName
   *   The name of the field on the child containing its parent's entity ID.
   *
   * @param string $entityTypeFieldName
   *   The name of the field on the child containing its parent's entity type.
   */
  public function __construct(
    protected readonly string $entityIdFieldName,
    protected readonly string $entityTypeFieldName,
  ) {}

  /**
   * Construct this value object given an entity type object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   An entity type to get field names from.
   *
   * @throws \InvalidArgumentException
   *   If the entity class does not implement
   *   AegirEntityWithAutoCreateParentInterface.
   */
  public static function createFromEntityType(
    EntityTypeInterface $entityType,
  ): self {

    // Use ReflectionClass to automagically get potentially overridden field
    // names.
    $reflection = new ReflectionClass($entityType->getClass());

    if (!$reflection->implementsInterface(AegirEntityWithAutoCreateParentInterface::class,
    )) {

      throw new InvalidArgumentException((string) t(
        '@class must implement @interface!',
        [
          '@class'      => $reflection->name,
          '@interface'  => AegirEntityWithAutoCreateParentInterface::class,
        ],
      ));

    }

    return new self(
      $reflection->getConstant('AUTO_CREATE_REFERENCING_FIELD_NAME'),
      $reflection->getConstant('AUTO_CREATE_REFERENCING_TYPE_FIELD_NAME'),
    );

  }

  /**
   * Get the name of the field on the child containing its parent's entity ID.
   *
   * @return string
   */
  public function getEntityIdFieldName(): string {
    return $this->entityIdFieldName;
  }

  /**
   * Get the name of the field on the child containing its parent's entity type.
   *
   * @return string
   */
  public function getEntityTypeFieldName(): string {
    return $this->entityTypeFieldName;
  }

}
