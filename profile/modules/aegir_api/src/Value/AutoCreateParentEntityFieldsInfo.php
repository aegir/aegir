<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Value;

use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateChildrenInterface;
use Drupal\Core\Entity\EntityInterface;
use function t;
use InvalidArgumentException;

/**
 * Abstracts the auto create reference field type on parents.
 *
 * This allows for overriding the field name on entity classes or interfaces
 * that extend AegirEntityWithAutoCreateChildrenInterface, and makes this value
 * object and that interface the sources of truth for how to handle this name.
 *
 * @see \Drupal\aegir_api\Entity\AegirEntityWithAutoCreateChildrenInterface::AUTO_CREATE_FIELD_TYPE
 */
class AutoCreateParentEntityFieldsInfo {

  /**
   * Constructor.
   *
   * @param string $fieldType
   *   The auto create field type.
   */
  public function __construct(
    protected readonly string $fieldType,
  ) {}

  /**
   * Construct this value object given an entity object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entityType
   *   An entity object to get the field type from.
   *
   * @throws \InvalidArgumentException
   *   If the entity class does not implement
   *   AegirEntityWithAutoCreateChildrenInterface.
   */
  public static function createFromEntity(EntityInterface $entity): self {

    if (!($entity instanceof AegirEntityWithAutoCreateChildrenInterface)) {

      throw new InvalidArgumentException((string) t(
        '@class must implement @interface!',
        [
          '@class'      => $entity::class,
          '@interface'  => AegirEntityWithAutoCreateChildrenInterface::class,
        ],
      ));

    }

    return new self($entity::AUTO_CREATE_FIELD_TYPE);

  }

  /**
   * Get the auto create field type for this entity.
   *
   * @return string
   */
  public function getFieldType(): string {
    return $this->fieldType;
  }

}
