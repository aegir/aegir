<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Form;

use Drupal\aegir_api\Logger\AegirLoggerTrait;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default Ægir entity form.
 */
class AegirEntityForm extends ContentEntityForm {

  use AegirLoggerTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    EntityRepositoryInterface $entityRepository,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    TimeInterface $time,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {
    parent::__construct($entityRepository, $entityTypeBundleInfo, $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    $instance = new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

    // Use real dependency injection for the messenger trait which defaults to
    // using the \Drupal static object if it isn't set.
    //
    // @see \Drupal\Core\Entity\EntityTypeManager::getFormObject()
    //   Sets various other dependencies via setter methods so we don't have to.
    $instance->setMessenger($container->get('messenger'));

    return $instance;

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {

    $wrappedEntity = $this->typedRepositoryManager->wrap($this->entity);

    $status = $wrappedEntity->save();

    if ($status === SAVED_NEW) {

      $this->log()->success('Created the %label @entityType.', [
        '%label'      => $wrappedEntity->label(),
        '@entityType' => $wrappedEntity->getEntityType()->getSingularLabel(),
      ]);

    } else {

      $this->log()->success('Saved the %label @entityType.', [
        '%label'      => $wrappedEntity->label(),
        '@entityType' => $wrappedEntity->getEntityType()->getSingularLabel(),
      ]);

    }

    $form_state->setRedirectUrl($wrappedEntity->getEntity()->toUrl(
      'collection',
    ));

  }

}
