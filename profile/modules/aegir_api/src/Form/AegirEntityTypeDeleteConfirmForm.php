<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default Ægir entity type delete confirm form.
 *
 * This form prevents the deletion of a bundle if there are still entities
 * using that bundle. Unfortunately, core does not provide a built-in mechanism
 * at the time of writing (10.3.x/11.0.x) to do this, and even core entity
 * types have to implement their own solutions - such as the media type and
 * node type delete forms.
 *
 * @see \Drupal\media\Form\MediaTypeDeleteConfirmForm
 *
 * @see \Drupal\node\Form\NodeTypeDeleteConfirm
 *
 * @see https://www.drupal.org/project/drupal/issues/2413769
 *   Ongoing core issue to add some kind of constraint validation or similar.
 */
class AegirEntityTypeDeleteConfirmForm extends EntityDeleteForm {

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    $instance = new static(
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

    // Use real dependency injection for the messenger trait which defaults to
    // using the \Drupal static object if it isn't set.
    //
    // @see \Drupal\Core\Entity\EntityTypeManager::getFormObject()
    //   Sets various other dependencies via setter methods so we don't have to.
    $instance->setMessenger($container->get('messenger'));

    return $instance;

  }

  /**
   * Count existing entities for which this entity type provides the bundle of.
   *
   * @return int
   *   The number of entities.
   */
  protected function countExistingEntities(): int {

    $wrappedEntity = $this->typedRepositoryManager->wrap($this->getEntity());

    return $this->entityTypeManager->getStorage(
      $wrappedEntity->getEntityType()->getBundleOf(),
    )->getQuery()
    ->accessCheck(false)
    ->condition('type', $this->getEntity()->id())
    ->count()
    ->execute();

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $existingCount = $this->countExistingEntities();

    // If there are no existing entities, return the normal entity delete
    // confirm form.
    if ($existingCount === 0) {
      return parent::buildForm($form, $form_state);
    }

    $wrappedEntity = $this->typedRepositoryManager->wrap($this->getEntity());

    $entityType = $wrappedEntity->getEntityType();

    $bundleOfType = $this->entityTypeManager->getStorage(
      $wrappedEntity->getEntityType()->getBundleOf(),
    )->getEntityType();

    $caption = $this->formatPlural($existingCount,
      '%bundle is used by @count @entitySingular on your site. You cannot remove this @entityTypeSingular until you have removed the existing @entitySingular.',
      '%bundle is used by @count @entityPlural on your site. You cannot remove %bundle until you have removed all of the %bundle @entityPlural.',
      [
        '%bundle'             => $wrappedEntity->label(),
        '@entitySingular'     => $bundleOfType->getSingularLabel(),
        '@entityPlural'       => $bundleOfType->getPluralLabel(),
        '@entityTypeSingular' => $entityType->getSingularLabel(),
        '@entityTypePlural'   => $entityType->getPluralLabel(),
      ],
    );

    $form['#title'] = $this->t('Cannot delete @entityType %bundle', [
      '@entityType' => $entityType->getSingularLabel(),
      '%bundle'     => $wrappedEntity->label(),
    ]);

    $form['description'] = [
      '#type'   => 'html_tag',
      '#tag'    => 'p',
      '#value'  => $caption,
    ];

    return $form;

  }

}
