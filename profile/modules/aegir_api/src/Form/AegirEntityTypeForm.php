<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Form;

use Drupal\aegir_api\Logger\AegirLoggerTrait;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default Ægir entity type form.
 */
class AegirEntityTypeForm extends EntityForm {

  use AegirLoggerTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    $instance = new static(
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

    // Use real dependency injection for the messenger trait which defaults to
    // using the \Drupal static object if it isn't set.
    //
    // @see \Drupal\Core\Entity\EntityTypeManager::getFormObject()
    //   Sets various other dependencies via setter methods so we don't have to.
    $instance->setMessenger($container->get('messenger'));

    return $instance;

  }

  /**
   * Machine name exists callback to determine if the name is in use already.
   *
   * @param string|int $entityId
   *
   * @param array $element
   *
   * @see \Drupal\Core\Render\Element\MachineName
   */
  public function machineNameExists(
    string|int $entityId, array $element, FormStateInterface $formState,
  ): bool {

    $entity = $this->getEntity();

    /** @var \Drupal\Core\Entity\EntityStorageInterface */
    $storage = $this->entityTypeManager->getStorage(
      $entity->getEntityTypeId(),
    );

    return !empty(($storage->getQuery())
      ->condition('id', $element['#field_prefix'] . $entityId)
      ->accessCheck()
      ->execute()
    );

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form, FormStateInterface $form_state,
  ): array {

    $form = parent::buildForm($form, $form_state);

    $entity = $this->getEntity();

    $entityType = $this->entityTypeManager->getDefinition(
      $entity->getEntityTypeId(),
    );

    // @todo Get key name and UI label from the entity key and its label field
    //  definition so that we don't hard code those here.
    $form['label'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Name'),
      '#maxlength'      => 255,
      '#default_value'  => $entity->label(),
      '#description'    => $this->t('Name for this %entityType.', [
        '%entityType' => $entityType->getSingularLabel(),
      ]),
      '#required' => true,
    ];

    $form['id'] = [
      '#type'           => 'machine_name',
      '#default_value'  => $entity->id(),
      // @todo This should really be at the config/schema/entity/API level so
      //  it's enforced during config import, programmatic entity creation, etc.
      //
      // @see https://wimleers.com/talk-config-validation-lille/
      //
      // @see https://www.drupal.org/node/3404425
      //   Drupal core 10.3 change record: "Stricter validation for config
      //   schema types is available"
      //
      // @see https://symfony.com/doc/current/validation.html
      //
      // @see https://www.drupal.org/docs/drupal-apis/entity-api/entity-validation-api
      //
      // @see \Drupal\Core\Validation\Plugin\Validation\Constraint\LengthConstraint
      '#maxlength'      => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled'       => !$entity->isNew(),
      '#machine_name'   => [
        'exists' => [$this, 'machineNameExists'],
      ],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {

    $wrappedEntity = $this->typedRepositoryManager->wrap($this->entity);

    $status = $wrappedEntity->save();

    if ($status === SAVED_NEW) {

      $this->log()->success('Created the %label @entityType.', [
        '%label'      => $wrappedEntity->label(),
        '@entityType' => $wrappedEntity->getEntityType()->getSingularLabel(),
      ]);

    } else {

      $this->log()->success('Saved the %label @entityType.', [
        '%label'      => $wrappedEntity->label(),
        '@entityType' => $wrappedEntity->getEntityType()->getSingularLabel(),
      ]);

    }

    $form_state->setRedirectUrl($wrappedEntity->getEntity()->toUrl(
      'collection',
    ));

  }

}
