<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntityVariants;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\typed_entity\InvalidValueException;
use Drupal\typed_entity\TypedEntityContext;
use Drupal\typed_entity\WrappedEntityVariants\VariantConditionBase;

/**
 * Variant condition that matches to Ægir entity type entities.
 */
class AegirEntityTypeVariantCondition extends VariantConditionBase {

  /**
   * Variant condition constructor; saves dependencies.
   *
   * @param \Drupal\typed_entity\TypedEntityContext $context
   *   The context.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface|null $stringTranslation
   *   The string translation service, or null if this is instantiated in a
   *   static context.
   *
   * @param bool $isNegated
   *   Inverse the result of the evaluation.
   */
  public function __construct(
    TypedEntityContext $context,
    // @todo Can we autowire this and make it not nullable?
    ?TranslationInterface $stringTranslation = null,
    bool $isNegated = false,
  ) {

    parent::__construct($context, $isNegated);

    // VariantConditionBase doesn't actually inject this but relies on
    // StringTranslationTrait which will use \Drupal::service
    // ('string_translation') to fetch the service if it isn't set to the
    // property, so by setting it here we make use of dependency injection best
    // practices.
    if ($stringTranslation !== null) {
      $this->setStringTranslation($stringTranslation);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {

    $this->validateContext();

    $entityDefinition = $this->context->offsetGet('entity_definition');

    $result = $entityDefinition->entityClassImplements(
      AegirEntityTypeInterface::class,
    );

    return $this->isNegated() ? !$result : $result;

  }

  /**
   * {@inheritdoc}
   */
  public function summary(): TranslatableMarkup {

    return $this->t(
      'Active when an entity definition is an Ægir entity type entity.',
    );

  }

  /**
   * {@inheritdoc}
   */
  public function validateContext(): void {

    if (!$this->context->offsetExists('entity_definition')) {

      throw new InvalidValueException((string) $this->t(
        'You must provide an entity definition in the context!',
      ));

    }

    $entityDefinition = $this->context->offsetGet('entity_definition');

    if (!is_object($entityDefinition)) {

      throw new InvalidValueException((string) $this->t(
        'The provided entity definition context must be an object!',
      ));

    }

  }

}
