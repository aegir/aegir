<?php

declare(strict_types=1);

namespace Drupal\aegir_api\WrappedEntityVariants;

use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateChildrenInterface;
use Drupal\aegir_api\WrappedEntityVariants\AegirEntityVariantCondition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Variant condition for Ægir entities that can auto create child entities.
 */
class AegirEntityWithAutoCreateChildrenVariantCondition extends AegirEntityVariantCondition {

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {

    $this->validateContext();

    $entityDefinition = $this->context->offsetGet('entity_definition');

    $result = parent::evaluate() && $entityDefinition->entityClassImplements(
      AegirEntityWithAutoCreateChildrenInterface::class,
    );

    return $this->isNegated() ? !$result : $result;

  }

  /**
   * {@inheritdoc}
   */
  public function summary(): TranslatableMarkup {

    return $this->t(
      'Active when an entity definition is an Ægir entity that can auto create child entities.',
    );

  }

}
