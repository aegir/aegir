<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;
use function t;

/**
 * Common base class for Ægir entities.
 */
abstract class AegirEntityBase extends RevisionableContentEntityBase implements AegirEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entityType) {

    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entityType);
    $fields += static::publishedBaseFieldDefinitions($entityType);
    $fields += static::ownerBaseFieldDefinitions($entityType);

    // EntityChangedTrait does not provide a base field definition like the
    // other traits do, so we have to create it ourselves here.
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t(
        'The time that this @entityType was last edited.',
        ['@entityType' => $entityType->getSingularLabel()]
      ))
      ->setTranslatable(true);

    $fields[
      $entityType->getKey('label')
    ] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t(
        'A descriptive name for this @entityType.',
        ['@entityType' => $entityType->getSingularLabel()]
      ))
      ->setRequired(true)
      ->setTranslatable(true)
      ->setRevisionable(true)
      ->setDefaultValue('')
      ->setSetting('max_length', 60)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        // Move this up so that it tends to be before most or all other fields
        // by default on the form.
        'weight'  => -1,
      ])
      ->setDisplayOptions('view', [
        'region'  => 'content',
        'label'   => 'hidden',
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t(
        'The time that this @entityType was created.',
        ['@entityType' => $entityType->getSingularLabel()]
      ));

    // Hide the revision log message field by default but make it configurable.
    //
    // @see \Drupal\Core\Entity\RevisionLogEntityTrait::revisionLogBaseFieldDefinitions()
    $fields[$entityType->getRevisionMetadataKey('revision_log_message')]
    ->setDisplayOptions('form', [
      'region' => 'hidden',
    ])
    ->setDisplayConfigurable('form', true)
    ->setDisplayOptions('display', [
      'region' => 'hidden',
    ])
    ->setDisplayConfigurable('display', true);

    // @todo Move this to the entities that actually use it and remove from this
    //   base class. This is here purely for backwards compatibility with all
    //   the exported config that still exists referencing this.
    $fields['operation_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Operation status'))
      ->setDescription(t('The status of the operation.'))
      ->setRevisionable(true)
      ->setReadOnly(true)
      ->setDefaultValue('none')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    return $fields;

  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(TranslatableMarkup|string $label): AegirEntityInterface {

    $this->set($this->getEntityType()->getKey('label'), $label);

    return $this;

  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {

    return $this->get('created')->value;

  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): AegirEntityInterface {

    $this->set('created', $timestamp);

    return $this;

  }

  /**
   * {@inheritdoc}
   *
   * Adds a title attribute to view revision links so Behat tests can find them.
   *
   * Drupal core does not make it easy to make minor alterations to the revision
   * table output for entities without either overriding the controller or using
   * some hook to alter link or table output; the latter is inefficient because
   * it would be invoked for all links or tables, and the former requires tying
   * ourselves more closely to core's code than we'd like and would break more
   * easily in a core update.
   *
   * While not ideal, this solution mostly avoids having to care about how core
   * generates the revision overview and instead applies to the link itself,
   * where ever it's rendered.
   *
   * @see \Drupal\Core\Entity\Controller\VersionHistoryController::getRevisionDescription()
   *
   * @see \Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider::getRoutes()
   *
   * @todo Find a more suitable location for this outside of the entity class?
   */
  public function toUrl($rel = null, array $options = []): Url {

    $url = parent::toUrl($rel, $options);

    if ($rel !== 'revision') {
      return $url;
    }

    $attributes = $url->getOption('attributes');

    $attributes['title'] = t('view revision');

    $url->setOption('attributes', $attributes);

    return $url;

  }

}
