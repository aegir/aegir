<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Common interface for Ægir entities.
 */
interface AegirEntityInterface {

  /**
   * Set the label for this entity.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $label
   *   The label to set.
   *
   * @return \Drupal\aegir_api\Entity\AegirEntityInterface
   *   The entity for chaining.
   */
  public function setLabel(TranslatableMarkup|string $label): AegirEntityInterface;

  /**
   * Gets the entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the entity.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the entity creation timestamp.
   *
   * @param int $timestamp
   *   The entity creation timestamp.
   *
   * @return \Drupal\aegir_api\Entity\AegirEntityInterface
   *   The entity for chaining.
   */
  public function setCreatedTime(int $timestamp): AegirEntityInterface;

}
