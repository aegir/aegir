<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

/**
 * Ægir entity interface indicating an entity can have a parent auto create it.
 */
interface AegirEntityWithAutoCreateParentInterface {

  /**
   * Name of the field on this entity that references its parent.
   *
   * Implementations may override this and specify a different field name.
   */
  public const AUTO_CREATE_REFERENCING_FIELD_NAME =
    'auto_create_referencing_entity';

  /**
   * Name of the field on this entity that references its parent's type.
   *
   * Implementations may override this and specify a different field name.
   */
  public const AUTO_CREATE_REFERENCING_TYPE_FIELD_NAME =
    'auto_create_referencing_entity_type';

}
