<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default Ægir entity access control handler.
 *
 * @todo The only real difference this handler provides is using "aegir ENTITY"
 *   instead of "aegir_ENTITY" in permission names; i.e. it's literally just an
 *   underscore; remove this handler and use the Drupal core handler instead.
 */
class AegirEntityAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    EntityTypeInterface $entityType,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityWrapperInterface     $typedRepositoryManager,
  ) {

    parent::__construct($entityType);

  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container, EntityTypeInterface $entity_type,
  ) {

    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(
    EntityInterface $entity, $operation, AccountInterface $account,
  ): AccessResultInterface {

    $wrappedEntity = $this->typedRepositoryManager->wrap($entity);

    $label = (string) $wrappedEntity->getEntityType()->getSingularLabel();

    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, "view unpublished aegir {$label} entities");
        }
        return AccessResult::allowedIfHasPermission($account, "view published aegir {$label} entities");

      case 'update':
        return AccessResult::allowedIfHasPermission($account, "edit aegir {$label} entities");

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, "delete aegir {$label} entities");

      case 'view all revisions':
      case 'view revision':
        return AccessResult::allowedIfHasPermission($account, "view all aegir {$label} revisions");

      case 'revert':
        return AccessResult::allowedIfHasPermission($account, "revert all aegir {$label} revisions");

      case 'delete revision':
        return AccessResult::allowedIfHasPermission($account, "delete all aegir {$label} revisions");

    }

    // Unknown operation, no opinion.
    // @codeCoverageIgnoreStart
    return AccessResult::neutral();
    // @codeCoverageIgnoreEnd
  }

  /**
   * {@inheritdoc}
   *
   * @todo Change "add" to "create" to match the UI label of the permission;
   *   don't forget to update all existing entity types with the new permission
   *   name.
   */
  protected function checkCreateAccess(
    AccountInterface $account, array $context, $entity_bundle = null,
  ): AccessResultInterface {

    $label = (string) $this->entityTypeManager
      ->getStorage($context['entity_type_id'])
      ->getEntityType()
      ->getSingularLabel();

    return AccessResult::allowedIfHasPermission($account, "add aegir {$label} entities");

  }

}
