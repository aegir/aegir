<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Common interface for Ægir entity type entities.
 */
interface AegirEntityTypeInterface extends RevisionableEntityBundleInterface {}
