<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

/**
 * Ægir entity interface indicating an entity can have auto created children.
 */
interface AegirEntityWithAutoCreateChildrenInterface {

  /**
   * The auto create field type name.
   *
   * @todo Revisit and remove this if/when we want to make the criteria more
   *   flexible, i.e. testing instead for an interface on the field definition
   *   so that this field can be extended under a different field type name.
   */
  public const AUTO_CREATE_FIELD_TYPE = 'auto_create_entity_reference';

}
