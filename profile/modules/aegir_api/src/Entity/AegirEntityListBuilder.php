<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default Ægir entity list builder.
 */
class AegirEntityListBuilder extends EntityListBuilder {

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {

    parent::__construct($entity_type, $storage);

  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container, EntityTypeInterface $entity_type,
  ) {

    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

  }

  /**
   * {@inheritdoc}
   *
   * @todo Get keys and UI labels from entity keys and field definitions,
   *   respectively; don't hard-code them.
   */
  public function buildHeader(): array {

    return [
      'id'    => $this->t('ID'),
      'name'  => $this->t('Name'),
      'type'  => $this->t('Type'),
    ] + parent::buildHeader();

  }

  /**
   * {@inheritdoc}
   *
   * @todo Get keys from entity keys to avoid hard-coding them here..
   */
  public function buildRow(EntityInterface $entity): array {

    $wrappedEntity = $this->typedRepositoryManager->wrap($entity);

    $bundle = $wrappedEntity->getBundleEntity();

    return [
      'id'    => $entity->id(),
      'name'  => $entity->toLink(),
      'type'  => $bundle->label(),
    ] + parent::buildRow($entity);

  }

}
