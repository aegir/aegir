<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Default Ægir entity type list builder.
 */
class AegirEntityTypeListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * @todo Get keys and UI labels from entity keys and field definitions,
   *   respectively; don't hard-code them.
   */
  public function buildHeader(): array {

    return [
      'label' => $this->t('Type'),
      'id'    => $this->t('Machine name'),
    ] + parent::buildHeader();

  }

  /**
   * {@inheritdoc}
   *
   * @todo Get keys from entity keys to avoid hard-coding them here..
   */
  public function buildRow(EntityInterface $entity): array {

    return [
      'label' => $entity->toLink(),
      'id'    => $entity->id(),
    ] + parent::buildRow($entity);

  }

}
