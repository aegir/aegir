<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Common base class for Ægir entity type entities.
 */
abstract class AegirEntityTypeBase extends ConfigEntityBundleBase implements AegirEntityTypeInterface {

  /**
   * The Ægir entity type ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The Ægir entity type label.
   *
   * @var string
   */
  protected string $label;

  /**
   * Whether to default to creating new revisions.
   *
   * @var bool
   */
  protected bool $defaultNewRevision = true;

  /**
   * @inheritdoc
   */
  public function shouldCreateNewRevision(): bool {
    return $this->defaultNewRevision;
  }

}
