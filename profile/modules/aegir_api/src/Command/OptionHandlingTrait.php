<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;

/**
 * Trait OptionHandlingTrait.
 *
 * @package Drupal\aegir_api
 */
trait OptionHandlingTrait {

  use ConfirmTrait;
  use IoTrait;

  protected $options = [];

  protected function registerOptions(): void {
    foreach ($this->options as $option) {
      $this->$option = $this->registerOption($option);
    }
    if ($this->confirm) $this->registerConfirmation();
  }

  protected function determineOptions(): void {
    foreach ($this->options as $option) {
      $this->$option = $this->determineOption($option);
    }
    if ($this->confirm) $this->determineConfirmation();
  }

  protected function registerOption(string $option): mixed {
    return $this->$option ? null : $this->input()->getOption($option);
  }

  protected function determineOption(string $option): ?string {
    return $this->$option ? null : $this->promptForOption($option);
  }

  protected function chooseOption(
    string $option, array $choices,
  ): string {
    $question = new AegirChoiceQuestion(
      $this->translate("options.{$option}.prompt"),
      $choices,
      key($choices)
    );
    $question->setErrorMessage(
      $this->translate("options.{$option}.error")
    );

    return $this->getHelper('question')
      ->ask($this->input(), $this->io(), $question);
  }

  protected function getReplacements(): array {
    $replacements = [];
    foreach ($this->options as $option) {
      if ($this->$option) {
        $replacements['@' . $option] = $this->getReplacement($option);
      }
    }
    return $replacements;
  }

  protected function askOption(string $option): mixed {
    $question = new Question(
      $this->translate("options.{$option}.prompt") . ' '
    );

    return $this->getHelper('question')
      ->ask($this->input(), $this->io(), $question);
  }

}
