<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Command;

use Drupal\aegir_api\Command\EntityApiTrait;
use Drupal\aegir_api\Command\OptionHandlingTrait;
use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DeleteCommand.
 *
 * @package Drupal\aegir_api
 *
 * @TODO Add tests.
 */
#[AsCommand(name: 'aegir:delete', description: 'Delete Ægir entities.')]
class DeleteCommand extends Command {

  use EntityApiTrait;

  use OptionHandlingTrait;

  use StringTranslationTrait;

  protected string|null $entity_type = null;
  // This should really be int|null but OptionHandlingTrait only does strings
  // and null.
  protected string|null $id = null;
  protected string|null $name = null;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

    parent::__construct();

  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {

    $this->confirm = true;

    $this->options = ['entity_type', 'id', 'name'];

    $this
      ->addOption(
        'entity_type',
        null,
        InputOption::VALUE_REQUIRED,
        (string) $this->t('The entity type to delete.'),
      )
      ->addOption(
        'id',
        null,
        InputOption::VALUE_REQUIRED,
        (string) $this->t('The ID of the entity to delete.'),
      )
      ->addOption(
        'name',
        null,
        InputOption::VALUE_REQUIRED,
        (string) $this->t('The name of the entity to create.'),
      );

  }

  /**
   * {@inheritdoc}
   */
  protected function initialize(
    InputInterface $input, OutputInterface $output,
  ): void {
    parent::initialize($input, $output);
    $this->initializeIo($input, $output);
    $this->registerOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(
    InputInterface $input, OutputInterface $output,
  ): void {
    $this->determineOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {
    if (!$this->confirmed()) return Command::FAILURE;
    if ($this->name) {
      // This should really be an integer but OptionHandlingTrait only does
      // strings and null.
      $this->id = (string) $this->getIdFromName();
    }
    if ($this->deleteEntity()) {
      $this->io()->success($this->translate('success'));

      return Command::SUCCESS;

    }
    else {
      $this->io()->error($this->translate('failure'));

      return Command::FAILURE;

    }
  }

  /**
   * Returns a translated UI string given a key.
   *
   * This currently exists as a replacement for the translations previously
   * stored in YAML files that were handled by Drupal Console.
   *
   * @param string $key
   *
   * @return string
   *
   * @todo Move these strings out of this method and to the code that calls
   *   them, and once the method is empty, delete it.
   */
  protected function translate(string $key): string {

    switch ($key) {

      case 'options.entity_type.prompt':
        return (string) $this->t(
          'What type of entity would you like to delete?',
        );

      case 'options.entity_type.error':
        return (string) $this->t(
          'Type @entity_type is invalid.',
          ['@entity_type' => $this->entity_type],
        );

      case 'options.id.prompt':
        return (string) $this->t(
          'Which @id would you like to delete?',
          ['@id' => $this->id],
        );

      case 'options.id.error':
        return (string) $this->t(
          'ID @id is invalid.',
          ['@id' => $this->id],
        );

      case 'error.no-entity':
        return (string) $this->t(
          'No @entity_type entity exists with ID @id.',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@id' => $this->id,
          ],
        );

      case 'confirm':
        return (string) $this->t(
          'You are about to delete the @entity_type with ID "@id". Proceed?',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@id' => $this->id,
          ],
        );

      case 'success':
        return (string) $this->t(
          'Deleted a @entity_type with ID @id.',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@id' => $this->id,
          ],
        );

      case 'failure':
        return (string) $this->t(
          'Failed to delete a @entity_type with ID @id.',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@id' => $this->id,
          ],
        );

    }

  }

  protected function promptForOption(string $option): string {
    switch ($option) {
      case 'entity_type':
        $choices = $this->getAegirEntityTypeLabels();
        return $this->chooseOption($option, $choices);
      case 'id':
        $choices = $this->getAegirEntityChoices($this->entity_type);
        return $this->chooseOption($option, $choices);
      default:
        # TODO Throw an appropriate exception.
        return '';
    }
  }

  protected function getIdFromName(): string {
    $ids = $this->entityTypeManager->getStorage($this->entity_type)->getQuery()
      ->condition('name', $this->name, '=')
      ->accessCheck(false)
      ->execute();
    // @todo What if we don't get any results?
    return reset($ids);
  }

  protected function getReplacement(string $option): string {
    switch ($option) {
      case 'entity_type':
        return $this->getEntityTypeLabel();
      case 'id':
        return $this->id;
      case 'name':
        return $this->name;
      default:
        # TODO Throw an appropriate exception.
        return '';
    }
  }

  protected function getEntityTypeLabel(): string {
    return $this->getAegirEntityTypeLabels()[$this->entity_type];
  }

  /**
   * Delete an entity.
   */
  protected function deleteEntity(): bool {
    $entity = $this->loadEntity();
    if (!$entity) return FALSE;
    $entity->delete();
    return TRUE;
  }

  /**
   * Load an entity.
   *
   * @return \Drupal\aegir_api\Entity\AegirEntityInterface|null
   */
  protected function loadEntity(): ?AegirEntityInterface {
    $entity = $this
      ->entityTypeManager
      ->getStorage($this->entity_type)
      ->load($this->id);
    if (is_null($entity)) {
      $this->io()->error(
        $this->translate('error.no-entity')
      );
      return null;
    }
    return $entity;
  }

}
