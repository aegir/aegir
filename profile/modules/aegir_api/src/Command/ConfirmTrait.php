<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Trait ConfirmTrait.
 *
 * @package Drupal\aegir_api
 */
trait ConfirmTrait {

  protected bool $confirm = FALSE;

  protected bool $confirmed = FALSE;

  protected function askConfirmation(): void {
    $question = new ConfirmationQuestion(
      $this->translate('confirm') . ' ',
      FALSE
    );
    $this->confirmed = $this->getHelper('question')
      ->ask($this->input(), $this->io(), $question);
  }

  protected function confirmed(): bool {
    return $this->confirmed;
  }

  protected function registerConfirmation(): void {
    $this->confirmed = $this->input()->getOption('yes');
  }

  protected function determineConfirmation(): void {
    $this->confirmed || $this->askConfirmation();
  }

}
