<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Command;

use Drupal\aegir_api\Command\EntityApiTrait;
use Drupal\aegir_api\WrappedEntities\WrappedEntityInfoInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use function is_null;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ListCommand.
 *
 * @package Drupal\aegir_api
 *
 * @TODO Add tests.
 */
#[AsCommand(name: 'aegir:list', description: 'List Ægir entities.')]
class ListCommand extends Command {

  use EntityApiTrait;

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    protected readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    TranslationInterface $stringTranslation,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

    parent::__construct();

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {

    $table = new Table($output);
    $table->setHeaders([
      $this->t('ID'),
      $this->t('Name'),
      $this->t('Type'),
      $this->t('Bundle'),
    ]);
    $rows = [];
    foreach ($this->getAegirEntities() as $type => $entities) {
      foreach ($entities as $id => $entity) {

        // Fallback if a bundle isn't found.
        $bundleLabel = 'N/A';

        $wrappedEntity = $this->typedRepositoryManager->wrap($entity);

        if (
          is_null($wrappedEntity) ||
          !($wrappedEntity instanceof WrappedEntityInfoInterface)
        ) {
          continue;
        }

        $bundleEntity = $wrappedEntity->getBundleEntity();

        // Only try to get the bundle label if the entity actually has a bundle.
        //
        // @todo Should we output an error instead as all of our entities
        //   (currently) have bundles so this would be a failure state?
        if (!is_null($bundleEntity)) {
          $bundleLabel = $bundleEntity->label();
        }

        $rows[] = [
          $id,
          $entity->label(),
          $wrappedEntity->getEntityType()->getLabel(),
          $bundleLabel,
        ];
      }
    }
    $table
      ->setRows($rows)
      ->render();

    return Command::SUCCESS;

  }

}
