<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Command;

/**
 * Trait EntityApiTrait.
 *
 * @package Drupal\aegir_api
 */
trait EntityApiTrait {

  protected function getAegirBundles(): array {
    $bundles = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      foreach ($this->getBundleInfo($type) as $bundle => $info) {
        $bundles[$type] = [$bundle => $info['label']];
      }
    }
    return $bundles;
  }

  protected function getAegirBundlesByType(string $type): array {
    return $this->getAegirBundles()[$type];
  }

  protected function getBundleInfo(string $type): array {
    return $this->entityTypeBundleInfo->getBundleInfo($type);
  }

  protected function getAegirEntityTypeLabels(): array {
    $labels = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      # Cast to string.
      $labels[$type] = "{$entity_type->getLabel()}";
    }
    return $labels;
  }

  protected function getAegirEntityTypeLabel(string $type): string {
    return $this->getAegirEntityTypeLabels()[$type];
  }

  protected function getAegirBundleLabel(string $type, string $bundle): string {
    return $this->getAegirBundlesByType($type)[$bundle];
  }

  protected function getAegirEntityTypes(): array {
    $types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $type => $entity_type) {
      if ($this->isAnAegirEntityType($type)) {
        $types[$type] = $entity_type;
      }
    }
    return $types;
  }

  protected function isAnAegirEntityType(string $type): bool {
    # @TODO Use a class property (or similar) to determine relevant entity
    # types.
    return in_array($type, ['aegir_platform', 'aegir_site']);
  }

  protected function getAegirEntities(): array {
    $entities = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      $entities[$type] = $this->entityTypeManager->getStorage($type)->loadMultiple();
    }
    return $entities;
  }

  protected function getAegirEntityChoices(string $entity_type): array {
    $options = [];
    foreach ($this->getAegirEntities() as $type => $entities) {
      if ($type !== $entity_type) continue;
      foreach ($entities as $id => $entity) {
        $options[$id] = $entity->label();
      }
    }
    return $options;
  }

}
