<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Command;

use Drupal\aegir_api\Command\EntityApiTrait;
use Drupal\aegir_api\Command\OptionHandlingTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AegirCreateCommand.
 *
 * @package Drupal\aegir_api
 *
 * @TODO Add tests.
 */
#[AsCommand(name: 'aegir:create', description: 'Create Ægir entities.')]
class CreateCommand extends Command {

  use EntityApiTrait;

  use OptionHandlingTrait;

  use StringTranslationTrait;

  protected string|null $entity_type = null;
  protected string|null $bundle = null;
  protected string|null $name = null;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

    parent::__construct();

  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {

    $this->confirm = true;

    $this->options = ['entity_type', 'bundle', 'name'];

    $this
      ->addOption(
        'entity_type',
        null,
        InputOption::VALUE_REQUIRED,
        (string) $this->t('The entity type to create.'),
      )
      ->addOption(
        'bundle',
        null,
        InputOption::VALUE_REQUIRED,
        (string) $this->t('The entity bundle to create.'),
      )
      ->addOption(
        'name',
        null,
        InputOption::VALUE_REQUIRED,
        (string) $this->t('The name of the entity to create.'),
      );

  }

  /**
   * {@inheritdoc}
   */
  protected function initialize(
    InputInterface $input, OutputInterface $output,
  ): void {
    parent::initialize($input, $output);
    $this->initializeIo($input, $output);
    $this->registerOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(
    InputInterface $input, OutputInterface $output,
  ): void {
    $this->determineOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {

    if (!$this->confirmed()) return Command::FAILURE;
    if ($this->createEntity()) {
      $this->io()->success($this->translate('success'));

      return Command::SUCCESS;

    }
    else {
      $this->io()->error($this->translate('failure'));

      return Command::FAILURE;

    }

  }

  /**
   * Returns a translated UI string given a key.
   *
   * This currently exists as a replacement for the translations previously
   * stored in YAML files that were handled by Drupal Console.
   *
   * @param string $key
   *
   * @return string
   *
   * @todo Move these strings out of this method and to the code that calls
   *   them, and once the method is empty, delete it.
   */
  protected function translate(string $key): string {

    switch ($key) {

      case 'options.entity_type.prompt':
        return (string) $this->t(
          'What type of entity would you like to create?',
        );

      case 'options.entity_type.error':
        return (string) $this->t(
          'Type @entity_type is invalid.',
          ['@entity_type' => $this->entity_type],
        );

      case 'options.bundle.prompt':
        return (string) $this->t(
          'Which @entity_type bundle would you like to create?',
          ['@entity_type' => $this->getAegirEntityTypeLabel(
            $this->entity_type,
          )],
        );

      case 'options.bundle.error':
        return (string) $this->t(
          'Bundle @bundle is invalid.',
          ['@bundle' => $this->bundle],
        );

      case 'options.name.prompt':
        return (string) $this->t(
          'What is the name of the @bundle @entity_type to create?',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@bundle' => $this->getAegirBundleLabel(
              $this->entity_type, $this->bundle,
            ),
          ],
        );

      case 'confirm':
        return (string) $this->t(
          'You are about to create a @bundle @entity_type entity name "@name". Proceed?',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@bundle' => $this->getAegirBundleLabel(
              $this->entity_type, $this->bundle,
            ),
            '@name' => $this->name,
          ],
        );

      case 'success':
        return (string) $this->t(
          'Created a @bundle @entity_type named "@name".',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@bundle' => $this->getAegirBundleLabel(
              $this->entity_type, $this->bundle,
            ),
            '@name' => $this->name,
          ],
        );

      case 'failure':
        return (string) $this->t(
          'Failed to create a @bundle @entity_type named "@name".',
          [
            '@entity_type' => $this->getAegirEntityTypeLabel(
              $this->entity_type,
            ),
            '@bundle' => $this->getAegirBundleLabel(
              $this->entity_type, $this->bundle,
            ),
            '@name' => $this->name,
          ],
        );

    }

  }

  protected function promptForOption(string $option): string {
    switch ($option) {
      case 'entity_type':
        $choices = $this->getAegirEntityTypeLabels();
        return $this->chooseOption($option, $choices);
      case 'bundle':
        $choices = $this->getAegirBundlesByType($this->entity_type);
        return $this->chooseOption($option, $choices);
      case 'name':
        return $this->askOption($option);
      default:
        # TODO Throw an appropriate exception.
        return '';
    }
  }

  protected function getReplacement(string $option): string {
    switch ($option) {
      case 'entity_type':
        return $this->getEntityTypeLabel();
      case 'bundle':
        return $this->getBundleLabel();
      case 'name':
        return $this->name;
      default:
        # TODO Throw an appropriate exception.
        return '';
    }
  }

  protected function getEntityTypeLabel(): string {
    return $this->getAegirEntityTypeLabels()[$this->entity_type];
  }

  protected function getBundleLabel(): string {
    return $this->getAegirBundlesByType($this->entity_type)[$this->bundle];
  }

  /**
   * Create an entity.
   *
   * @see \Drupal\Core\Entity\EntityInterface::save()
   *   Return value is of this.
   */
  protected function createEntity(): int {
    $type = $this->getAegirEntityTypes()[$this->entity_type];
    $bundle_key = $type->getKey('bundle');
    $label_key = $type->getKey('label');

    return $this
      ->entityTypeManager
      ->getStorage($this->entity_type)
      ->create([
        $bundle_key => $this->bundle,
        $label_key => $this->name,
      ])
      ->save();
  }

}
