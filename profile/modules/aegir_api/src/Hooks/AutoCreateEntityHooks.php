<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Hooks;

use Drupal\aegir_api\Value\AutoCreateChildEntityFieldsInfo;
use Drupal\aegir_api\WrappedEntities\WrappedEntityWithAutoCreateInfoInterface;
use Drupal\aegir_api\WrappedEntityVariants\AegirEntityWithAutoCreateParentVariantCondition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\hux\Attribute\Hook;
use Drupal\typed_entity\EntityWrapperInterface;
use Drupal\typed_entity\TypedEntityContext;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Auto create entity hook implementations.
 */
class AutoCreateEntityHooks {

  use StringTranslationTrait;

  /**
   * Constructs this hook; saves dependencies.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedEntityRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    TranslationInterface $stringTranslation,
    #[Autowire(service: 'Drupal\typed_entity\RepositoryManager')]
    protected readonly EntityWrapperInterface $typedEntityRepositoryManager,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices. Unfortunately, we
    // can't do this via constructor property promotion because
    // StringTranslationTrait doesn't yet type hint the interface on the
    // property, which means it's incompatible with autowiring at the time of
    // writing on 10.3.x.
    $this->setStringTranslation($stringTranslation);

  }

  #[Hook('entity_base_field_info')]
  /**
   * Implements \hook_entity_base_field_info() for auto create entities.
   *
   * This attempts to automagically create the appropriate base fields on
   * entities that can have a parent auto create them to store the parent
   * reference.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type the hook is being invoked for.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by field name.
   *
   * @see \Drupal\aegir_api\WrappedEntityVariants\AegirEntityWithAutoCreateParentVariantCondition
   *   Encapsulates the logic to determine if an entity definition matches our
   *   criteria.
   */
  public function baseFieldInfo(EntityTypeInterface $entityType): array {

    /** @var \Drupal\typed_entity\WrappedEntityVariants\VariantConditionInterface */
    $condition = new AegirEntityWithAutoCreateParentVariantCondition(
      new TypedEntityContext(['entity_definition' => $entityType]),
      $this->getStringTranslation(),
    );

    $result = $condition->evaluate();

    if ($result === false) {
      return [];
    }

    $fieldInfo = AutoCreateChildEntityFieldsInfo::createFromEntityType(
      $entityType,
    );

    $referencingField = BaseFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Referencing entity'))
      ->setDescription($this->t('The entity that references this one.'));

    $referencingTypeField = BaseFieldDefinition::create('string')
      ->setLabel($this->t('Referencing entity type'))
      ->setDescription($this->t(
        'The type of entity that references this one.',
      ));

    return [
      $fieldInfo->getEntityIdFieldName()    => $referencingField,
      $fieldInfo->getEntityTypeFieldName()  => $referencingTypeField,
    ];

  }

  #[Hook('entity_presave')]
  /**
   * Implements \hook_entity_presave() to handle auto create entity fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being created or updated.
   *
   * @todo Replace interface check with the auto create child variant condition.
   *
   * @see \Drupal\aegir_api\WrappedEntityVariants\AegirEntityWithAutoCreateChildrenVariantCondition
   */
  public function preSave(EntityInterface $entity): void {

    /** @var \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null */
    $wrappedEntity = $this->typedEntityRepositoryManager->wrap($entity);

    // Return here if no wrapped entity was created or the entity does not have
    // auto create reference fields.
    if (
      $wrappedEntity === null ||
      !($wrappedEntity instanceof WrappedEntityWithAutoCreateInfoInterface) ||
      !$wrappedEntity->hasAutoCreateFields()
    ) {
      return;
    }

    // Create referenced entities if they don't already exist.
    $wrappedEntity->createReferencedEntities();

  }

  #[Hook('entity_insert')]
  /**
   * Implements \hook_entity_insert() to handle auto create entity fields.
   *
   * This functions as a post save hook for newly created entities, despite the
   * name difference.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being created or updated.
   *
   * @todo Replace interface check with the auto create child variant condition.
   *
   * @see \Drupal\aegir_api\WrappedEntityVariants\AegirEntityWithAutoCreateChildrenVariantCondition
   */
  public function postSave(EntityInterface $entity): void {

    /** @var \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null */
    $wrappedEntity = $this->typedEntityRepositoryManager->wrap($entity);

    // Return here if no wrapped entity was created or the entity does not have
    // auto create reference fields.
    if (
      $wrappedEntity === null ||
      !($wrappedEntity instanceof WrappedEntityWithAutoCreateInfoInterface) ||
      !$wrappedEntity->hasAutoCreateFields()
    ) {
      return;
    }

    $wrappedEntity->updateReferencedEntities();

  }

  #[Hook('entity_predelete')]
  /**
   * Implements \hook_entity_predelete() to handle auto create entity fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity about to be deleted.
   *
   * @todo Replace interface check with the auto create child variant condition.
   *
   * @see \Drupal\aegir_api\WrappedEntityVariants\AegirEntityWithAutoCreateChildrenVariantCondition
   */
  public function preDelete(EntityInterface $entity): void {

    /** @var \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null */
    $wrappedEntity = $this->typedEntityRepositoryManager->wrap($entity);

    // Return here if no wrapped entity was created or the entity does not have
    // auto create reference fields.
    if (
      $wrappedEntity === null ||
      !($wrappedEntity instanceof WrappedEntityWithAutoCreateInfoInterface) ||
      !$wrappedEntity->hasAutoCreateFields()
    ) {
      return;
    }

    $wrappedEntity->deleteReferencedEntities();

  }

}
