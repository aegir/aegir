<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Hooks;

use Drupal\Core\Form\FormStateInterface;
use Drupal\hux\Attribute\Alter;
use function array_key_exists;
use function in_array;

/**
 * Field widget hooks.
 */
class FieldWidgetHooks {

  /**
   * Entity types that we edit inline.
   *
   * @var string[]
   *
   * @todo Move these to wrapped entity classes so we can just wrap and call a
   *   method to find out if it wants the Inline Entity Form widget altered.
   */
  protected array $inlineEntityTypes = ['aegir_operation', 'aegir_task'];

  #[Alter('field_widget_single_element_form')]
  /**
   * Alters the Inline Entity Form widget to not display as fieldset.
   *
   * @param array &$element
   *   The field widget form element.
   *
   * @param \Drupal\Core\Form\FormStateInterface &$formState
   *   The current state of the form.
   *
   * @param array $context
   *   Widget context information.
   *
   * @see \hook_field_widget_single_element_form_alter()
   */
  public function alterInlineEntityFormWidget(
    array &$element, FormStateInterface &$formState, array $context,
  ): void {

    // Hook is disabled for the time being because removing the fieldsets
    // obfuscates what tasks are being run and can introduce confusing
    // situtations where it's unclear if a field belongs to an operation or a
    // task; name fields can be especially confusing in this regard. Ideally,
    // this should be solved in a way that reduces clutter (as was probably the
    // original intent), but also makes this information optionally available -
    // for example in a <details> that's closed by default.
    return;

    if (
      !array_key_exists('inline_entity_form', $element) ||
      !in_array(
        $element['inline_entity_form']['#entity_type'],
        $this->inlineEntityTypes,
      )
    ) {
      return;
    }

    // Convert the fieldset from the Inline Entity Form widget to a plain
    // container.
    $element['#type'] = 'container';

  }

}
