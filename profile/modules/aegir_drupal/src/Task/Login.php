<?php

declare(strict_types=1);

namespace Drupal\aegir_drupal\Task;

use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\aegir_api\Logger\AegirLoggerTrait;
use Drupal\aegir_api\Entity\AbstractOutputTask;
use Drupal\aegir_api\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller.
 *
 * Returns responses for Ægir site routes.
 *
 * @package Drupal\aegir_site\Entity
 */
// @TODO Move most of this into a parent class.
class Login { # extends AbstractOutputTask {

  use StringTranslationTrait, AegirLoggerTrait;

  /**
   * The label to identify our log messages.
   */
  protected $logLabel = 'aegir_drupal_login';

  /**
   * A description of the task.
   */
  protected $description = 'Login to Drupal site';

  /**
   * The output from the task.
   */
  protected $output = '';

  /**
   * The task to run on the backend.
   */
  protected $backendTask = 'dispatcherd.drupal_login';

  /**
   * The constructor for AbstractOutputTask objects.
   */
  public function __construct() {
    $this->queue = \Drupal::service('aegir_queue.task_queue');
    $this->log = \Drupal::logger($this->logLabel);
  }

  /**
   * Fetch the site's login link and redirect to it.
   */
  public function login(string $site_id, Request $request) {
    $site = $this->getSiteEntity($site_id);
    if (!$site) {
      return $this->log()->error('Failed to load site with ID :id', [
        ':id' => $site_id,
      ]);
    }

    $result = $this->runTask($site->marshal());
    if (!$result || is_null($this->output)) {
      // Redirect back to the referring page, log and print error message.
      $this->log()->error('Failed to retrieve login link to site %site', [
        '%site' => $site->label(),
      ]);
      $referer = \Drupal::request()->headers->get('referer');
      return new RedirectResponse($referer);
    }

    $this->log()->info('Redirecting to login link for %site', [
      '%site' => $site->label(),
    ]);
    return new TrustedRedirectResponse($this->output);
  }

  /**
   * Return a site entity from its entity ID.
   */
  protected function getSiteEntity(string $site_id) {
    $entity = entity_load('aegir_site', $site_id);
    // @todo Switch to checking for an interface.
    if ($entity instanceof \Drupal\aegir_site\Entity\Entity) {
      return $entity;
    }
    $this->log()->error('Failed to load site entity from ID: %id.', [
      '%id' => $site_id,
    ]);
    return FALSE;
  }

  /**
   * Run a task and save its output.
   *
   * @param array $context
   *   Information to pass to the backend task.
   */
  protected function runTask(array $context) {
    $task = $this->queue->addTask($this->backendTask, [$context]);
    if ($task == FALSE) {
      $this->log()->error('Failed to dispatch the %task task.', [
        '%task' => $this->description,
      ]);
      return FALSE;
    }

    $this->waitForTaskToComplete($task);

    if (!$task->isSuccess()) {
      $this->log()->error('%status: %task task failed. Output: :output', [
        '%status' => $task->getStatus(),
        '%task' => $this->description,
        ':output' => serialize($task->getResult()),
      ]);
      return FALSE;
    }

    $this->output = $task->getResult();
    return TRUE;
  }

  /**
   * Poll the queue until the task is ready.
   */
  protected function waitForTaskToComplete($task) {
    $counter = 0;
    // @TODO Make these configurable?
    $increment = 100000;
    $timeout = 5000000;
    while (!$task->isReady()) {
      usleep($increment);
      $counter += $increment;
      if ($counter >= $timeout) {
        $this->log()->error('Task execution exceeded timeout (:timeout seconds).', [
          ':timeout' => $timeout / 1000000,
        ]);
      }
    }
  }

}
