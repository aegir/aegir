@examples @types @api
Feature: Create and manage Aegir example types
  In order to define example types that can be combined into applications,
  as a example manager,
  I want to be able to manage Aegir example bundles.

  Background:
    Given I run "drush -y pm:install aegir_test_example"
      And I am logged in as a "Example manager"
      And I am on "admin/aegir/examples/types"

  Scenario: Create example types
     When I click "Add Ægir example type"
      And I fill in "Name" with "TEST_EXAMPLE_TYPE_NEW"
      And I fill in "Machine-readable name" with "test_example_type_new"
      And I press the "Save" button
     Then I should be on "admin/aegir/examples/types"
      And I should see the success message "Created the TEST_EXAMPLE_TYPE_NEW example type."

  Scenario: Update example types
     When I click "Edit" in the "TEST_EXAMPLE_TYPE_NEW" row
     Then I should see the heading "Edit example type" in the "header" region
     When I fill in "Name" with "TEST_EXAMPLE_TYPE_CHANGED"
      And I press the "Save" button
     Then I should be on "admin/aegir/examples/types"
     Then I should see the success message "Saved the TEST_EXAMPLE_TYPE_CHANGED example type."

  Scenario: Delete example types
     Then I should see the link "TEST_EXAMPLE_TYPE_CHANGED"
     When I click "Edit" in the "TEST_EXAMPLE_TYPE_CHANGED" row
     Then I should see the heading "Edit example type" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete the example type TEST_EXAMPLE_TYPE_CHANGED?" in the "header" region
     When I press the "Delete" button
     Then I should be on "admin/aegir/examples/types"
     Then I should see the success message "The example type TEST_EXAMPLE_TYPE_CHANGED has been deleted."
