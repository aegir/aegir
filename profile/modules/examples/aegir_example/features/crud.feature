@examples @entities @api
Feature: Create and manage Aegir examples
  In order to define examples that can be combined into applications,
  as a example manager,
  I want to be able to manage Aegir example entities.

  Background:
    Given I run "drush -y pm:install aegir_test_example"
      And I am logged in as a "Example manager"
      And I am on "admin/aegir/examples"

  Scenario: Create examples
     When I click "Add Ægir example"
     Then I should be on "admin/aegir/examples/add"
      And I should see the heading "Add example" in the "header" region
      And I should see the link "TEST_EXAMPLE_TYPE_1"
      And I should see the link "TEST_EXAMPLE_TYPE_2"
     When I click "TEST_EXAMPLE_TYPE_1" in the "content" region
     Then I should be on "admin/aegir/examples/add/test_example_type_1"
     When I fill in "Name" with "TEST_EXAMPLE_A"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_EXAMPLE_A example."
     When I click "TEST_EXAMPLE_A" in the "content" region
      And I should see the link "View" in the "tabs" region
      And I should see the link "Edit" in the "tabs" region
      And I should see the link "Revisions" in the "tabs" region
      And I should see the link "Delete" in the "tabs" region

  Scenario: Update examples
    Given I should see the link "TEST_EXAMPLE_A" in the "content" region
      And I click "TEST_EXAMPLE_A"
     When I click "Edit" in the "tabs" region
      And for "Revision log message" I enter "Test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_EXAMPLE_A example."

  Scenario: Delete examples
    Given I click "TEST_EXAMPLE_A" in the "content" region
     When I click "Delete" in the "tabs" region
     Then I should see the heading "Are you sure you want to delete the example TEST_EXAMPLE_A?" in the "header" region
     When I press the "Delete" button
     Then I should see the success message "The example TEST_EXAMPLE_A has been deleted."
      And I should be on "admin/aegir/examples"
      And I should not see the link "TEST_EXAMPLE_A"
