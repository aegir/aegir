@examples @access @api
Feature: Access to Aegir examples and types
  In order to define examples that can be combined into applications,
  as a example manager,
  I need to be able to access Aegir example entities and types.

  Background:
    Given I run "drush -y pm:install aegir_test_example"

  Scenario: Anonymous users should not have access to examples configuration.
    Given I am not logged in
     When I am on "admin/aegir"
     Then I should be on "user/login"
     Then I should see "Forgot your password?"

  Scenario: Authenticated users should not have access to examples configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Examples"

  Scenario: Example managers should have access to examples configuration.
    Given I am logged in as a "Example manager"
     When I am on "admin/aegir"
     Then I should see the link "Examples"
      And I should see the text "Ægir example entities and bundles"
     When I click "Examples"
     Then I should be on "admin/aegir/examples"
      And I should see the heading "Examples" in the "header" region
      And I should see the link "Add Ægir example"
      And I should see the link "Example types"
     When I click "Example types"
     Then I should be on "admin/aegir/examples/types"
      And I should see the heading "Example types" in the "header" region
      And I should see the link "Add Ægir example type"
