<?php

declare(strict_types=1);

namespace Drupal\aegir_example\Entity;

use Drupal\aegir_api\Entity\AegirEntityBase;
use Drupal\aegir_example\Entity\ExampleInterface;

/**
 * Defines the Ægir Example entity.
 *
 * @ingroup aegir_example
 *
 * @ContentEntityType(
 *   id               = "aegir_example",
 *   label            = @Translation("Example"),
 *   label_collection = @Translation("Examples"),
 *   label_singular   = @Translation("example"),
 *   label_plural     = @Translation("examples"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count example",
 *     plural   = "@count examples",
 *   ),
 *   bundle_label = @Translation("Example type"),
 *   handlers     = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-delete" = "Drupal\Core\Entity\Form\RevisionDeleteForm",
 *       "revision-revert" = "Drupal\Core\Entity\Form\RevisionRevertForm",
 *     },
 *     "access"         = "Drupal\aegir_api\Entity\AegirEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html"     = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     },
 *     "translation"  = "Drupal\content_translation\ContentTranslationHandler",
 *     "views_data"   = "Drupal\views\EntityViewsData",
 *   },
 *   base_table       = "aegir_example",
 *   data_table       = "aegir_example_field_data",
 *   revision_table   = "aegir_example_revision",
 *   revision_data_table = "aegir_example_field_revision",
 *   translatable     = true,
 *   show_revision_ui = true,
 *   admin_permission = "administer aegir example entities",
 *   entity_keys      = {
 *     "id"         = "id",
 *     "revision"   = "revision_id",
 *     "bundle"     = "type",
 *     "label"      = "name",
 *     "uuid"       = "uuid",
 *     "owner"      = "user_id",
 *     "langcode"   = "langcode",
 *     "published"  = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"        = "/admin/aegir/examples/{aegir_example}",
 *     "add-page"         = "/admin/aegir/examples/add",
 *     "add-form"         = "/admin/aegir/examples/add/{aegir_example_type}",
 *     "edit-form"        = "/admin/aegir/examples/{aegir_example}/edit",
 *     "delete-form"      = "/admin/aegir/examples/{aegir_example}/delete",
 *     "version-history"  = "/admin/aegir/examples/{aegir_example}/revisions",
 *     "revision"         = "/admin/aegir/examples/{aegir_example}/revisions/{aegir_example_revision}/view",
 *     "revision-revert-form" = "/admin/aegir/examples/{aegir_example}/revisions/{aegir_example_revision}/revert",
 *     "revision-delete-form" = "/admin/aegir/examples/{aegir_example}/revisions/{aegir_example_revision}/delete",
 *     "collection"       = "/admin/aegir/examples",
 *   },
 *   bundle_entity_type   = "aegir_example_type",
 *   field_ui_base_route  = "entity.aegir_example_type.canonical",
 * )
 */
class Example extends AegirEntityBase implements ExampleInterface {}
