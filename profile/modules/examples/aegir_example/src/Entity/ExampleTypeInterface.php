<?php

declare(strict_types=1);

namespace Drupal\aegir_example\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;

/**
 * Interface for Ægir Example type entities.
 */
interface ExampleTypeInterface extends AegirEntityTypeInterface {}
