<?php

declare(strict_types=1);

namespace Drupal\aegir_example\Entity;

use Drupal\aegir_api\Entity\AegirEntityInterface;

/**
 * Interface for Ægir Example entities.
 */
interface ExampleInterface extends AegirEntityInterface {}
