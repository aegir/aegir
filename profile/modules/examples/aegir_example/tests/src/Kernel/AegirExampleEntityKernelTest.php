<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_example\Kernel;

use Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ægir Example entity kernel tests.
 *
 * @group aegir
 *
 * @group aegir_example
 */
class AegirExampleEntityKernelTest extends AegirEntityBasicKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_example',
    'aegir_test_example',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_example');

  }

  /**
   * Data provider for AegirEntityBasicKernelTestBase::testAegirEntityBasics().
   *
   * @return array
   *
   * @see \Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase::testAegirEntityBasics()
   */
  public static function aegirEntityBasicsProvider(): array {

    return [
      [
        'entityTypeId'  => 'aegir_example',
        'managerRole'   => 'aegir_example_manager',

        'values' => [
          // Use one of the bundles exported to the 'aegir_test_example' module.
          'type'    => 'test_example_type_1',
          'name'    => 'TEST_EXAMPLE_A',
          'status'  => 1,
        ],
      ],
    ];

  }

}
