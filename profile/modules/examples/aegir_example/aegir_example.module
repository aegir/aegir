<?php

declare(strict_types=1);

/**
 * @file
 * Contains aegir_example.module..
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function aegir_example_help(
  string $route_name, RouteMatchInterface $route_match,
): string|array {
  switch ($route_name) {
    // Main module help for the aegir_example module.
    case 'help.page.aegir_example':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('A example definition used to compose an operation.') . '</p>';
      return $output;

    default:
      return '';
  }
}

/**
 * Implements hook_theme().
 */
function aegir_example_theme(): array {
  $theme = [];
  $theme['aegir_example'] = [
    'render element' => 'elements',
    'file' => 'aegir_example.page.inc',
    'template' => 'aegir_example',
  ];
  $theme['aegir_example_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'aegir_example.page.inc',
  ];
  return $theme;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function aegir_example_theme_suggestions_aegir_example(
  array $variables,
): array {
  $suggestions = [];
  $entity = $variables['elements']['#aegir_example'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'aegir_example__' . $sanitized_view_mode;
  $suggestions[] = 'aegir_example__' . $entity->bundle();
  $suggestions[] = 'aegir_example__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'aegir_example__' . $entity->id();
  $suggestions[] = 'aegir_example__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}
