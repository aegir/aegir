<?php

declare(strict_types=1);

/**
 * @file
 * Contains aegir_example.page.inc.
 *
 * Page callback for Ægir Example entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ægir example templates.
 *
 * Default template: aegir_example.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_aegir_example(array &$variables): void {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
