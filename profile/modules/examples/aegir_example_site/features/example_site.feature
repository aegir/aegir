@example @example-site @api @javascript @disable-on-ci
Feature: Example site setup
  In order to instantiate a new example site
  as an Ægir administrator,
  I need to be able to create and deploy a Example site.

  Background:
    Given I am logged in as an "Ægir administrator"
      And I am on "admin/aegir/sites"

  Scenario: Create new Example site instance.
    Given I click "Add Ægir site"
     Then I should be on "admin/aegir/sites/add"
      And I should see "Add site"
      And I should see the link "Example site" in the "content" region
     When I click "Example site" in the "content" region
     Then I should be on "admin/aegir/sites/add/example_site"
      And I should see "Name"
      And I should see "Add Example site"
      # @todo This is displayed as a tooltip in the Eldir theme rather than as
      #   an inline description so will fail; re-enable if/when theme is rebuilt
      #   to use inline descriptions.
      # And I should see "The name of the entity"
      And I should see "Debug data"
     When I enter "Example site 1" for "Name"
      And I enter "Example content" for "Debug data"
      And I press "Save"
     Then I should see "Created the Example site 1 site."
     When I go to "aegir/sites"
      And I click "Example site 1" in the "content" region
     Then I should see "Log debug data" in the "content" region
     When I run the "Log debug data" operation
      And I wait "10" seconds
      And I view the "Log debug data" operation
      And I wait for AJAX to finish
     Then I should see "STATIC LOG DATA" in the "drupal_modal" region
      And I should see "Log data: Example content" in the "drupal_modal" region
      And I should see "TASK [example_print_debug_data : Print current debug data.]" in the "drupal_modal" region
      And I should see "field_example_site_debug_data: Example content" in the "drupal_modal" region
