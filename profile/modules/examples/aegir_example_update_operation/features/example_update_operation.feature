@example @example-update-operation @api @javascript @disable-on-ci
Feature: Example operation to update debug data.
  In order to alter data on an example site
  as an Ægir administrator,
  I need to be able to enter new data into an update operation.

  Background:
    Given I am logged in as an "Ægir administrator"
      And I am on "admin/aegir/sites"

  Scenario: Create new Example site instance.
    Given I click "Add Ægir site"
     Then I should be on "admin/aegir/sites/add"
      And I should see "Add site"
      And I should see the link "Example site with update" in the "content" region
     When I click "Example site with update" in the "content" region
     Then I should be on "admin/aegir/sites/add/example_site_update"
      And I should see "Name"
      And I should see "Add Example site with update"
      # @todo This is displayed as a tooltip in the Eldir theme rather than as
      #   an inline description so will fail; re-enable if/when theme is rebuilt
      #   to use inline descriptions.
      # And I should see "The name of the entity"
      And I should see "Debug data"
     When I enter "Example site update" for "Name"
      And I enter "Example content (before)" for "Debug data"
      And I press "Save"
     Then I should see "Created the Example site update site."
      And I should see "Example site update"
     When I am on "aegir/sites"
      And I click "Example site update" in the content region
     Then I should see "Log debug data"
      And I should see "Update debug data"
      And I click "Run" in the "Update debug data" row
      And I enter "123" for "New data"
      And I press "Dispatch" in the "modal_buttons" region
      # Then the operation should run
      # The run button is disabled
      # The gears turn
      # We ought to be able to click View and see the log scroll by
      #
      # Wait for the operation to run
      And I wait "5" seconds
      #The "Log Debug Data Operation" row status should be "done"
     When I view the "Update debug data" operation
      And I wait for AJAX to finish
      #And I wait "5" seconds
     Then I should see "Operation log" in the "drupal_modal" region
      And I should see "TASK [example_write_debug_data : Print current debug data.]"
      And I should see "TASK [example_write_debug_data : Print new debug data.]"
      And I should see "TASK [example_write_debug_data : Write new data to the debug data field in the front-end.]"
      And I should see "field_example_new_data: 123"
