@operations @entities @api
Feature: Create and manage Aegir operations
  In order to define operations that can be combined into applications,
  as a operation manager,
  I want to be able to manage Aegir operation entities.

  Background:
    Given I run "drush -y pm:install aegir_test_operation"
      And I am logged in as a "Operation manager"
      And I am on "admin/aegir/operations"

  Scenario: Create operations
     When I click "Add Ægir operation"
     Then I should be on "admin/aegir/operations/add"
      And I should see the heading "Add operation" in the "header" region
      And I should see the link "TEST_OPERATION_TYPE_1"
      And I should see the link "TEST_OPERATION_TYPE_2"
     When I click "TEST_OPERATION_TYPE_1" in the "content" region
     Then I should be on "admin/aegir/operations/add/test_operation_type_1"
     When I fill in "Name" with "TEST_OPERATION_A"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_OPERATION_A operation."
     When I click "TEST_OPERATION_A"
     Then I should see the link "View" in the "tabs" region
      And I should see the link "Edit" in the "tabs" region
      And I should see the link "Revisions" in the "tabs" region
      And I should see the link "Delete" in the "tabs" region

  Scenario: Update operations
    Given I should see the link "TEST_OPERATION_A" in the "content" region
      And I click "TEST_OPERATION_A"
     When I click "Edit" in the "tabs" region
      And for "Revision log message" I enter "Test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_OPERATION_A operation."

  Scenario: Delete operations
    Given I click "TEST_OPERATION_A" in the "content" region
     When I click "Edit" in the "tabs" region
     Then I should see the heading "Edit TEST_OPERATION_A" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete the operation TEST_OPERATION_A?" in the "header" region
     When I press the "Delete" button
     Then I should see the success message "The operation TEST_OPERATION_A has been deleted."
      And I should be on "admin/aegir/operations"
      And I should not see the link "TEST_OPERATION_A"
