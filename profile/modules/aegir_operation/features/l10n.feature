@operations @l10n @api @javascript
Feature: Translate Aegir operations
  In order to allow usage in multiple languages,
  as a operation manager,
  I want to be able to translate Aegir operation entities.

  Background:
    Given I am logged in as a "Operation manager"

  Scenario: Enable a new language (French)
    Given I run "drush -y pm:install aegir_test_operation"
      And I am logged in as an "Administrator"
      And I am on "admin/config/regional/language"
     When I click "Add language" in the "content" region
     Then I should see the heading "Add language"
     When I select "French" from "Language name"
      And I press the "Add language" button
      And I wait for the batch job to finish
     Then I should see the success message "The language French has been created and can now be used."
      And I am on "admin/config/regional/content-language"
     When I check the box "Operation"
      # The "Site" form wrapper is a <details> that starts closed, so we have to
      # open it. The <summary> has role="button" which why the below works.
      And I press the "Operation" button
      And I check the box "settings[aegir_operation][test_operation_type_1][translatable]"
      And I check the box "settings[aegir_operation][test_operation_type_1][fields][user_id]"
      And I check the box "settings[aegir_operation][test_operation_type_1][settings][language][language_alterable]"
      And I press the "Save configuration" button
     Then I should see the success message "Settings successfully updated."

  Scenario: Create English operation
    Given I am on "admin/aegir/operations/add/test_operation_type_1"
     When I fill in "Name" with "TEST_OPERATION_ENG"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_OPERATION_ENG operation."

  Scenario: Translate operation
    Given I am on "admin/aegir/operations"
     Then I should see the link "TEST_OPERATION_ENG" in the "content" region
     When I click "TEST_OPERATION_ENG"
     Then I should see the link "Translate" in the "tabs" region
     When I click "Translate"
      And I click "Add" in the "French" row
     Then I should see the heading "Traduire TEST_OPERATION_ENG en French"
     When I fill in "Nom" with "TEST_OPERATION_FR"
      And I click "Informations sur les versions"
      And for "Message du journal de version (toutes les langues)" I enter "Première message."
      And I press the "Enregistrer" button
     Then I should see the success message "Saved the TEST_OPERATION_FR operation."
      And I am on "fr/admin/aegir/operations"

  Scenario: Update the translated operation to create some revisions
    Given I am on "fr/admin/aegir/operations"
     When I click "Traduire" in the "TEST_OPERATION_ENG" row
      And I click "TEST_OPERATION_FR" in the "content" region
     Then the url should match "fr/admin/aegir/operations/[0-9]+?"
     When I click "Modifier" in the "tabs" region
     Then I should see the heading "TEST_OPERATION_FR [traduction French]" in the "header" region
      And I click "Informations sur les versions"
      And for "Message du journal de version (toutes les langues)" I enter "Deuxième message."
      And I press the "Enregistrer" button
     Then I should see the success message "Saved the TEST_OPERATION_FR operation."
      And I am on "fr/admin/aegir/operations"

  Scenario: Revert a translated operation revision
    Given I am on "fr/admin/aegir/operations"
     When I click "Traduire" in the "TEST_OPERATION_ENG" row
      And I click "TEST_OPERATION_FR" in the "content" region
      And I click "Versions"
     Then I should see the text "Première message." in the "content" region
      And I should see the text "Deuxième message." in the "content" region
     When I click "Rétablir" in the "Première message." row
     Then I should see the text "Êtes-vous sûr(e) de vouloir rétablir la version du" in the "header" region
     When I press "Rétablir"
     Then I should see the success message "TEST_OPERATION_TYPE_1 TEST_OPERATION_FR a été rétabli(e) à la version du"

  Scenario: Delete operation translation
    Given I am on "admin/aegir/operations"
     When I click "TEST_OPERATION_ENG"
      And I click "Translate"
      And I click "Delete" in the "French" row
     Then I should see the heading "Êtes-vous certain de vouloir supprimer la traduction French de l'entité operation TEST_OPERATION_FR ?"
     When I press "Supprimer la traduction French"
     Then I should see the success message "La traduction French de l'entité operation TEST_OPERATION_FR a été supprimée."
