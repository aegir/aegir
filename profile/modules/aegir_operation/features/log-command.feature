@api @command @aegir:log
Feature: 'aegir:log' Drush command allows setting a value.
  In order to get log data into the front-end
  As a queue worker
  I need a console command that can write to Operation log fields.

  Background:
    Given I run "drush -y pm:install aegir_test_operation aegir_log_command_test aegir_input_command_test"
    And I am logged in as an "Administrator"
    And "log_command_test" operations:
    | id  | type             |  name                            | uuid                                 |
    | 234 | log_command_test | Fixture command test operation 1 | c34ac018-16e3-445c-b9d1-a9608191b12c |
    | 235 | log_command_test | Fixture command test operation 2 | fd21cc1a-3d1c-4d03-a392-7111b1236151 |

  Scenario: 'aegir:log' command logs site data.
     # @TODO: Validate arguments/options
     When I run "drush aegir:log --uri=aegir.ddev.site --root=/var/www/html c34ac018-16e3-445c-b9d1-a9608191b12c 123 'foo' 123"
     When I run "drush aegir:log --uri=aegir.ddev.site --root=/var/www/html c34ac018-16e3-445c-b9d1-a9608191b12c 124 'bar' 124"
     When I run "drush aegir:log --uri=aegir.ddev.site --root=/var/www/html c34ac018-16e3-445c-b9d1-a9608191b12c 125 'baz' 125"
     When I run "drush aegir:exitcode --uri=aegir.ddev.site --root=/var/www/html c34ac018-16e3-445c-b9d1-a9608191b12c 0"
     When I run "drush aegir:log --uri=aegir.ddev.site --root=/var/www/html fd21cc1a-3d1c-4d03-a392-7111b1236151 123 'foo' 123"
     When I run "drush aegir:log --uri=aegir.ddev.site --root=/var/www/html fd21cc1a-3d1c-4d03-a392-7111b1236151 125 'bar' 125"
     When I run "drush aegir:log --uri=aegir.ddev.site --root=/var/www/html fd21cc1a-3d1c-4d03-a392-7111b1236151 124 'baz' 124"
     When I run "drush aegir:exitcode --uri=aegir.ddev.site --root=/var/www/html fd21cc1a-3d1c-4d03-a392-7111b1236151 1"
     Then I should not see "No operation found with UUID"
     When I am on "/admin/aegir/operations/test-log"
     Then I should see "foo,bar,baz" in the "234" row
      And I should see "0" in the "234" row
     # @TODO: Fix this. We would expect the sequence (or timestamp) to order these differently
     Then I should see "foo,bar,baz" in the "235" row
     #Then I should see "foo,baz,bar" in the "235" row
      And I should see "1" in the "235" row
