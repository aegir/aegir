@operations @access @api
Feature: Access to Aegir operations and types
  In order to define operations that can be combined into applications,
  as a operation manager,
  I need to be able to access Aegir operation entities and types.

  Scenario: Anonymous users should not have access to operations configuration.
    Given I am not logged in
     When I am on "admin/aegir"
     Then I should be on "user/login"
     Then I should see "Forgot your password?"

  Scenario: Authenticated users should not have access to operations configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Operations"

  Scenario: Operation managers should have access to operations configuration.
    Given I am logged in as a "Operation manager"
     When I am on "admin/aegir"
     Then I should see the link "Operations" in the "content" region
      And I should see the text "Ægir operation entities and bundles" in the "content" region
     When I click "Operations" in the "content" region
     Then I should be on "admin/aegir/operations"
      And I should see the heading "Operations" in the "header" region
      And I should see the link "Add Ægir operation"
      And I should see the link "Operation types"
     When I click "Operation types"
     Then I should be on "admin/aegir/operations/types"
      And I should see the heading "Operation types" in the "header" region
      And I should see the link "Add Ægir operation type"

