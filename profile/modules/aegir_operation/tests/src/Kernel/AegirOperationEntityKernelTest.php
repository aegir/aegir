<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_operation\Kernel;

use Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Ægir Operation entity kernel tests.
 *
 * @group aegir
 *
 * @group aegir_operation
 */
class AegirOperationEntityKernelTest extends AegirEntityBasicKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'aegir_api',
    'aegir_operation',
    'aegir_test_operation',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('aegir_operation');

  }

  /**
   * Data provider for AegirEntityBasicKernelTestBase::testAegirEntityBasics().
   *
   * @return array
   *
   * @see \Drupal\Tests\aegir_api\Kernel\AegirEntityBasicKernelTestBase::testAegirEntityBasics()
   */
  public static function aegirEntityBasicsProvider(): array {

    return [
      [
        'entityTypeId'  => 'aegir_operation',
        'managerRole'   => 'aegir_operation_manager',

        'values' => [
          // Use one of the bundles exported to the 'aegir_test_operation'
          // module.
          'type'    => 'test_operation_type_1',
          'name'    => 'TEST_OPERATION_A',
          'status'  => 1,
        ],
      ],
    ];

  }

}
