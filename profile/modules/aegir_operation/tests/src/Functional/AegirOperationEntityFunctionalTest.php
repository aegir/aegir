<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir_operation\Functional;

use Drupal\Tests\aegir\Functional\AegirFunctionalTestBase;
use function array_merge;
use function time;

/**
 * Ægir Operation entity functional tests.
 *
 * @group aegir
 *
 * @group aegir_operation
 *
 * @todo Remove this in favour of the much faster Behat tests?
 */
class AegirOperationEntityFunctionalTest extends AegirFunctionalTestBase {

  /**
   * Test unpublished operation entity access.
   */
  public function testUnpublishedAccess(): void {

    $operationStorage = $this->container->get(
      'entity_type.manager',
    )->getStorage('aegir_operation');

    $operationManager = $this->drupalCreateUser([]);
    $operationManager->addRole('aegir_operation_manager');
    $operationManager->save();

    $this->drupalLogin($operationManager);

    $settings = [
      // Use one of the bundles exported to the 'aegir_test_operation' module.
      'type'    => 'test_operation_type_1',
      'name'    => 'TEST_OPERATION_A',
      'created' => time(),
      'user_id' => $operationManager->id(),
      'status'  => 1,
    ];

    $operation = $operationStorage->create($settings);

    $operation->setUnpublished();

    $this->assertFalse(
      $operation->isPublished(),
      'The Ægir operation is not unpublished as was expected!',
    );

    $operationStorage->save($operation);

    $this->drupalGet($operation->toUrl());

    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('TEST_OPERATION_A');

  }

}
