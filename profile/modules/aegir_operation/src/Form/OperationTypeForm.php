<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Form;

use Drupal\aegir_api\Form\AegirEntityTypeForm;
use Drupal\Core\Form\FormStateInterface;
use function array_key_exists;

/**
 * Add/edit form for Ægir operation entity types.
 *
 * @package Drupal\aegir_operation\Entity\EntityType\Form
 */
class OperationTypeForm extends AegirEntityTypeForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['task_order'] = [
      '#title' => $this->t('Task order'),
      '#description' => $this->t("Order of execution for any referenced tasks."),
      '#type' => 'table',
      // Place this after the fields from the parent; e.g. the name field.
      '#weight' => 1,
      '#header' => [
        $this->t('Task order'),
        $this->t('Weight'),
      ],
      '#attributes' => [
        'id' => 'task-order-table',
      ],
      '#tabledrag' => [[
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'task-order-weight',
      ]],
    ];

    $wrappedEntity = $this->typedRepositoryManager->wrap($this->entity);

    $task_order = $wrappedEntity->getTaskOrder();
    $ordered_tasks = $wrappedEntity->getOrderedTaskTypes();

    foreach ($ordered_tasks as $name => $field) {
      $form['task_order'][$name]['#attributes'] = ['class' => ['draggable']];
      # @TODO Debug why the '#weight' attribute doesn't appear to be working.
      #$form['task_order'][$name]['#weight'] = $task_order[$name] ?: 0;
      $form['task_order'][$name]['name'] = [
        '#type' => 'item',
        '#title' => $field->label(),
        '#markup' => $this->t('Backend role: %role', ['%role' => $name]),
      ];
      $form['task_order'][$name]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => array_key_exists($name, $task_order) ? $task_order[$name] : 0,
        '#attributes' => [
          'class' => ['task-order-weight'],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form, FormStateInterface $form_state,
  ): void {
    $task_order = [];
    foreach ($form_state->getValue('task_order') as $name => $values) {
      $task_order[$name] = $values['weight'];
    }

    $wrappedEntity = $this->typedRepositoryManager->wrap($this->entity);

    $wrappedEntity->setTaskOrder($task_order);

    parent::submitForm($form, $form_state);
  }

}
