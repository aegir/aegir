<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\inline_entity_form\ElementSubmit;
use Drupal\typed_entity\EntityWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for editing Ægir operations prior to dispatch.
 *
 * @ingroup aegir_operation
 */
class OperationDispatchForm extends ContentEntityForm {

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle service.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    EntityRepositoryInterface $entityRepository,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    TranslationInterface $stringTranslation,
    TimeInterface $time,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {

    parent::__construct($entityRepository, $entityTypeBundleInfo, $time);

    // ContentEntityForm uses StringTranslationTrait but does not inject
    // the 'string_translation' service. When you call $this->t(),
    // StringTranslationTrait will use \Drupal::service('string_translation')
    // to fetch the service if it isn't set to the property, so by setting it
    // here we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('string_translation'),
      $container->get('datetime.time'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(
    array $form, FormStateInterface $form_state,
  ): array {
    $actions['dispatch'] = [
      '#type' => 'submit',
      '#value' => $this->t('Dispatch'),
      '#submit' => [
        [ElementSubmit::class, 'trigger'],
        '::submitForm',
        '::save',
      ],
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => [
          'use-ajax-submit',
        ],
      ],
    ];

    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('aegir_operation.modal.close'),
      '#attributes' => [
        'class' => [
          'button',
          'btn',
          'btn-default',
          'use-ajax',
          'cancel-modal',
        ],
      ],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(
    array $form, FormStateInterface $form_state,
  ): void {

    parent::save($form, $form_state);

    $form_state->setRedirect('aegir_operation.modal.close');

    $this->typedRepositoryManager->wrap($this->getEntity())->dispatch();

  }

}
