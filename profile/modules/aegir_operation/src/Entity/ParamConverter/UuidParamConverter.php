<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Entity\ParamConverter;

use Drupal\aegir_operation\Entity\OperationInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

class UuidParamConverter implements ParamConverterInterface {

  use StringTranslationTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   *
   * @param \Psr\Log\LoggerInterface $loggerChannel
   *   Our logger channel.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    protected readonly EntityRepositoryInterface $entityRepository,
    protected readonly LoggerInterface $loggerChannel,
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    return $this->getOperationEntity($value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] == 'operation_uuid');
  }

  /**
   * Return the operation entity from its UUID.
   */
  public function getOperationEntity(string $uuid): EntityInterface|false {
    $entity = $this->entityRepository->loadEntityByUuid(
      'aegir_operation', $uuid,
    );

    if ($entity instanceof OperationInterface) {
      return $entity;
    }
    $this->loggerChannel->error($this->t('Failed to load operation entity from UUID: :uuid.', [':uuid' => $uuid]));
    return FALSE;
  }

}
