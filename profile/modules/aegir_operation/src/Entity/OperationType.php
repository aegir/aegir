<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeBase;
use Drupal\aegir_operation\Entity\OperationTypeInterface;

/**
 * Defines the Ægir Operation type configuration entity.
 *
 * @ConfigEntityType(
 *   id               = "aegir_operation_type",
 *   label            = @Translation("Operation type"),
 *   label_collection = @Translation("Operation types"),
 *   label_singular   = @Translation("operation type"),
 *   label_plural     = @Translation("operation types"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count operation type",
 *     plural   = "@count operation types",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityTypeListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_operation\Form\OperationTypeForm",
 *       "edit"   = "Drupal\aegir_operation\Form\OperationTypeForm",
 *       "delete" = "Drupal\aegir_api\Form\AegirEntityTypeDeleteConfirmForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix    = "aegir_operation_type",
 *   admin_permission = "administer aegir operation types",
 *   bundle_of        = "aegir_operation",
 *   entity_keys = {
 *     "id"     = "id",
 *     "label"  = "label",
 *     "uuid"   = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   links = {
 *     "canonical"    = "/admin/aegir/operations/types/{aegir_operation_type}",
 *     "add-form"     = "/admin/aegir/operations/types/add",
 *     "edit-form"    = "/admin/aegir/operations/types/{aegir_operation_type}/edit",
 *     "delete-form"  = "/admin/aegir/operations/types/{aegir_operation_type}/delete",
 *     "collection"   = "/admin/aegir/operations/types",
 *   },
 * )
 */
class OperationType extends AegirEntityTypeBase implements OperationTypeInterface {}
