<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Entity;

use Drupal\aegir_api\Entity\AegirEntityBase;
use Drupal\aegir_operation\Entity\OperationInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use function t;

/**
 * Defines the Ægir Operation entity.
 *
 * @ingroup aegir_operation
 *
 * @ContentEntityType(
 *   id               = "aegir_operation",
 *   label            = @Translation("Operation"),
 *   label_collection = @Translation("Operations"),
 *   label_singular   = @Translation("operation"),
 *   label_plural     = @Translation("operations"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count operation",
 *     plural   = "@count operations",
 *   ),
 *   bundle_label = @Translation("Operation type"),
 *   handlers     = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "dispatch"         = "Drupal\aegir_operation\Form\OperationDispatchForm",
 *       "revision-delete"  = "Drupal\Core\Entity\Form\RevisionDeleteForm",
 *       "revision-revert"  = "Drupal\Core\Entity\Form\RevisionRevertForm",
 *     },
 *     "access"         = "Drupal\aegir_api\Entity\AegirEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html"     = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     },
 *     "translation"  = "Drupal\content_translation\ContentTranslationHandler",
 *     "views_data"   = "Drupal\views\EntityViewsData",
 *   },
 *   base_table       = "aegir_operation",
 *   data_table       = "aegir_operation_field_data",
 *   revision_table   = "aegir_operation_revision",
 *   revision_data_table = "aegir_operation_field_revision",
 *   translatable     = true,
 *   show_revision_ui = true,
 *   admin_permission = "administer aegir operation entities",
 *   entity_keys      = {
 *     "id"         = "id",
 *     "revision"   = "revision_id",
 *     "bundle"     = "type",
 *     "label"      = "name",
 *     "uuid"       = "uuid",
 *     "owner"      = "user_id",
 *     "langcode"   = "langcode",
 *     "published"  = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"        = "/admin/aegir/operations/{aegir_operation}",
 *     "add-page"         = "/admin/aegir/operations/add",
 *     "add-form"         = "/admin/aegir/operations/add/{aegir_operation_type}",
 *     "edit-form"        = "/admin/aegir/operations/{aegir_operation}/edit",
 *     "delete-form"      = "/admin/aegir/operations/{aegir_operation}/delete",
 *     "version-history"  = "/admin/aegir/operations/{aegir_operation}/revisions",
 *     "revision"         = "/admin/aegir/operations/{aegir_operation}/revisions/{aegir_operation_revision}/view",
 *     "revision-revert-form" = "/admin/aegir/operations/{aegir_operation}/revisions/{aegir_operation_revision}/revert",
 *     "revision-delete-form" = "/admin/aegir/operations/{aegir_operation}/revisions/{aegir_operation_revision}/delete",
 *     "collection"       = "/admin/aegir/operations",
 *   },
 *   bundle_entity_type   = "aegir_operation_type",
 *   field_ui_base_route  = "entity.aegir_operation_type.canonical",
 * )
 */
class Operation extends AegirEntityBase implements OperationInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entityType,
  ) {

    $fields = parent::baseFieldDefinitions($entityType);

    $fields['task_log_output'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Task log'))
      ->setDescription(t('The output resulting from the last dispatch of this task.'))
      ->setRevisionable(true)
      ->setCardinality(-1);

    $fields['task_log_sequence'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Task log sequence'))
      ->setDescription(t('The sequences of the output from the last dispatch of this task.'))
      ->setRevisionable(true)
      ->setCardinality(-1);

    $fields['task_log_timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Task log timestamps'))
      ->setDescription(t('The timestamps of the output from the last dispatch of this task.'))
      ->setRevisionable(true)
      ->setCardinality(-1);

    $fields['task_exitcode'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Task exitcode'))
      ->setDescription(t('The exit code from the last dispatch of this task.'))
      ->setRevisionable(true)
      ->setCardinality(1);

    return $fields;

  }

}
