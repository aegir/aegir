<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Entity;

use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateChildrenInterface;
use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateParentInterface;

/**
 * Interface for Ægir Operation entities.
 */
interface OperationInterface extends AegirEntityInterface, AegirEntityWithAutoCreateChildrenInterface, AegirEntityWithAutoCreateParentInterface {}
