<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeInterface;

/**
 * Interface for Ægir Operation type entities.
 */
interface OperationTypeInterface extends AegirEntityTypeInterface {}
