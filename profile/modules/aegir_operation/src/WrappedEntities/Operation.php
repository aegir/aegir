<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\WrappedEntities;

use Drupal\aegir_api\WrappedEntities\AegirWrappedEntityWithChildrenAndParent;
use Drupal\aegir_operation\Entity\OperationInterface as OperationEntityInterface;
use Drupal\aegir_operation\Value\OperationStatus;
use Drupal\aegir_operation\WrappedEntities\OperationInterface;
use Drupal\aegir_queue\TaskQueue\TaskQueueInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\TypedData\ListInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Drupal\typed_entity\TypedEntityContext;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wraps Ægir operation entities.
 */
class Operation extends AegirWrappedEntityWithChildrenAndParent implements OperationInterface {

  /**
   * The name of the operation status field on the operation entity.
   */
  protected const STATUS_FIELD_NAME = 'operation_status';

  /**
   * The name of the task log output field on the operation entity.
   */
  protected const TASK_LOG_OUTPUT_FIELD_NAME = 'task_log_output';

  /**
   * The name of the task log sequence field on the operation entity.
   */
  protected const TASK_LOG_SEQUENCE_FIELD_NAME = 'task_log_sequence';

  /**
   * The name of the task log timestamp field on the operation entity.
   */
  protected const TASK_LOG_TIMESTAMP_FIELD_NAME = 'task_log_timestamp';

  /**
   * The name of the exit code field on the operation entity.
   */
  protected const EXIT_CODE_FIELD_NAME = 'task_exitcode';

  /**
   * {@inheritdoc}
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Our logger channel.
   *
   * @param \Drupal\aegir_queue\TaskQueue\TaskQueueInterface $queue
   *   The task queue.
   */
  public function __construct(
    EntityInterface             $entity,
    EntityFieldManagerInterface $entityFieldManager,
    EntityTypeManagerInterface  $entityTypeManager,
    EntityWrapperInterface      $typedRepositoryManager,
    TranslationInterface        $stringTranslation,
    protected readonly LoggerInterface    $logger,
    protected readonly TaskQueueInterface $queue,
  ) {

    parent::__construct(
      $entity, $entityFieldManager, $entityTypeManager, $typedRepositoryManager,
      $stringTranslation,
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container, EntityInterface $entity,
  ) {
    return new static(
      $entity,
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
      $container->get('string_translation'),
      $container->get('logger.factory')->get($entity->getEntityTypeId()),
      $container->get('aegir_queue.task_queue'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {

    return $context->offsetGet('entity') instanceof OperationEntityInterface;

  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {

    return $this->entity->get(self::STATUS_FIELD_NAME)->first()->getString();

  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(string $status): string {

    $this->entity->set(
      self::STATUS_FIELD_NAME, OperationStatus::from($status)->value,
    );

    $this->entity->setNewRevision(false);

    $this->save();

    return $status;

  }

  /**
   * {@inheritdoc}
   */
  public function setStatusDispatched(): void {

    $this->entity->set(
      self::STATUS_FIELD_NAME, OperationStatus::from('dispatched')->value,
    );

    $this->entity->setNewRevision(true);

    $this->resetTaskLogFields();

    $this->save();

  }

  /**
   * {@inheritdoc}
   */
  public function setStatusError(): void {

    $this->setStatus('error');

  }

  /**
   * {@inheritdoc}
   */
  public function updateStatus(): void {

    $this->setStatus(
      OperationStatus::fromTaskLogOutput($this->getTaskLogOutput())->value,
    );

  }

  /**
   * {@inheritdoc}
   */
  public function resetTaskLogFields(): void {

    foreach ([
      self::TASK_LOG_OUTPUT_FIELD_NAME,
      self::TASK_LOG_SEQUENCE_FIELD_NAME,
      self::TASK_LOG_TIMESTAMP_FIELD_NAME,
    ] as $fieldName) {

      // These fields are all multi-value (i.e. arrays), so just reset each to
      // an empty array.
      $this->entity->set($fieldName, []);

    }

  }

  /**
   * {@inheritdoc}
   */
  public function getTaskLogOutput(): ListInterface {

    return $this->entity->get(self::TASK_LOG_OUTPUT_FIELD_NAME);

  }

  /**
   * {@inheritdoc}
   */
  public function addTaskLogOutput(string $output): void {

    $this->getTaskLogOutput()->appendItem($output);

  }

  /**
   * {@inheritdoc}
   */
  public function getTaskLogSequences(): ListInterface {

    return $this->entity->get(self::TASK_LOG_SEQUENCE_FIELD_NAME);

  }

  /**
   * {@inheritdoc}
   */
  public function addTaskLogSequence(int $number): void {

    $this->getTaskLogSequences()->appendItem($number);

  }

  /**
   * {@inheritdoc}
   *
   * @todo Add validation using typed data and/or DateTimePlus.
   *
   * @see \Drupal\Core\TypedData\Plugin\DataType\Timestamp
   *   Can be used to validate timestamps.
   *
   * @see \Drupal\Component\Datetime\DateTimePlus
   *   Can be used to validate timestamps.
   */
  public function addTaskLogTimestamp(int $timestamp): void {

    $this->getTaskLogTimestamps()->appendItem($timestamp);

  }

  /**
   * {@inheritdoc}
   */
  public function getTaskLogTimestamps(): ListInterface {

    return $this->entity->get(self::TASK_LOG_TIMESTAMP_FIELD_NAME);

  }

  /**
   * {@inheritdoc}
   */
  public function getExitCode(): int {
    return $this->entity->get(self::EXIT_CODE_FIELD_NAME)->getCastedValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setExitCode(int $code): void {
    $this->entity->set(self::EXIT_CODE_FIELD_NAME, $code);
  }

  /**
   * {@inheritdoc}
   *
   * @todo Return the task/taskId, so that we can reconstruct the AsyncResult
   *   to check on task progress/status?
   *
   * @todo Make the task configurable.
   */
  public function dispatch(): void {

    if ($this->hasParent()) {
      $marshalFrom = $this->getParent();
    } else {
      $marshalFrom = $this;
    }

    $this->logger->notice($this->t('Marshalling variables from :label', [
      ':label' => $marshalFrom->label(),
    ]));

    $variables = $marshalFrom->marshal();

    $this->logger->notice($this->t('Dispatching tasks from :label.', [
      ':label' => $this->label(),
    ]));

    $this->setStatusDispatched();

    $config = $this->getConfig();

    # @TODO: Flesh out this structure, allowing for executor "plugins" to
    # inject their own config/vars?
    # Ultimately, we want an array like:
    # [ 'name' => 'Print debug data',
    #   'uuid' => '1234-5678...',
    #   'tasks' => [
    #     'print_debug_data' => [
    #       'weight' => 0,
    #       'executor' => 'ansible',
    #       'role' => 'task1_role',
    #       'vars' => [
    #
    #       ],
    #     ],
    #     'write_debug_data' => [
    #       'weight' => 1,
    #       'executor' => 'bash',
    #       'role' => 'task2_role',
    #       'vars' => [
    #       ],
    #     ],
    #   ],
    # ]
    #
    # This shape is close enough for dispatcherd to handle Ansible tasks for now.
    $operation = [
      'name' => $this->entity->label(),
      'uuid' => $this->entity->uuid(),
      'variables' => $variables,
      'tasks' => $this->getTasks(),
    ];

    // @TODO Add a global "debug" flag to simplify testing IPC components.
    $task = $this->queue->addTask('dispatcherd.dispatch_operation', [$config, $operation]);

    /* @todo Return the task/taskId, so that we can reconstruct the AsyncResult to check on task progress/status? */
    if (!$task) {

      $this->logger->error($this->t('Failed to dispatch operation: :label.', [
        ':label' => $this->label(),
      ]));

      $this->setStatusError();

    }

  }

  /**
   * Return a list of referenced tasks keyed by field name, including UUIDs and executor.
   *
   * @return array
   *
   * @todo Add helper method(s) to allow us to iterate over a simpler array.
   */
  protected function getTasks(): array {

    $tasks = [];

    $wrappedBundleEntity = $this->repositoryManager()->wrap(
      $this->getBundleEntity(),
    );

    foreach ($wrappedBundleEntity->getTaskTypes() as $name => $taskField) {

      $fieldName = $taskField->getName();

      $values = $this->entity->get($fieldName)->referencedEntities();

      $task = reset($values);

      $tasks[$name] = [
        'uuid' => $task->uuid(),
        'executor' => $task->get('task_executor')->getString(),
      ];
    }
    return $tasks;
  }

  /**
   * Return the operation configuration to pass to the backend.
   *
   * @return array
   *
   * @todo Make this configurable.
   */
  protected function getConfig(): array {

    if ($this->hasParent()) {
      $referencingEntity = $this->getParent()->getEntity();
    } else {
      $referencingEntity = $this->getEntity();
    }

    return [
      'roles'       => $this->marshalRoles(),
      'roles_path'  => '/app/backend/ansible/roles/',
      'uuid'        => $this->entity->uuid(),
      'uuids'       => [
        'operation' => $this->entity->uuid(),
        $referencingEntity->getEntityTypeId() => $referencingEntity->uuid(),
      ],
    ];
  }

}
