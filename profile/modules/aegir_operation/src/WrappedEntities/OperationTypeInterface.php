<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\WrappedEntities;

/**
 * Interface for wrapped Ægir operation entity type entities.
 */
interface OperationTypeInterface {

  /**
   * Return the weighted order to execute referenced tasks.
   */
  public function getTaskOrder(): array;

  /**
   * Save the weighted order to execute referenced tasks.
   */
  public function setTaskOrder(array $taskOrder): void;

  /**
   * Return the task types referenced by this operation type in weighted order.
   */
  public function getOrderedTaskTypes(): array;

  /**
   * Return the task types referenced by this operation type.
   */
  public function getTaskTypes(): array;

}
