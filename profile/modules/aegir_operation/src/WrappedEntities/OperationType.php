<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\WrappedEntities;


use Drupal\aegir_api\WrappedEntities\AegirWrappedEntityType;
use Drupal\aegir_operation\Entity\OperationTypeInterface as OperationEntityTypeInterface;
use Drupal\aegir_operation\WrappedEntities\OperationTypeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Drupal\typed_entity\TypedEntityContext;
use function array_key_exists;
use function array_pop;
use function asort;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wraps Ægir operation entity type entities.
 *
 * @todo Move much of the task order to an iterable value object and type to
 *   that instead of arrays.
 */
class OperationType extends AegirWrappedEntityType implements OperationTypeInterface {

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    EntityInterface $entity,
    protected readonly ConfigFactoryInterface       $configFactory,
    protected readonly EntityFieldManagerInterface  $entityFieldManager,
    EntityTypeManagerInterface  $entityTypeManager,
    EntityWrapperInterface      $typedRepositoryManager,
  ) {

    parent::__construct($entity, $entityTypeManager, $typedRepositoryManager);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container, EntityInterface $entity,
  ) {

    return new static(
      $entity,
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

  }

  /**
   * {@inheritdoc}
   *
   * @todo Operation entity type interface.
   */
  public static function applies(TypedEntityContext $context): bool {

    return $context->offsetGet('entity') instanceof OperationEntityTypeInterface;

  }

  /**
   * Get the configuration name for this operation type's task order.
   *
   * @return string
   *
   * @todo Move to a trait or reusable class.
   */
  protected function getTaskOrderConfigName(): string {
    return 'aegir_operation.task_order.' . $this->entity->id();
  }

  /**
   * Load task order from storage.
   *
   * @return array
   */
  protected function loadTaskOrder(): array {

    $configName = $this->getTaskOrderConfigName();

    $taskOrder = $this->configFactory->get($configName)->get($configName);

    return $taskOrder ?: [];

  }

  /**
   * {@inheritdoc}
   */
  public function getTaskOrder(): array {

    $taskOrder = $this->loadTaskOrder();

    asort($taskOrder);

    return $taskOrder;

  }

  /**
   * Save task order to storage.
   *
   * @param array $taskOrder
   */
  protected function saveTaskOrder(array $taskOrder): void {

    $configName = $this->getTaskOrderConfigName();

    $config = $this->configFactory->getEditable($configName);

    $config->set($configName, $taskOrder);

    $config->save();

  }

  /**
   * {@inheritdoc}
   */
  public function setTaskOrder(array $taskOrder): void {

    asort($taskOrder);

    $this->saveTaskOrder($taskOrder);

  }

  /**
   * {@inheritdoc}
   */
  public function getOrderedTaskTypes(): array {

    $taskOrder = $this->getTaskOrder();

    $tasks = $this->getTaskTypes();

    $orderedTasks = [];

    foreach ($taskOrder as $name => $weight) {

      // If a task is removed from an operation, it'll still be saved in the
      // task order, so drop it here.
      if (!array_key_exists($name, $tasks)) continue;

      $orderedTasks[$name] = $tasks[$name];

      unset($tasks[$name]);

    }

    // Append any remaining tasks; i.e., that don't have a weight yet.
    $orderedTasks += $tasks;

    return $orderedTasks;

  }

  /**
   * {@inheritdoc}
   */
  public function getTaskTypes(): array {

    $entityTypeId = $this->getEntityType()->getBundleOf();

    $bundleId = $this->entity->id();

    $fields = $this->entityFieldManager->getFieldDefinitions(
      $entityTypeId, $bundleId,
    );

    $tasks = [];

    foreach ($fields as $name => $definition) {

      if ($definition->getSetting('target_type') != 'aegir_task') continue;

      $name = array_pop($definition->getSetting('handler_settings')[
        'target_bundles'
      ]);

      $tasks[$name] = $definition;

    }

    return $tasks;

  }

}
