<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\WrappedEntities;

use Drupal\Core\TypedData\ListInterface;

/**
 * Interface for wrapped Ægir operation entities.
 */
interface OperationInterface {

  /**
   * Get the current status of the operation.
   *
   * @return string
   *
   * @see \Drupal\aegir_operation\Value\OperationStatus
   */
  public function getStatus(): string;

  /**
   * Set the operation status.
   *
   * Note that this will also save the entity to storage without creating a new
   * revision.
   *
   * @param string $status
   *
   * @return string
   *   The status if it was successfully set, to enable one line setting and
   *   returning/assignment/validation.
   */
  public function setStatus(string $status): string;

  /**
   * Set the operation status as dispatched.
   *
   * Note that this will reset (empty) task output and save the operation to
   * storage with a new revision.
   */
  public function setStatusDispatched(): void;

  /**
   * Set the status of the operation to error.
   */
  public function setStatusError(): void;

  /**
   * Update (read) the operation status from the latest task log output.
   */
  public function updateStatus(): void;

  /**
   * Empty task log fields, usually in preparation for new task log output.
   */
  public function resetTaskLogFields(): void;

  /**
   * Get the task log output for the operation.
   *
   * Note that the return value is intentionally typed to ListInterface
   * instead of FieldItemListInterface as the latter is more complex to test
   * while the former is easier and simpler to mock up for and all
   * FieldItemListInterface extend ListInterface.
   *
   * @return \Drupal\Core\TypedData\ListInterface
   *   A list of task log output lines, one line per list item.
   *
   * @see \Drupal\Core\Field\FieldItemListInterface
   *
   * @see https://gitlab.com/aegir/aegir/-/issues/215
   *   Will be replaced with a value object.
   */
  public function getTaskLogOutput(): ListInterface;

  /**
   * Add a line to the task log output for the operation.
   *
   * @param string $output
   *   A line of output to add to the log.
   *
   * @see https://gitlab.com/aegir/aegir/-/issues/215
   *   Will be replaced with a value object.
   */
  public function addTaskLogOutput(string $output): void;

  /**
   * Get the task log sequence numbers for the operation.
   *
   * @return \Drupal\Core\TypedData\ListInterface
   *   A list of task log sequences, each item corresponding to a log line.
   *
   * @see https://gitlab.com/aegir/aegir/-/issues/215
   *   Will be replaced with a value object.
   */
  public function getTaskLogSequences(): ListInterface;

  /**
   * Add a task log sequence number to the operation.
   *
   * @param int $number
   *   A task log sequence number.
   *
   * @see https://gitlab.com/aegir/aegir/-/issues/215
   *   Will be replaced with a value object.
   */
  public function addTaskLogSequence(int $number): void;

  /**
   * Get task log timestamps for the operation.
   *
   * @return \Drupal\Core\TypedData\ListInterface
   *   A list of task log timestamps, each item corresponding to a log line.
   *
   * @see https://gitlab.com/aegir/aegir/-/issues/215
   *   Will be replaced with a value object.
   */
  public function getTaskLogTimestamps(): ListInterface;

  /**
   * Add a task log timestamp to the operation.
   *
   * @param int $timestamp
   *   A timestamp to add.
   *
   * @see https://gitlab.com/aegir/aegir/-/issues/215
   *   Will be replaced with a value object.
   */
  public function addTaskLogTimestamp(int $timestamp): void;

  /**
   * Get the exit code for the operation.
   *
   * @return int
   *   An exit code.
   */
  public function getExitCode(): int;

  /**
   * Set an exit code for the operation.
   *
   * @param int $code
   *   An exit code.
   */
  public function setExitCode(int $code): void;

  /**
   * Dispatch the operation's tasks.
   */
  public function dispatch(): void;

}
