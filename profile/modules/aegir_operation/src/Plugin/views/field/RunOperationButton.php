<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Plugin\views\field;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to present a button to run an operation.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("run_operation")
 */
class RunOperationButton extends AbstractOperationButton {

  /**
   * Constructor; saves dependencies.
   *
   * @param array $configuration
   *   A configuration array containing information about the plug-in instance.
   *
   * @param string $pluginId
   *   The plugin_id for the plug-in instance.
   *
   * @param array $pluginDefinition
   *   The plug-in implementation definition.
   *
   * @param \Drupal\Core\Access\AccessManagerInterface $accessManager
   *   The access manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    array $configuration, $pluginId, $pluginDefinition,
    AccessManagerInterface $accessManager,
    EntityTypeManagerInterface $entityTypeManager,
    EntityRepositoryInterface $entityRepository,
    LanguageManagerInterface $languageManager,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {

    parent::__construct(
      $configuration, $pluginId, $pluginDefinition,
      $accessManager, $entityTypeManager, $entityRepository, $languageManager,
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {

    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('access_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

  }

  /**
   * {@inheritdoc}
   */
  protected $modalRoute = 'aegir_operation.modal.dispatch';

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('Run');
  }

  /**
   * {@inheritdoc}
   */
  protected function getModalUrlClasses() {
    $classes = parent::getModalUrlClasses();
    $classes[] = 'operation-run-button';
    return $classes;
  }

  /**
   * {@inheritdoc}
   */
  protected function buttonIsEnabled(ResultRow $row) {
    return $this->typedRepositoryManager->wrap(
      $this->getEntity($row),
    )->getStatus() !== 'dispatched';
  }

}
