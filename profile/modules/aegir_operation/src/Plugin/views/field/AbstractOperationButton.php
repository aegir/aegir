<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Plugin\views\field;

use Drupal\Core\Url;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\EntityLink;

/**
 * Base class for field handler to present operation buttons.
 *
 * @ingroup views_field_handlers
 */
abstract class AbstractOperationButton extends EntityLink {

  /**
   * The route to trigger the ajax modal command.
   */
  protected $modalRoute = NULL;

  /**
   * Return URL classes to enable a modal dialog.
   */
  protected array $modalUrlClasses = [
    'use-ajax',
    'button',
    'operation-button',
  ];

  /**
   * Classes for the disabled element.
   *
   * Since this isn't a link, using the 'use-ajax' class on it will result in
   * many Ajax errors client-side as the operation status is refreshed because
   * Drupal will attempt to get the URL from the element to attach Ajax
   * behaviours to, which it obviously won't find.
   *
   * @var string[]
   */
  protected array $disabledElementClasses = [
    'button',
    'operation-button',
    'is-disabled',
  ];

  /**
   * Return the route to trigger the ajax modal command.
   */
  protected function getModalRoute() {
    return $this->modalRoute;
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    return $this
      ->getModalUrl($row->_entity)
      ->setOptions($this->getModalUrlOptions());
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    $text = parent::renderLink($row);
    if ($this->buttonIsEnabled($row)) {
      return $text;
    }
    return $this->disabledButton((string) $text);
  }

  /**
   * Determine whether a button should be enabled or disabled.
   *
   * Override in subclasses to test for appropriate circumstances.
   */
  protected function buttonIsEnabled(ResultRow $row) {
    return TRUE;
  }

  /**
   * Return a disabled button.
   */
  protected function disabledButton(string $text) {

    $this->options['alter']['make_link'] = false;

    $classes = implode(' ', $this->disabledElementClasses);

    return "<span class='{$classes}'>{$text}</span>";
  }

  /**
   * Return the URL that will return an AJAX modal dialog command.
   */
  protected function getModalUrl($entity) {
    $route = $this->getModalRoute();
    $uuid = $entity->uuid();
    return Url::fromRoute($route, ['operation' => $uuid]);
  }

  /**
   * Return URL options to enable a modal dialog.
   */
  protected function getModalUrlOptions() {
    return [
      'attributes' => [
        'class' => $this->getModalUrlClasses(),
        'data-dialog-type' => 'modal',
      ],
    ];
  }

  /**
   * Return URL classes to enable a modal dialog.
   */
  protected function getModalUrlClasses() {
    return $this->modalUrlClasses;
  }

}
