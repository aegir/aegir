<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Plugin\views\field;

/**
 * Field handler to present a button to view an operation log.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("view_operation_log")
 */
class ViewOperationButton extends AbstractOperationButton {

  /**
   * {@inheritdoc}
   */
  protected $modalRoute = 'aegir_operation.modal.log';

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('View');
  }

  /**
   * {@inheritdoc}
   */
  protected function getModalUrlClasses() {
    $classes = parent::getModalUrlClasses();
    $classes[] = 'operation-view-button';
    return $classes;
  }

}
