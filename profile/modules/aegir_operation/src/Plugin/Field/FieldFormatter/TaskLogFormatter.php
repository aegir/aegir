<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the Task Log formatter.
 *
 * @FieldFormatter(
 *   id = "task_log",
 *   label = @Translation("Task Log"),
 *   description = @Translation("Formats task log output with Ansi colours."),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class TaskLogFormatter extends FormatterBase {

  use StringTranslationTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param string $pluginId
   *   The plugin_id for the formatter.
   *
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The definition of the field to which the formatter is associated.
   *
   * @param array $settings
   *   The formatter settings.
   *
   * @param string $label
   *   The formatter label display setting.
   *
   * @param string $viewMode
   *   The view mode.
   *
   * @param array $thirdPartySettings
   *   Any third party settings.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    string $pluginId,
    array $pluginDefinition,
    FieldDefinitionInterface $fieldDefinition,
    array $settings,
    string $label,
    string $viewMode,
    array $thirdPartySettings,
    protected $stringTranslation,
  ) {

    parent::__construct(
      $pluginId,
      $pluginDefinition,
      $fieldDefinition,
      $settings,
      $label,
      $viewMode,
      $thirdPartySettings,
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {

    return new static(
      $pluginId,
      $pluginDefinition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('string_translation'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [$this->t('Displays task log output.')];
  }

  /**
   * {@inheritdoc}
   *
   * @todo Implement this as a template so that it can be filtered for XSS,
   *   customized and/or overridden, and styled via CSS.
   */
  public function viewElements(
    FieldItemListInterface $items, $langcode,
  ): array {
    $converter = new AnsiToHtmlConverter();

    $first = 0;
    $last = count($items) - 1;
    $elements = [];
    foreach ($items as $delta => $item) {
      switch ($delta) {
        case $first:
          $class = 'task-log-row first-task-log-row';
          break;
        case $last:
          $class = 'task-log-row last-task-log-row';
          break;
        default:
          $class = 'task-log-row';
      }
      // Render each element as markup.
      $elements[$delta] = [
        '#type'     => 'markup',
        '#children' => '<pre class="' . $class . '">' . $converter->convert($item->value) . '</pre>',
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Replace #prefix and #suffix with a template.
   */
  public function view(FieldItemListInterface $items, $langcode = NULL): array {
    $elements = parent::view($items, $langcode);
    $elements['#prefix'] = '<div class="task-log-field">';
    $elements['#suffix'] = '</div>';
    $elements['#attached']['library'][] = 'aegir_operation/task_log';
    return $elements;
  }

}
