<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\ModalDialog;

use Drupal\aegir_operation\Entity\OperationInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use function count;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Modal dialog to display an operation entity form, if required, before
 * dispatching it to the queue.
 */
class DispatchModal extends AbstractModal implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  protected ?string $title = 'Run operation';

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entityFormBuilder
   *   The entity form builder.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    protected readonly EntityFormBuilderInterface $entityFormBuilder,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.form_builder'),
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );
  }

  /**
   * {@inheritdoc}
   *
   */
  public function openModal(OperationInterface $operation): Response {
    $this->setEntity($operation);
    if ($this->dispatchRequiresInput()) {
      return parent::openModal($operation);
    }
    return $this->dispatchOperation();
  }

  /**
   * Determine whether a given operation has fields.
   */
  protected function dispatchRequiresInput(): bool {

    return count(
      $this->typedRepositoryManager->wrap($this->getEntity())->marshal(),
    ) > 0;

  }

  /**
   * {@inheritdoc}
   *
   * Provides the operation dispatch form.
   *
   * @see \Drupal\aegir_operation\Entity\Operation
   *
   * @see \Drupal\aegir_operation\Form\OperationDispatchForm
   */
  protected function getContent(): array {
    return $this->entityFormBuilder->getForm($this->getEntity(), 'dispatch');
  }

  /**
   * Dispatch an operation to the queue.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *
   * @todo Add error checking and logging.
   */
  protected function dispatchOperation(): AjaxResponse {

    $this->typedRepositoryManager->wrap($this->getEntity())->dispatch();

    return $this->closeModal();

  }

}
