<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\ModalDialog;

use Drupal\aegir_operation\Entity\OperationInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Symfony\Component\HttpFoundation\Response;

/**
 * A base modal dialog class.
 */
abstract class AbstractModal {

  /**
   * The title of the modal dialog.
   */
  protected ?string $title = NULL;

  /**
   * Options to pass to the ajax command.
   */
  protected array $options = [
    'dialogClass' => 'modal-dialog-class',
    'width' => '80%',
  ];

  /**
   * The Operation entity that this modal is handling.
   */
  protected ?OperationInterface $entity = null;

  /**
   * Provide an AJAX response to open a modal dialog.
   *
   * @param \Drupal\aegir_operation\Entity\OperationInterface $operation
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A Symfony Response object. Note that we're not typing this to the more
   *   specific AjaxResponse so that classes that extend this can use other
   *   response types in addition to AjaxResponse.
   */
  public function openModal(OperationInterface $operation): Response {
    $this->setEntity($operation);

    $title = $this->getTitle();
    $content = $this->getContent();
    $options = $this->getOptions();

    $response = new AjaxResponse();
    $response->setAttachments(['library' => ['core/drupal.dialog.ajax']]);
    $response->addCommand(new OpenModalDialogCommand($title, $content, $options));

    return $response;
  }

  /**
   * Close a modal dialog.
   */
  public function closeModal(): AjaxResponse {
    $command = new CloseModalDialogCommand();
    $response = new AjaxResponse();
    $response->addCommand($command);
    return $response;
  }

  /**
   * Return the content to be displayed in the modal dialog.
   */
  abstract protected function getContent(): array;

  /**
   * Return the title of the modal dialog.
   */
  protected function getTitle(): ?string {
    return $this->title;
  }

  /**
   * Return the modal dialg options.
   */
  protected function getOptions(): array {
    return $this->options;
  }

  /**
   * Return the Operation entity this modal is handling.
   */
  protected function getEntity(): ?OperationInterface {
    return $this->entity;
  }

  /**
   * Set the Operation entity this modal is handling.
   */
  protected function setEntity(OperationInterface $entity): void {
    $this->entity = $entity;
  }

}
