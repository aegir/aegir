<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Hooks;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\hux\Attribute\Alter;
use Drupal\hux\Attribute\Hook;
use Drupal\views\ViewExecutable;
use function in_array;

/**
 * Views hook implementations.
 */
class ViewsHooks {

  use StringTranslationTrait;

  /**
   * Constructs this hook; saves dependencies.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    TranslationInterface $stringTranslation,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices. Unfortunately, we
    // can't do this via constructor property promotion because
    // StringTranslationTrait doesn't yet type hint the interface on the
    // property, which means it's incompatible with autowiring at the time of
    // writing on 10.3.x.
    $this->setStringTranslation($stringTranslation);

  }

  #[Alter('views_data')]
  /**
   * Implements hook_views_data_alter() for Ægir operation entities.
   */
  public function dataAlter(array &$data): void {

    $data['aegir_operation']['run_operation'] = [
      'field' => [
        'title' => $this->t('Run operation'),
        'help'  => $this->t('A button to run this operation.'),
        'id'    => 'run_operation',
      ],
    ];

    $data['aegir_operation']['view_operation_log'] = [
      'field' => [
        'title' => $this->t('View operation log'),
        'help'  => $this->t('A button to view the log for this operation.'),
        'id'    => 'view_operation_log',
      ],
    ];

    $data['aegir_operation']['operation_status'] = [
      'field' => [
        'title' => $this->t('Operation status icon'),
        'help'  => $this->t('An icon representing the operation\'s status.'),
        'id'    => 'operation_status_icon',
      ],
    ];

  }

  #[Hook('views_pre_render')]
  /**
   * Implements \hook_views_pre_render() for Ægir operation entities.
   */
  public function preRender(ViewExecutable $view): void {

    // @todo Can we use some heuristic or flag to detect these dynamically
    //  rather than hard-coding the view names here?
    $operationViews = ['platform_operations', 'site_operations'];

    if (!in_array($view->storage->id(), $operationViews)) {
      return;
    }

    $view->element['#attached'][
      'library'
    ][] = 'aegir_operation/operation_views';

  }

}
