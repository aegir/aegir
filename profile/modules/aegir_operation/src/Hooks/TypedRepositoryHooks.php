<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Hooks;

use Drupal\aegir_operation\WrappedEntities\Operation;
use Drupal\aegir_operation\WrappedEntities\OperationType;
use Drupal\hux\Attribute\Alter;
use function array_unshift;

/**
 * Typed entity repository hooks for Ægir operation entities.
 */
class TypedRepositoryHooks {

  #[Alter('typed_repository_info')]
  /**
   * Adds the Ægir operation entity wrapper definition.
   *
   * @param array &$definitions
   *   Discovered typed repository definitions.
   */
  public function addOperationEntityDefinition(array &$definitions): void {

    if (!isset($definitions['aegir_entity:aegir_operation'])) {
      return;
    }

    // Prepend our class to variants so that it gets to match to operation
    // entities before the general purpose variants.
    array_unshift(
      $definitions[
        'aegir_entity:aegir_operation'
      ]['wrappers']->variants,
      Operation::class,
    );

  }

  #[Alter('typed_repository_info')]
  /**
   * Adds the Ægir operation entity type entity wrapper definition.
   *
   * @param array &$definitions
   *   Discovered typed repository definitions.
   */
  public function addOperationEntityTypeDefinition(array &$definitions): void {

    // Prepend our class to variants so that it gets to match to operation
    // entity type entities before the general purpose variants.
    if (!isset($definitions['aegir_entity_type:aegir_operation_type'])) {
      return;
    }

    array_unshift(
      $definitions[
        'aegir_entity_type:aegir_operation_type'
      ]['wrappers']->variants,
      OperationType::class,
    );

  }

}
