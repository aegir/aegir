<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use function is_null;

/**
 * Class AegirExitcodeCommand.
 *
 * @package Drupal\aegir_exitcode_command
 */
#[AsCommand(
  name:         'aegir:exitcode',
  description:  'Register exit code for an Ægir Operation.',
)]
class AegirExitcodeCommand extends Command {

  use StringTranslationTrait;

  // The entity type for Aegir operations. We should only be registering exit codes to operations.
  const OPERATION_ENTITY_TYPE = 'aegir_operation';

  /**
   * Our logger channel.
   */
  const LOGGER_CHANNEL = 'aegir_task_log';

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    protected readonly EntityRepositoryInterface $entityRepository,
    protected readonly LoggerChannelFactoryInterface $loggerChannelFactory,
    TranslationInterface $stringTranslation,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

    parent::__construct();

  }

  /**
   * Get our logger channel.
   *
   * We don't save our logger channel in the constructor to avoid instantiating
   * it until we actually need it.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected function loggerChannel(): LoggerChannelInterface {
    return $this->loggerChannelFactory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this
      ->setHidden(true)
      ->addArgument(
        'uuid',
        InputArgument::REQUIRED,
        (string) $this->t(
          'The UUID of the entity into which we will be inputting data.',
        ),
      )
      ->addArgument(
        'exitcode',
        InputArgument::REQUIRED,
        (string) $this->t('The exit code of the operation.'),
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {
    $io = new SymfonyStyle($input, $output);
    $args = $input->getArguments();
    foreach ($args as $key => $value) {
      $io->info($key . ': ' . $value);
    }

    $operation = $this->entityRepository->loadEntityByUuid(self::OPERATION_ENTITY_TYPE, $args['uuid']);

    if (is_null($operation)) {

      $io->error((string) $this->t(
        'No operation found with UUID @uuid',
        ['@uuid' => $args['uuid']],
      ));

      return Command::FAILURE;

    }

    /** @var \Drupal\aegir_operation\WrappedEntities\OperationInterface */
    $wrappedEntity = $this->typedRepositoryManager->wrap($operation);

    $this->loggerChannel()->notice($this->t('Updating status of :label operation.', [':label' => $wrappedEntity->label()]));

    $wrappedEntity->setExitCode((int) $args['exitcode']);
    // Don't create a new revision when updating the task exit code.
    $wrappedEntity->getEntity()->setNewRevision(false);
    $wrappedEntity->updateStatus();
    $wrappedEntity->save();

    // In our tests, we create stand-alone operations, that do not have parent
    // entities. So we bail here, in that case. This doesn't make sense in
    // normal operations, where we will want to update the status of the
    // parent.
    if (!$wrappedEntity->hasParent()) {
      return Command::SUCCESS;
    }

    /** @var \Drupal\aegir_operation\WrappedEntities\OperationInterface */
    $parentWrapped = $wrappedEntity->getParent();

    $this->loggerChannel()->notice($this->t(
      'Updating status of :label :type.',[
        ':label'  => $parentWrapped->label(),
        ':type'   => $parentWrapped->getEntityType()->getSingularLabel(),
      ],
    ));

    $parentWrapped->getEntity()->set(
      // @todo Method to set this on the wrapped entity.
      'operation_status', $wrappedEntity->getStatus(),
    );
    // @todo Figure out whether to create a new revision of the refering entity.
    #$parentWrapped->getEntity()->createNewRevision(false);
    $parentWrapped->save();

    // @todo Validation and return return Command::FAILURE if failure?
    return Command::SUCCESS;

  }

}
