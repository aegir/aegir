<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Value;

use Drupal\aegir_operation\Value\OperationStatusInterface;
use Drupal\Core\TypedData\ListInterface;
use function preg_match;
use function strpos;

/**
 * Ægir operation status value object.
 */
enum OperationStatus: string implements OperationStatusInterface {

  case unreachable = 'unreachable';

  case failed = 'failed';

  case changed = 'changed';

  case ok = 'ok';

  case dispatched = 'dispatched';

  case unknown = 'unknown';

  /**
   * {@inheritdoc}
   *
   * @todo Rework the foreach to use match()?
   *
   * @see https://www.php.net/manual/en/control-structures.match.php
   *
   * @see https://www.php.net/manual/en/language.enumerations.static-methods.php
   */
  public static function fromTaskLogOutput(ListInterface $log): self {

    $line = self::findStatusLine($log);

    // Return early with unknown to avoid trying to parse an empty value that
    // would result in errors.
    if (empty($line)) {
      return self::unknown;
    }

    # @TODO: The Ansible output has changed. Do we need to take the other
    #        status types into account?
    #ok=5\ \ \ \[0m\ changed=0\ \ \ \ unreachable=0\ \ \ \ failed=0\ \ \ \ skipped=0\ \ \ \ rescued=0\ \ \ \ ignored=0
    # @TODO: We used to include a 'warning' status here. But we dropped that
    #        Ansible output plugin. We should either use it, or remove it from
    #        the theme, etc.
    foreach (['unreachable', 'failed', 'changed', 'ok'] as $status) { # Order is important!

      if (self::parseStatusLine($status, $line)) {
        return self::from($status);
      }

    }

    return self::unknown;

  }

  /**
   * Return the line from the log output that contains a status summary.
   *
   * @param \Drupal\Core\TypedData\ListInterface $log
   *
   * @return string
   *
   * @todo Rework the strpos() to use match()?
   *
   * @todo Should this be moved to an Ansible adapter?
   */
  protected static function findStatusLine(ListInterface $log): string {

    foreach ($log as $line) {

      $text = $line->getString();

      if (strpos($text, 'ok=') === false) continue;
      if (strpos($text, 'changed=') === false) continue;
      if (strpos($text, 'unreachable=') === false) continue;
      if (strpos($text, 'failed=') === false) continue;

      return $text;

    }

    return 'unknown';

  }

  /**
   * Extract the value of a given field from the provided log output line.
   *
   * @param string $status
   *   The status to search for.
   *
   * @param string $line
   *   The line to search within.
   *
   * @return string|null
   *
   * @todo Should this be moved to an Ansible adapter?
   */
  protected static function parseStatusLine(string $status, string $line): ?string {

    $regex = '/' . $status . '=(?<' . $status . '>\d+)/';

    preg_match($regex, $line, $matches);

    return $matches[$status];

  }

}
