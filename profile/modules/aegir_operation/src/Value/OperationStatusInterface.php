<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Value;

use Drupal\Core\TypedData\ListInterface;

/**
 * Interface for Ægir operation status value objects.
 */
interface OperationStatusInterface {

  /**
   * Create a status enum from a provided task log.
   *
   * @param \Drupal\Core\TypedData\ListInterface $log
   *
   * @return self
   */
  public static function fromTaskLogOutput(ListInterface $log): self;

}
