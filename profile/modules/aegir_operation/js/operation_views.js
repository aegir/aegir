(function ($) {
  $(".operations-view").ready(function(){

    var refresh_ops;
    var interval;
    function autorefresh_ops() {
      // Schedule a refresh for every 5 second.
      interval = 5000;
      if ( $(".operation-row.dispatched").length ) {
        // Refresh more frequently when there's an operation to wait on.
        interval = 500;
      }

      refresh_ops = setInterval(function() {
        $('.operations-view').trigger('RefreshView');
        clearInterval(refresh_ops);
        autorefresh_ops();
      }, interval);
    }

    // Remove the throbber that'll flash on view refreshes.
    delete Drupal.Ajax.prototype.setProgressIndicatorThrobber;
    delete Drupal.Ajax.prototype.setProgressIndicatorFullscreen;

    autorefresh_ops();
  })
})(jQuery);

