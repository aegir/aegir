<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Base class for Ægir kernel tests.
 */
abstract class AegirKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   *
   * @todo Fix all of our missing config schema and remove this so they can be
   *   checked during tests and new issues caught.
   *
   * @see https://www.drupal.org/project/config_inspector
   *   Use to find and fix schema errors.
   *
   * @see https://www.drupal.org/project/bootstrap/issues/2860072
   *   Bootstrap schema error; may no longer be an issue so remove this @see if
   *   so.
   */
  protected $strictConfigSchema = false;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->setInstallProfile('aegir');

  }

}
