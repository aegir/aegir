<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir\Functional;

use Drupal\Tests\aegir\Functional\AegirFunctionalTestBase;

/**
 * Ægir installation profile functional tests.
 *
 * @group aegir
 *
 * @group aegir_profile
 */
class AegirProfileTest extends AegirFunctionalTestBase {

  /**
   * Test logging in and navigating to /admin.
   */
  public function testBasicAdminLogin(): void {

    $basicAdmin = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
    ]);

    $this->drupalLogin($basicAdmin);

    $this->drupalGet('admin');

    $this->assertSession()->pageTextContains('Administration');

  }

  /**
   * Test that there are no pending updates right after installation.
   */
  public function testNoPendingUpdates(): void {

    $superUser = $this->drupalCreateUser([], [], true);

    $this->drupalLogin($superUser);

    $this->drupalGet('update.php/selection');

    $this->assertSession()->pageTextContains('No pending updates.');

    $this->assertFalse(
      $this->container->get('entity.definition_update_manager')->needsUpdates(),
      'Entity schema should be up to date after installation but is not!',
    );

  }

}
