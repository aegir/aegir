<?php

declare(strict_types=1);

namespace Drupal\Tests\aegir\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base class for Ægir functional tests.
 */
abstract class AegirFunctionalTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'eldir';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'aegir';

  /**
   * {@inheritdoc}
   *
   * @todo Fix all of our missing config schema and remove this so they can be
   *   checked during tests and new issues caught.
   *
   * @see https://www.drupal.org/project/config_inspector
   *   Use to find and fix schema errors.
   *
   * @see https://www.drupal.org/project/bootstrap/issues/2860072
   *   Bootstrap schema error; may no longer be an issue so remove this @see if
   *   so.
   */
  protected $strictConfigSchema = false;

}
