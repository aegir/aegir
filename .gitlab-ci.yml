stages:
  - test
#  - notify
#  - analyze
  - publish
#  - release

variables:
  LOCAL_DIR: "$CI_PROJECT_DIR/build/local"
  DEBIAN_FRONTEND: "noninteractive"
  PYTHONUNBUFFERED: "true"
  COMPOSER_CACHE_DIR: "$CI_PROJECT_DIR/build/composer/cache"
  # ANSIBLE_CONFIG: "$CI_PROJECT_DIR/build/ansible/ansible.cfg"
  # ANSIBLE_FORCE_COLOR: "true"
  # ANSIBLE_PLAYBOOK: "$CI_PROJECT_DIR/build/ansible/playbook.yml"
  # ANSIBLE_ROLES_PATH: "$CI_PROJECT_DIR/build/ansible/roles/"
  # ANSIBLE_ROLE_FILE: "$CI_PROJECT_DIR/build/ansible/requirements.yml"
  # ANSIBLE_INVENTORY: "$CI_PROJECT_DIR/build/ansible/hosts"
  # ANSIBLE_EXTRA_VARS: "aegir_profile_path=$CI_PROJECT_DIR/profile dispatcherd_service_type=daemon add_hostname_to_hosts_file=false"
  # AEGIR_PLATFORM_PATH: "/var/aegir/platforms/aegir"
  # IRC_CHANNEL: "#aegir-dev"
  # IRC_USER: "hefring-build"
  # IRC_PROJECT: "aegir"
  # IRC_BRANCH: "$CI_BUILD_REF_NAME"
  # IMAGE_REGISTRY_USERNAME: "hefring"
  # IMAGE_REGISTRY_SERVER: "registry.gitlab.com"
  # XDG_RUNTIME_DIR: "/run/user/1000"
  GIT_SUBMODULE_STRATEGY: recursive

cache:
  key: "$CI_BUILD_REF_NAME"  # Cache is per-branch.
  paths:
    - "$LOCAL_DIR"    # Drumkit tools; includes Ansible.
    - "$COMPOSER_CACHE_DIR"

# Hidden job intended as a template for tests.
#
# @see https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#anchors
.test-base: &test-base
  stage: test

  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    # Allow service containers to see each other.
    #
    # @see https://docs.gitlab.com/ee/ci/services/#connecting-services
    FF_NETWORK_PER_BUILD: 'true'
    # Remove "umask 0000" usage, so DDEV has permissions on the cloned
    # repository.
    #
    # @see https://docs.gitlab.com/runner/configuration/feature-flags.html#available-feature-flags
    FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR: 'true'

  services:
    - name: docker:20-dind
      alias: docker
      command: ["--tls=false"]

  image:
    name: registry.gitlab.com/consensus.enterprises/drumkit/ddev
    # Options to run the Docker executor with; notably run as the 'ddev' user
    # and group. This must match the user and group that the Drumkit DDEV image
    # creates.
    #
    # @see https://docs.gitlab.com/ee/ci/yaml/index.html#imagedocker
    docker:
      user: ddev:ddev
    # We have to override the container entrypoint or else we end up in /bin/sh
    # and `source d` doesn't work.
    #
    # @see https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#overriding-the-entrypoint-of-an-image
    entrypoint: [""]

  before_script:
    # Opt into sending diagnotistic data to the DDEV project to help improve it.
    #
    # @see https://ddev.readthedocs.io/en/stable/users/usage/diagnostics/
    - ddev config global --instrumentation-opt-in=true
    # Disable the DDEV SSH agent container as it fails its health checks and we
    # shouldn't need it.
    - ddev config global --omit-containers=ddev-ssh-agent
    - ddev mutagen reset && ddev config global --mutagen-enabled=false
    # Bootstrap Drumkit.
    - source d
    - make start
    # Bypass the ancient make build that will result in Drumkit trying to run
    # Composer outside of DDEV and then predictably failing because there's no
    # "php" executable to be found.
    #
    # Note that we have to disable the merge plug-in to work around it running a
    # stealth 'composer update' on a fresh check out, potentially causing
    # untested package updates to break things. After the first install,
    # we-renable it and run a second install which shouldn't cause it to
    # run 'composer update'.
    #
    # @todo Fix 'make build' so it works in CI and we don't have to do this here
    #   and there as well.
    #
    # @see https://gitlab.com/consensus.enterprises/drumkit/-/issues/107
    #
    # @see https://gitlab.com/aegir/aegir/-/issues/206
    #
    # @see https://github.com/wikimedia/composer-merge-plugin/issues/252#issuecomment-2278613428
    - ddev composer config allow-plugins.wikimedia/composer-merge-plugin false
    - ddev composer install --no-progress
    - ddev composer config allow-plugins.wikimedia/composer-merge-plugin true
    - ddev composer install --no-progress
    - make install workers

test:behat:
  <<: *test-base
  script:
    - make run-behat SUITE=ci

test:phpunit:
  <<: *test-base
  script:
    - make run-phpunit

#notify:irc:failure:
#  stage: notify
#  script:
#    - make irc-ci-failure
#  when: on_failure
#  only:
#    - branches@aegir/aegir
#  allow_failure: true
#
#notify:irc:success:
#  stage: notify
#  script:
#    - make irc-ci-success
#  when: on_success
#  only:
#    - branches@aegir/aegir
#  allow_failure: true

# @TODO: Re-enable analysis steps once we get queue tests running under CI.
#analysis:coverage:
#  stage: analyze
#  image: registry.gitlab.com/aegir/aegir:base-latest
#  before_script:
#    - . d
#    # Install Ansible roles.
#    - ansible-galaxy install -v -r $ANSIBLE_ROLE_FILE -p $ANSIBLE_ROLES_PATH
#    # Build platform and install an Aegir site.
#    - ansible-playbook $ANSIBLE_PLAYBOOK -i $ANSIBLE_INVENTORY -e "$ANSIBLE_EXTRA_VARS"
#  script:
#    - make coverage-init
#    - make cover-all SUITE="ci"
#  artifacts:
#    paths:
#      # Save generated HTML code-coverage report.
#      - build/coverage
#  allow_failure: true
#
#analysis:lint:
#  stage: analyze
#  image: registry.gitlab.com/aegir/aegir:base-latest
#  before_script:
#    - . d
#    # Install Ansible roles.
#    - ansible-galaxy install -v -r $ANSIBLE_ROLE_FILE -p $ANSIBLE_ROLES_PATH
#    # Build platform and install an Aegir site.
#    - ansible-playbook $ANSIBLE_PLAYBOOK -i $ANSIBLE_INVENTORY -e "$ANSIBLE_EXTRA_VARS"
#  script:
#    - make lint
#  allow_failure: true

pages:
  stage: publish
  image:
    name: registry.gitlab.com/consensus.enterprises/drumkit/base:latest
    entrypoint: [""]

  before_script:
    # Check the current version of Hugo, so we can keep our local env in sync.
    - . d
    - make hugo
    - hugo version
  script:
    # Build our documentation site.
    - hugo -s docs
    # Copy HTML code-coverage report.
#    - cp -r build/coverage/html/merged/ public/coverage
  artifacts:
    paths:
      - public
#  when: on_success
  when: always
  allow_failure: true
  only:
    - main@aegir/aegir

# @TODO
#publish:api:
#  stage: publish
#  script:
#    - TBD  # Trigger refresh of http://api.aegirproject.org.
#  when: on_success
#  only:
#    - main@aegir/aegir

#release:docker:xenial:
#  stage: release
#  image: registry.gitlab.com/aegir/aegir:docker-latest
#  before_script:
#    - . d
#    - make packer
#    - service docker start
#  script:
#    - packer build build/packer/docker/xenial.json
##  only:
##    - main@aegir/aegir
#  when: manual
#
#release:docker:docker:
#  stage: release
#  image: registry.gitlab.com/aegir/aegir:docker-latest
#  before_script:
#    - . d
#    - make packer
#    - service docker start
#  script:
#    - packer build build/packer/docker/docker.json
##  only:
##    - main@aegir/aegir
#  when: manual
#
#release:docker:base:
#  stage: release
#  image: registry.gitlab.com/aegir/aegir:docker-latest
#  before_script:
#    - . d
#    - make packer
#    - service docker start
#  script:
#    - packer build build/packer/docker/base.json
##  only:
##    - main@aegir/aegir
#  when: manual

# @TODO
#release:vagrant:
#  stage: release
#  script:
#    - packer build build/packer/vagrant/xenial.json
#  only:
#    - main@aegir/aegir

# @TODO
#release:amazon:
#  stage: release
#  script:
#    - packer build build/packer/amazon/xenial.json
#  only:
#    - main@aegir/aegir

# @TODO
#release:debian:
#  stage: release
#  script:
#    - TBD  # Build and upload Debian packages
#  only:
#    - main@aegir/aegir

