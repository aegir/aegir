#!/bin/bash

# This directory gets created during the image build as the 'aegir' user, but
# since we're running under DDEV which runs nginx as your current user, this
# causes nginx to fail with a 500 error before it can even boot up Drupal if you
# submit some forms, such as /admin/config/regional/content-language
#
# @see https://gitlab.com/aegir/aegir/-/issues/207
#
# @todo Remove this if/when we standardize running nginx and other things under
#   the 'aegir' user.
sudo chown -R "$(id -u):$(id -g)" /var/cache/nginx
