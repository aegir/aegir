#!/usr/bin/python
# Make coding more python3-ish, this is required for contributions to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.plugins.action import ActionBase
from ansible.module_utils._text import to_text
from celery import Celery

class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):
        '''
        Update the value of a front-end field.
        '''

        result = {}

        dispatcherd = Celery('aegir', broker='pyamqp://aegir:53cr3t!@rabbitmq//')

        dispatcherd.conf.update(
          task_serializer='json',
          result_serializer='json',
          result_backend = 'redis://redis/0',
        )

        # @TODO: incorporate this into a conditional based on "debug" flag to the module
        # For debugging purposes, this will dump task variable data into Ansible output.
        # result['msg'] = to_text(task_vars['aegir_uuids'])
        # result['msg'] = to_text(self._task.args)

        result['_ansible_verbose_always'] = True

        config = data = {}

        uuids = task_vars['aegir_uuids']
        config['uuid'] = uuids['operation']

        dispatcherd.send_task('dispatcherd.relay_aegir_log',
                queue='dispatcherd', args=[ config, self._task.args['data'] ])

        return result
