# Run a local Docker registry to test building images and configure DDEV to use
# them.
#
# @see https://www.allisonthackston.com/articles/local-docker-registry.html
#
# @see https://www.docker.com/blog/how-to-use-your-own-registry-2/

LOCAL_REGISTRY_HOST ?= localhost
LOCAL_REGISTRY_PORT ?= 5000
LOCAL_REGISTRY_NAME ?= aegir-registry

CONTAINER_REGISTRY_HOST_LOCAL ?= $(LOCAL_REGISTRY_HOST):$(LOCAL_REGISTRY_PORT)

ifeq ($(shell test -s $(DDEV_CONFIG_DIR)/config.local-registry.yaml && echo yup),yup)
  LOCAL_REGISTRY_ACTIVE = true
  CONTAINER_REGISTRY_HOST=$(CONTAINER_REGISTRY_HOST_LOCAL)
else
  LOCAL_REGISTRY_ACTIVE = false
endif

# @todo Start using the above to provide nicer error messages if you try to
#  start or stop the registry when it's already running or not running,
#  respectively.

write-local-registry-config:
	@echo '{"CONTAINER_REGISTRY_URL": "$(CONTAINER_REGISTRY_URL)"}' | mustache $(DDEV_TEMPLATES_DIR)/config.local-registry.yaml.tmpl > $(DDEV_CONFIG_DIR)/config.local-registry.yaml
	@echo '{"CONTAINER_REGISTRY_URL": "$(CONTAINER_REGISTRY_URL)"}' | mustache $(DDEV_TEMPLATES_DIR)/docker-compose.override.yaml.tmpl > $(DDEV_CONFIG_DIR)/docker-compose.override.yaml

config-local-registry: mustache
	@$(ECHO) "$(YELLOW)Configuring DDEV to use the local Docker registry.$(RESET)"
	@make -s write-local-registry-config CONTAINER_REGISTRY_HOST=$(CONTAINER_REGISTRY_HOST_LOCAL)
	@$(ECHO) "$(YELLOW)To remove these changes and revert to using the remote registry again, run:$(RESET)"
	@$(ECHO) ""
	@$(ECHO) "$(YELLOW)    make unconfig-local-registry$(RESET)"

unconfig-local-registry:
	@$(ECHO) "$(YELLOW)Removing DDEV local Docker registry configuration.$(RESET)"
	@rm -f $(DDEV_CONFIG_DIR)/config.local-registry.yaml $(DDEV_CONFIG_DIR)/docker-compose.override.yaml

start-local-registry: config-local-registry
# Leading new line since we run config-local-registry first.
	@$(ECHO) ""
	@$(ECHO) "$(YELLOW)Starting local Docker registry.$(RESET)"
# Note that the internal port is not intended to be configurable so left as
# hard-coded. Also note that "docker run" doesn't respect the --quiet flag so
# we have to redirect the output.
#
# @see https://github.com/docker/cli/pull/3377
	@docker run -d -p $(LOCAL_REGISTRY_PORT):5000 --name $(LOCAL_REGISTRY_NAME) registry > /dev/null

stop-local-registry: unconfig-local-registry
	@$(ECHO) "$(YELLOW)Stopping local Docker registry.$(RESET)"
	@docker stop $(LOCAL_REGISTRY_NAME) > /dev/null

remove-local-registry: stop-local-registry
	@$(ECHO) "$(YELLOW)Removing local Docker registry.$(RESET)"
	@docker rm $(LOCAL_REGISTRY_NAME) > /dev/null

local-registry: start-local-registry
