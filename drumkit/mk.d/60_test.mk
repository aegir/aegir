SUITE ?= default
BEHAT_SUITE = --suite='$(SUITE)'
ifdef TAGS
    BEHAT_TAGS = --tags='$(TAGS)'
endif

PHPUNIT_GROUP ?= aegir
ifdef PHPUNIT_GROUP
		PHPUNIT_GROUP_PARAM = --group $(PHPUNIT_GROUP)
endif

run-behat: fixtures
	@$(DDEV) behat $(BEHAT_TAGS) $(BEHAT_SUITE)
	@$(MAKE-QUIET) fixtures-remove

wip: run-behat-wip

# This enables or disables Drupal test extension discovery. By default, Drupal
# does not scan under <extension-name>/tests for modules, themes, and so on for
# performance and security reasons, but can be instructed to do so via the
# 'extension_discovery_scan_tests' settings.php value.
#
# @see https://www.drupal.org/project/drupal/issues/2327095
drupal-test-extension-discovery:
# We have to to make the site directory and settings.php temporarily writable as
# it's usually write-protected for security as per Drupal best practices.
	@chmod u+w \
		$(PROJECT_ROOT)/web/sites/$(SITE_URL) \
		$(PROJECT_ROOT)/web/sites/$(SITE_URL)/settings.php
# This is kind of ugly but works. Note that the 'required' key is necessary to
# force writing this setting because it's not usually found in settings.php but
# instead in settings.local.php, which is not included by default and would
# require more work to include.
#
# @todo Can we rework this into a Drush command?
	@$(DRUSH) php:eval "\Drupal\Core\Site\SettingsEditor::rewrite('sites/$(SITE_URL)/settings.php', ['settings' => ['extension_discovery_scan_tests' => (object) ['value' => $(VALUE), 'required' => true]]]);"
# Now that we've updated the settings.php, remove write permissions.
	@chmod u-w \
		$(PROJECT_ROOT)/web/sites/$(SITE_URL) \
		$(PROJECT_ROOT)/web/sites/$(SITE_URL)/settings.php

fixtures:
	@$(MAKE-QUIET) VALUE=true drupal-test-extension-discovery
	@$(DRUSH) pm:install -y aegir_input_command_test aegir_log_command_test

fixtures-remove:
# Disabled for the time being as config is not fully removed, preventing these
# modules from being installed again.
#
# @see https://gitlab.com/aegir/aegir/-/issues/203
# 	@$(DRUSH) pm:uninstall -y aegir_input_command_test aegir_log_command_test
	@$(MAKE-QUIET) VALUE=false drupal-test-extension-discovery

run-behat-wip: fixtures
	@$(DDEV) behat $(BEHAT_SUITE) --tags="@wip&&~@disabled"
	@$(MAKE-QUIET) fixtures-remove

# @todo Make the browser output removal optional?
run-phpunit: phpunit-clean
	@$(DDEV) exec phpunit $(PHPUNIT_GROUP_PARAM)

phpunit-clean:
	@$(ECHO) "$(YELLOW)Removing any existing PHPUnit browser output.$(RESET)"
	@rm -rf $(PROJECT_ROOT)/web/sites/simpletest/browser_output

tests: run-phpunit run-behat ##@aegir Ægir: Run PHPUnit and Behat tests.
