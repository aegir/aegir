# Set some variables for use across commands.
SITE_URL  = aegir.ddev.site
SITE_NAME = "Ægir"

ADMIN_USER = dev
ADMIN_PASS = pwd
INSTALL_PROFILE = aegir

DB_NAME = db
DB_USER = db
DB_PASS = db
DB_HOST = db

TIMESTAMP = $(shell date +%s)
TMP_DIR   = tmp

# Suppress Make-specific output, but allow for greater verbosity.
VERBOSE := 1
QUIET   :=  
ifeq ($(VERBOSE), 0)
    MAKE-QUIET = $(MAKE) -s
    QUIET      = > /dev/null
    DRUSH_VERBOSE =
else
    MAKE-QUIET = $(MAKE)
    DRUSH_VERBOSE = --verbose
endif

# Allow debug output
DEBUG := 0
ifeq ($(DEBUG), 0)
    DRUSH_DEBUG =
else
    DRUSH_DEBUG = --debug
endif

# Normalize local development and CI commands.
DDEV = $(shell which ddev)
ifeq ($(DDEV),)
    DRUSH_CMD = ./bin/drush
    APP_PATH  =
    COMPOSER  = composer --ansi
else
    DRUSH_CMD = $(DDEV) drush
    APP_PATH  = /app/
    COMPOSER  = $(DDEV) exec composer --ansi
endif
DRUSH = $(DRUSH_CMD) --uri=$(SITE_URL) $(DRUSH_VERBOSE) $(DRUSH_DEBUG)

# Colour output. See 'help' for example usage.
ECHO       = @echo -e
BOLD       = \033[1m
RESET      = \033[0m
make_color = \033[38;5;$1m  # defined for 1 through 255
GREEN      = $(strip $(call make_color,22))
GREY       = $(strip $(call make_color,241))
RED        = $(strip $(call make_color,124))
WHITE      = $(strip $(call make_color,255))
YELLOW     = $(strip $(call make_color,94))

# Drumkit variables to get specific tool versions
ansible_RELEASE = v2.9.22
ansible-galaxy_RELEASE = v2.9.22
ansible-playbook_RELEASE = v2.9.22
packer_RELEASE  = 1.11.1
