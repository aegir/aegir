CONTAINER_REGISTRY_HOST ?= registry.gitlab.com
# Don't use '?=' assignment because then this will always defer to the value set
# in .mk/mk/projects/k8s_image/images.mk
#
# @see https://gitlab.com/aegir/aegir/-/issues/199
CONTAINER_REGISTRY_URL  = $(CONTAINER_REGISTRY_HOST)/aegir/aegir
CONTAINER_PROJECT_NAME  = aegir
DOCKER_IMAGES           = docker/relayd-base docker/relayd docker/dispatcherd-base docker/dispatcherd docker/minion-base docker/minion #docker/rabbitmq
DOCKER_TAG_BRANCH      ?= FALSE

.mk/.local/bin/packer:
	$(MAKE) packer

docker-login:
	docker login registry.gitlab.com

ci-check-deps:
	@if [ -z ${CONTAINER_SCRIPT} ]; then echo -e "$(YELLOW)Missing required variable $(GREY)CONTAINER_SCRIPT$(YELLOW).$(RESET)"; exit 1; fi
	@if [ -z ${CONTAINER_PROJECT_NAME} ]; then echo -e "$(YELLOW)Missing required variable $(GREY)CONTAINER_PROJECT_NAME$(YELLOW).$(RESET)"; exit 1; fi
	@if [ -z ${CONTAINER_REGISTRY_URL} ]; then echo -e "$(YELLOW)Missing required variable $(GREY)CONTAINER_REGISTRY_URL$(YELLOW).$(RESET)"; exit 1; fi

ci-image: .mk/.local/bin/packer ci-check-deps
	@echo "Building packer image for CI: $(CONTAINER_SCRIPT)"
ifeq ($(DOCKER_TAG_BRANCH),FALSE)
	@packer build -except=tag-branch $(CONTAINER_SCRIPT)
else
	@echo "Tagging package image with git tag/branch: $(DOCKER_TAG_BRANCH)"
	@packer build $(CONTAINER_SCRIPT)
endif

# To push images with a particular Docker tag, use: DOCKER_TAG_BRANCH=0.0.x-pre-Alpha0 make ci-images
ci-images: images-intro-text $(DOCKER_IMAGES)
images-intro-text:
	@echo "Building packer images for CI."
	@echo "Using project name: $(CONTAINER_PROJECT_NAME)"
	@echo "Using container registry: $(CONTAINER_REGISTRY_URL)"

$(DOCKER_IMAGES):
	make -s ci-image CONTAINER_SCRIPT=build/packer/$@.json CONTAINER_PROJECT_NAME=$(CONTAINER_PROJECT_NAME) CONTAINER_REGISTRY_URL=$(CONTAINER_REGISTRY_URL) DOCKER_TAG_BRANCH=$(DOCKER_TAG_BRANCH)
