# Start and destroy DDEV environments.

DDEV_TEMPLATES_DIR = $(PROJECT_ROOT)/drumkit/files/ddev
DDEV_CONFIG_DIR = $(PROJECT_ROOT)/.ddev

.PHONY: start stop off destroy

start: pull-images ## Start DDEV containers.
	@$(MAKE-QUIET) start-real
start-real:
	@$(ECHO) "$(YELLOW)Starting DDEV containers. (Be patient. This may take a while.)$(RESET)"
	ddev start $(QUIET)
	@$(ECHO) "$(YELLOW)Started DDEV containers.$(RESET)"

pull-images:
	@docker pull $(CONTAINER_REGISTRY_URL)/relayd
	@docker pull $(CONTAINER_REGISTRY_URL)/dispatcherd

stop: ## Stop DDEV containers.
	@$(MAKE-QUIET) stop-real
stop-real:
	@$(ECHO) "$(YELLOW)Stopping DDEV containers.$(RESET)"
	ddev stop $(QUIET)
	@$(ECHO) "$(YELLOW)Stopped DDEV containers.$(RESET)"

off: ## Power off DDEV containers
	@$(MAKE-QUIET) off-real
off-real:
	@$(ECHO) "$(YELLOW)Powering off DDEV.$(RESET)"
	ddev poweroff $(QUIET)
	@$(ECHO) "$(YELLOW)DDEV powered off.$(RESET)"

destroy: ## Destroy DDEV containers
	@$(MAKE-QUIET) destroy-real
destroy-real:
	@$(ECHO) "$(YELLOW)Destroying DDEV containers.$(RESET)"
	ddev stop --remove-data $(QUIET)
	@$(ECHO) "$(YELLOW)Destroyed DDEV containers.$(RESET)"

restart: ## Rebuild DDEV containers
	@$(MAKE-QUIET) restart-real
restart-real:
	@$(ECHO) "$(YELLOW)Restarting DDEV containers.$(RESET)"
	ddev restart $(QUIET)
	@$(ECHO) "$(YELLOW)Restarted DDEV containers.$(RESET)"

reboot: ## Stop/delete, then restart all DDEV containers
	@$(MAKE-QUIET) reboot-real
reboot-real:
	@$(ECHO) "$(YELLOW)Rebooting DDEV containers.$(RESET)"
	ddev stop -O --remove-data $(QUIET)
	ddev start $(QUIET)
	@$(ECHO) "$(YELLOW)Rebooted DDEV containers.$(RESET)"

delete: ## Delete DDEV project (and containers)
	@$(MAKE-QUIET) delete-real
delete-real:
	@$(ECHO) "$(YELLOW)Deleting DDEV project.$(RESET)"
	ddev delete $(QUIET)
	@$(ECHO) "$(YELLOW)Deleted DDEV project.$(RESET)"
