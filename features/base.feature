Feature: Base settings and configuration
  In order to deploy a complete Ægir system
  as an Administrator,
  I want to be able to enable a set of default functionality and configuration on install

  Scenario: Ensure some core utility modules are enabled
    Given I run "drush pm:list --status=enabled --type=module --format=list"
     Then I should get:
     """
     block
     dblog
     page_cache
     dynamic_page_cache
     views
     """
