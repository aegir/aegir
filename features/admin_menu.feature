Feature: Add admin menu
  In order to develop new features effectively,
  as a developer,
  I want to be able to efficiently navigate Drupal configuration settings.

  @api
  Scenario: Only administrators should have access to the admin menu.
    Given I am not logged in
     When I am on the homepage
     Then I should not see the link "Manage"
    Given I am logged in as an "Authenticated user"
     Then I should not see the link "Manage"
    Given I am logged in as an "Administrator"
     Then I should see the link "Manage"
