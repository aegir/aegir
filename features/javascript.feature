@smoke-test @api @chromedriver
Feature: Chromedriver is available to run JavaScript.
  In order to test JavaScript functionality
  As an Developer
  I need to be able to run tests through Selenium and Chrome.

  Scenario: Confirm that login works without JavaScript.
    Given I am logged in as a "Administrator"

  @javascript
  Scenario: Confirm that login works with JavaScript (via Selenium and Chrome).
    Given I am logged in as a "Administrator"
