<?php

declare(strict_types=1);

use Drumkit\DrumkitContext;

/**
 * This Behat Feature Context class contains Aegir-specific step definitions
 * for using Aegir from the CLI.
 */
class AegirCliContext extends DrumkitContext {
}
