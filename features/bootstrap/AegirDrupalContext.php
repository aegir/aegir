<?php

declare(strict_types=1);

use Drupal\DrupalExtension\Context\DrupalContext;
use Drupal\DrupalExtension\Hook\Scope\BeforeUserCreateScope;
use Behat\Behat\Hook\Scope\AfterFeatureScope;
use Behat\Gherkin\Node\TableNode;

/**
 * This Behat Feature Context class contains Aegir-specific step definitions
 * for testing Aegir from the web UI.
 */
class AegirDrupalContext extends DrupalContext {

  /**
   * Array of operation entities created during tests.
   *
   * @var array
   */
  protected $operations = [];

  /**
   * @AfterFeature
   */
  public static function cleanEntities(AfterFeatureScope $scope) {

    # All test entities should be prefixed for easy identification.
    $prefix = 'TEST_';

    # List of entity and bundle types to clean up after.
    $types = [
      'deployment_target' => [
        'entity_type' => 'aegir_deployment_target_type',
        'entity' => 'aegir_deployment_target',
        'test_modules' => [
          'aegir_deployment_target_examples',
        ],
      ],
      'examples' => [
        'entity_type' => 'aegir_example_type',
        'entity' => 'aegir_example',
        'test_modules' => [
          'aegir_test_example',
        ],
      ],
      'operations' => [
        'entity_type' => 'aegir_operation_type',
        'entity' => 'aegir_operation',
        'test_modules' => [
          'aegir_test_operation',
        ],
      ],
      'platforms' => [
        'entity_type' => 'aegir_platform_type',
        'entity' => 'aegir_platform',
        'test_modules' => [
          'aegir_test_platform',
        ],
      ],
      'sites' => [
        'entity_type' => 'aegir_site_type',
        'entity' => 'aegir_site',
        'test_modules' => [
          'aegir_test_site',
        ],
      ],
      'tasks' => [
        'entity_type' => 'aegir_task_type',
        'entity' => 'aegir_task',
        'test_modules' => [
          'aegir_test_task',
        ],
      ],
    ];

    $tags = $scope->getFeature()->getTags();
    foreach ($tags as $tag) {
      if (array_key_exists($tag, $types)) {
        print("Cleaning up test '{$tag}' entities and bundles.\n");
        $data = $types[$tag];
        $query = \Drupal::entityQuery($data['entity_type']);

        /** @var \Drupal\Core\Entity\EntityTypeInterface */
        $typeEntityType = \Drupal::entityTypeManager()->getStorage(
          $data['entity_type'],
        )->getEntityType();

        /** @var \Drupal\Core\Entity\EntityTypeInterface */
        $entityType = \Drupal::entityTypeManager()->getStorage(
          $data['entity'],
        )->getEntityType();

        $bundles = $query
          ->accessCheck(false)
          ->condition($typeEntityType->getKey('label'), $prefix, 'STARTS_WITH')
          ->execute();
        foreach ($bundles as $bundleId) {
          $query = \Drupal::entityQuery($data['entity']);
          $entity_ids = $query
            ->accessCheck(false)
            ->condition($entityType->getKey('bundle'), $bundleId)
            ->condition($entityType->getKey('label'), $prefix, 'STARTS_WITH')
            ->execute();
          $entities = \Drupal::entityTypeManager()
            ->getStorage($data['entity'])
            ->loadMultiple($entity_ids);
          foreach ($entities as $id => $entity) {
            print('  Deleting ' . $entity->label() . ' ' . $entity->getEntityType()->getSingularLabel() . ' (bundle: ' . $bundleId . ")\n");
            $entity->delete();
          }

          /** @var \Drupal\Core\Entity\EntityStorageInterface */
          $bundleStorage = \Drupal::entityTypeManager()->getStorage(
            $entityType->getBundleEntityType(),
          );
          $bundleEntity = $bundleStorage->load($bundleId);

          if ($bundleEntity) {

            print(
              '  Deleting ' . $bundleId . ' ' .
              $typeEntityType->getSingularLabel() . "\n"
            );

            $bundleStorage->delete([$bundleEntity]);

          }
        }
        print('Disabling test module(s): ' . implode(',', $data['test_modules']) . "\n");
        \Drupal::service('module_installer')->uninstall($data['test_modules']);
      }
    }
    if (in_array('l10n', $tags)) {
      print("Disabling languages.\n");
      Drupal::configFactory()->getEditable('language.entity.fr')->delete();
      print("Rebuilding caches.\n");
      drupal_flush_all_caches();
    }
  }

  /**
   * Re-bootstrap Drupal before users are created to pick up new user roles.
   *
   * This fixes an edge case where installing a module that provides a new
   * user role via Drush doesn't get picked up by Drupal and/or the Drupal
   * driver, which then results in a fatal error if a feature attempts to then
   * create a user with that new role. The solution seems to be to bootstrap
   * Drupal again before creating a user.
   *
   * @beforeUserCreate
   *
   * @todo While this doesn't seem to be a signficant performance hit, this is
   *   not ideal to do every time a user is to be created; should be reworked
   *   to only do this if a non-existent user role (according to the Drupal
   *   driver) is asked for in case it was just added by a module install.
   *
   * @see profile/modules/devel/modules/example_entity/features/access.feature
   *   Issue was first encountered with the test module here.
   */
  public function rebootstrapUserRoles(BeforeUserCreateScope $scope): void {

    $this->getDrupal()->getDriver()->bootstrap();

  }

  /**
   * @Given I am at :path on the command line
   */
  public function iAmAtOnTheCommandLine($path)
  {
    chdir($path);
  }

  /**
   * @When I wait :seconds seconds
   */
  public function iWaitSeconds($seconds)
  {
    sleep((int) $seconds);
  }

  /**
   * Creates operation of a given type provided in the form:
   * | id | type                     | name                  | uuid |
   * | 10 | example_update_operation | Test update operation | ...  |
   * | ...| ...                      | ...                   | ...  |
   *
   * @Given :type operations:
   */
  public function createOperations($type, TableNode $operationsTable) {
    foreach ($operationsTable->getHash() as $operationHash) {
      $operation = (object) $operationHash;
      $operation->type = $type;
      $this->operationCreate($operation);
    }
  }

  /**
   * Create an operation, calling the underlying Drupal driver.
   *
   * @return object
   *   The created operation.
   */
  public function operationCreate($operation) {
    // @TODO: implement OperationCreateScope classes, if needed.
    $this->parseEntityFields('aegir_operation', $operation);
    $saved = $this->getDriver()->createEntity('aegir_operation', $operation);
    $this->operations[] = $saved;
  }

  /**
   * Switch to using the document embedded in the given iframe.
   *
   * @Given I am on the :iframe iframe
   */
  public function iAmOnTheIFrame($iframe) {
    $frame = $this->awaitElement("iframe[name='$iframe']", 30)
        ->getAttribute('name');

    $this->getSession()->switchToIFrame($frame);
  }

  /**
   * Remove any created operations.
   *
   * @AfterScenario
   */
    public function cleanOperations()
    {
        // Remove any nodes that were created.
        foreach ($this->operations as $operation) {
            $this->getDriver()->entityDelete('aegir_operation', $operation);
        }
        // @TODO: Ensure that tasks get cleaned up properly too, if needed.
        $this->operations = [];
    }

  /**
   * @When I run the :operation operation
   */
    public function runOperation($operation)
    {
        $this->assertClickInTableRow("Run", $operation);
    }

    /**
     * @When I view the :operation operation
     */
    public function viewOperation($operation)
    {
        $this->assertClickInTableRow("View", $operation);
    }

    /**
     * @When I dispatch the operation in the :region region
     */
    public function dispatchOperation($region)
    {
        $button = "Dispatch";
        $regionObj = $this->getRegion($region);

        $buttonObj = $regionObj->findButton($button);
        dsm($buttonObj);
        if (empty($buttonObj)) {
            throw new \Exception(sprintf("The button '%s' was not found in the region '%s' on the page %s", $button, $region, $this->getSession()->getCurrentUrl()));
        }
        $regionObj->pressButton($button);
    }

    /**
     * Return a region from the current page.
     *
     * @throws \Exception
     *   If region cannot be found.
     *
     * @param string $region
     *   The machine name of the region to return.
     *
     * @return \Behat\Mink\Element\NodeElement
     */
    protected function getRegion($region) {
        $session = $this->getSession();
        $regionObj = $session->getPage()->find('region', $region);
        if (!$regionObj) {
            throw new \Exception(sprintf('No region "%s" found on the page %s.', $region, $session->getCurrentUrl()));
        }

        return $regionObj;
    }

    /**
     * Install one or more modules.
     *
     * @Given the :moduleNames modules are installed
     *
     * @see https://github.com/jhedstrom/DrupalDriver/issues/239#issuecomment-1061038039
     *   Implemented as recommended in this Drupal Driver issue.
     */
    public function modulesInstalled(string $moduleNames): void {

      // @todo Trim white-space.
      $moduleNamesSplit = \explode(',', $moduleNames);

      $status = \Drupal::service('module_installer')->install(
        $moduleNamesSplit
      );

      if ($status !== true) {
        // @todo Show which ones?
        throw new \Exception('One or more module(s) could not be installed!');
      }

      // Completely clear everything that's cached to ensure all changes,
      // config, entities, etc. take full effect.
      //
      // @todo Rework this so it's more fine-grained and doesn't clear
      //   absolutely everything?
      \drupal_flush_all_caches();

    }

}
