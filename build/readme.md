This directory contains the Ægir Docker image build files.

See https://docs.aegir.hosting/tutorial/developer/building_images/ for details.
