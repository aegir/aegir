#!/bin/sh -eux

# Install Python and related utilities.

export DEBIAN_FRONTEND=noninteractive

apt-get install -yqq \
  python3-minimal \
  python3-pip \
  python3-yaml \
  python3-apt \
  python3-pymysql \
  python3-setuptools \
  python3-wheel \
> /dev/null

# Note that since we're mixing pip and apt, we have to either create a virtual
# environment or pass --break-system-packages to pip to avoid it throwing an
# error. Using --break-system-packages within a container is simpler; outside of
# a container, you really should create a virtual environment instead. We also
# suppress the warning that we're running pip as root, which in this case is
# expected.
#
# Since the version of pip and Python can vary depending on the image, we're
# setting the options using environment variables to avoid the pip commands
# throwing an error if it predates the addition of --break-system-packages and
# --root-user-action=ignore.
#
# @see https://packaging.python.org/en/latest/specifications/externally-managed-environments/#externally-managed-environments

export PIP_BREAK_SYSTEM_PACKAGES='true'
export PIP_ROOT_USER_ACTION='ignore'

update-alternatives --install /usr/bin/python python /usr/bin/python3 1
pip3 install jinja2-cli matrix-client ansible
pip3 install --upgrade jinja2
