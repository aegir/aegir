#!/bin/sh -eux

# Install Python and related utilities.

export DEBIAN_FRONTEND=noninteractive

apt-get install -yqq \
  python3-minimal \
  python3-pip \
  python3-yaml \
  python3-apt \
  python3-setuptools \
  python3-wheel \
> /dev/null

update-alternatives --install /usr/bin/python python /usr/bin/python3 1
