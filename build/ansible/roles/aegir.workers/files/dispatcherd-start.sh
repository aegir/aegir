#!/bin/bash
set -x
set -o errexit nounset pipefail

# @TODO add healthcheck?

exec /usr/bin/supervisord -n
