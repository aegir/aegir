#PROJECT_TYPE   ?= profile
#PROJECT_NAME   ?= aegir
#packer_RELEASE  = 1.4.5
## Override colours in CI messages, since riot.im doesn't appear to support
## them.
#IRC_SUCCESS     = SUCCESS building $(IRC_BRANCH) branch. See $(IRC_REF_URL)
#IRC_FAILURE     = FAILURE building $(IRC_BRANCH) branch. See $(IRC_REF_URL)

include .mk/GNUmakefile

.PHONY: workers dev dev-enable

dev: install dev-enable ##@aegir Ægir: Run Drupal site install and dev-enable

dev-enable: ##@aegir Ægir: Enable aegir_devel for developer tools
	$(DRUSH) pm:enable aegir_devel -y $(QUIET)
	$(DRUSH) cache-rebuild $(QUIET)

workers: ##@aegir Ægir: Run ddev dispatcherd and relayd
	ddev dispatcherd
	ddev relayd
	sleep 1

.DEFAULT_GOAL = help-aegir

#CI_PROJECT_DIR ?= /vagrant
#AEGIR_PLATFORM_PATH ?= /var/aegir/platforms/aegir
#ifdef CLASS
#    SIMPLETEST_CLASS = --class '$(CLASS)'
#endif
#help-aegir: ## Print this help message
#	@echo "help-aegir"
#	@echo "  Print this help message."
#	@echo "run-simpletest"
#	@echo "  Run Simpletest-based tests."
#	@echo "run-behat"
#	@echo "  Run Behat-based tests."
#	@echo "coverage-init"
#	@echo "  Initialize coverage (i.e., install xdebug, etc.)."
#	@echo "run-coverage"
#	@echo "  Run tests to gather code-coverage data."
#	@echo "coverage-text"
#	@echo "  Generate a text code-coverage report (from existing coverage data.)"
#	@echo "coverage-html"
#	@echo "  Generate an HTML code-coverage report (from existing coverage data.)"
#	@echo "boxes"
#	@echo "  Build vagrant boxes for development."
#	@echo "clean-boxes"
#	@echo "  Clean up vagrant boxes."
#
#simpletest-init:
#	@$(run) "sudo -u aegir --login drush @aegir -y en simpletest"
#
## N.B. xdebug-2.4.0 seg faults under Apache, hence 2.4.1+.
#xdebug-init:
#	@$(run) "sudo /bin/bash -c \"echo 'deb http://us.archive.ubuntu.com/ubuntu/ zesty universe' > /etc/apt/sources.list.d/zesty-universe.list\""
#	@$(run) "sudo apt-get update -qq"
#	@$(run) "sudo apt-get install -y php-xdebug -t zesty > /dev/null"
#	@$(run) "sudo apache2ctl restart"
#
#coverage-init: xdebug-init
#	@$(run) "sudo /bin/bash -c 'echo \"memory_limit = -1\" > /etc/php/7.0/cli/conf.d/99-memory.ini'"
#	@$(run) "sudo /bin/bash -c 'echo \"memory_limit = 1024M\" > /etc/php/7.0/fpm/conf.d/99-memory.ini'"
#	@$(run) "sudo /bin/bash -c 'echo \"memory_limit = 1024M\" > /etc/php/7.0/apache2/conf.d/99-memory.ini'"
#	@$(run) "sudo /bin/bash -c 'echo \"SetEnv CI_PROJECT_DIR $(CI_PROJECT_DIR)\" > /etc/apache2/conf-enabled/env-vars.conf'"
#	@$(run) "sudo /bin/bash -c 'echo \"SetEnv AEGIR_PLATFORM_PATH $(AEGIR_PLATFORM_PATH)\" >> /etc/apache2/conf-enabled/env-vars.conf'"
#	@$(run) "sudo apache2ctl restart"
#
#run-simpletest: simpletest-init
#	@$(run) "sudo -u www-data CI_PROJECT_DIR=$(CI_PROJECT_DIR) php $(AEGIR_PLATFORM_PATH)/core/scripts/run-tests.sh --non-html --color --verbose --url http://aegir.vm aegir $(SIMPLETEST_CLASS)"
#
#coverage-dir-writable:
#	@chmod 777 build/coverage/ -R
#
#coverage-clean-php: coverage-dir-writable
#	@rm -rf build/coverage/php/*.cov
#
#coverage-clean-html: coverage-dir-writable
#	@rm -rf build/coverage/html/*/
#
#generate-coverage-text:
#	@$(run) "sudo -u aegir --login phpcov merge $(CI_PROJECT_DIR)/build/coverage/php --text=1"
#
#generate-coverage-html: coverage-clean-html
#	@$(run) "sudo -u aegir --login phpcov merge $(CI_PROJECT_DIR)/build/coverage/php/ --html $(CI_PROJECT_DIR)/build/coverage/html/merged"
#	@echo "Coverage report URL: file://$(shell pwd)/build/coverage/html/merged/index.html"
#
#cover-simpletest: coverage-clean-php add-coverage-prepend run-simpletest remove-coverage-prepend
#
#cover-behat: coverage-clean-php add-coverage-prepend run-behat remove-coverage-prepend
#
#cover-all: coverage-clean-php add-coverage-prepend run-tests remove-coverage-prepend generate-coverage-text generate-coverage-html
#
#add-coverage-prepend:
#	@$(run) "sudo /bin/bash -c 'echo \"auto_prepend_file=$(CI_PROJECT_DIR)/build/coverage/prepend.php\" > /etc/php/7.0/apache2/conf.d/98-coverage.ini'"
#	@$(run) "sudo /bin/bash -c 'echo \"auto_prepend_file=$(CI_PROJECT_DIR)/build/coverage/prepend.php\" > /etc/php/7.0/fpm/conf.d/98-coverage.ini'"
#	@$(run) "sudo /bin/bash -c 'echo \"auto_prepend_file=$(CI_PROJECT_DIR)/build/coverage/prepend.php\" > /etc/php/7.0/cli/conf.d/98-coverage.ini'"
#	@$(run) "sudo apache2ctl restart"
#
#remove-coverage-prepend:
#	@$(run) "sudo rm /etc/php/7.0/apache2/conf.d/98-coverage.ini"
#	@$(run) "sudo rm /etc/php/7.0/fpm/conf.d/98-coverage.ini"
#	@$(run) "sudo rm /etc/php/7.0/cli/conf.d/98-coverage.ini"
#	@$(run) "sudo apache2ctl restart"
#
#lint-init:
## Register Drupal coding standards
#	@$(run) "sudo -u aegir --login /bin/bash -c 'phpcs --config-set installed_paths $(AEGIR_PLATFORM_PATH)/modules/coder/coder_sniffer'"
#	@$(run) "sudo -u aegir --login /bin/bash -c 'phpcs -i'"
#
#lint: lint-init
## Analyze code for Drupal coding standards violations.
#	@$(run) "sudo -u aegir --login /bin/bash -c 'phpcs -a --colors --standard=Drupal,DrupalPractice --extensions=php,module,install,inc -v --ignore=*/bootstrap/*,*.css $(AEGIR_PLATFORM_PATH)/profiles/aegir/' "
#
#lint-fix: lint-init
## Fix code for Drupal coding standards violations.
#	@$(run) "sudo -u aegir --login /bin/bash -c 'phpcbf -vv -p --extensions=php,module,install,inc $(AEGIR_PLATFORM_PATH)/profiles/aegir/'"
#
#backup:
#	@$(run) "sudo -u aegir --login /bin/bash -c 'cd $(AEGIR_PLATFORM_PATH); drush sql-dump > database.backup.sql'"
#
#restore:
#	@$(run) "sudo -u aegir --login /bin/bash -c 'cd $(AEGIR_PLATFORM_PATH); drush sqlc < database.backup.sql'"
#
#re-install: backup
#	@$(run) "sudo -u aegir --login /bin/bash -c 'cd $(AEGIR_PLATFORM_PATH); drush site-install -y --db-url=mysql://root:root@localhost/aegir0 --account-name=admin --account-pass=pwd --site-name=Ægir'"
#
#re-build:
#	$(run) "sudo -u aegir --login /bin/bash -c 'cd $(AEGIR_PLATFORM_PATH); composer update -vvv'"
#
#re-dev: re-install
#	@$(run) "sudo -u aegir --login drush @aegir -y -q en aegir_devel aegir_test_resource"
#	@$(run) "sudo -u aegir --login drush @aegir uli"
#
#re-up:
#	@vagrant destroy -f
#	@vagrant up
#
#logs:
#	@$(run) "sudo tail /var/log/apache2/error.log"
#
#sass-init:
#	sudo apt-get install -y ruby-sass
#
#sass:
#	cd profile/themes/eldir/sass/; sass --watch style.scss:style.css
#
#
#clean-boxes:
#	rm -r build/packer/builds/
#boxes: packer box-ubuntu box-aegir
#box-aegir: build/packer/builds/aegir-dev.box
#build/packer/builds/aegir-dev.box: build/packer/builds/bionic/bionic.ovf
#	cd build/packer; packer build --force vagrant/aegir-dev.json
#box-ubuntu: build/packer/builds/bionic.box
#build/packer/builds/bionic.box: build/packer/builds/bionic/bionic.ovf
#build/packer/builds/bionic/bionic.ovf:
#	cd build/packer; packer build --force vagrant/bionic.json
#
#containers: container-bionic container-base container-docker
#container-bionic:
#	cd build/packer; packer build --force docker/bionic.json
#container-base:
#	cd build/packer; packer build --force docker/base.json
#container-docker:
#	cd build/packer; packer build --force docker/docker.json
