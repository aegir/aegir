---
title: Home
---

Welcome to Ægir
===============

**NB This project documentation is under active development. Suggestions and feedback welcome!**

**NOTE:** This documentation is specific to **Ægir 5.x**, hosted and developed at https://gitlab.com/aegir/aegir.

### Ægir 5.x is a full re-write using Drupal 8.

It is currently in **pre-alpha development**, and thus not yet fully functional.

### [Aegir 3.x](http://docs.aegirproject.org) remains the supported release.

Check out [http://aegir.hosting/](http://aegir.hosting/) for more information. Professional support, training, consulting and other services are offered by the [Ægir Cooperative](http://aegir.coop).

### Ægir is a framework for hosting and managing websites and other applications.

Ægir is built as a [Drupal](https://www.drupal.org) distribution, making it both easy to self-host, integrate with other tools and services, and extend with plugins, themes or custom code.

**See our [Roadmap](/roadmap)!**

This documentation is designed primarily for the following audiences:

* [Users](/howto/getting-started/user): those looking to use Ægir to install and manage a website or other supported application.
* [Administrators](/howto/getting-started/administrator): those looking to host Ægir on their own infrastructure.
* [Developers](/howto/getting-started/developer): those looking to add new features, enhance or fix bugs in existing ones or otherwise extend Ægir.
