---
title: Glossary
weight: 30
---

## Aegir Site Builder Terms

* Platform - codebase
* Site - hosted instance
* Operation - eg. Install a site
* Task - eg. Create a VHost
* Server - VM, container pod, etc.
* Service - mysql, nginx, solr, etc.

## Aegir Admin Terms

## Aegir Backend Developer Terms

* Task Queue
* Message Queue
* AMQP
* Broker
* Result Store
* exchange, routing_key etc. in Celery
