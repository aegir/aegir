---
title: Deployment Targets
weight: 10
---

## Deployment Targets

As part of incorporating our Drumkit backend into Aegir5, we need to build the conceptual model the frontend uses to match the Kubernetes backend concepts.

All required config and tests are in "aegir_deployment_target" module.

### Create and manage Aegir deployment target

Under "Ægir" menu, user with access should see "Deployment Targets". It should go to "admin/aegir/deployment-targets" and list all the Deployment Target content.

Under "Deployment Targets" menu item, should have "Deployment Target types" as sub menu item. It should go to "admin/aegir/deployment-targets/types".

### Access Deployment Target

Currently, only users with "Ægir administrator" and "Deployment Target manager" roles have all the access to "Deployment Targets".

### Testing

"aegir_deployment_target_examples" modules contains an example bundle of Deployment Target, used for testing.
