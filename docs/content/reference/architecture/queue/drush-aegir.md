---
title: Drush Aegir commands
menutitle: Drush Aegir Commands
weight: 30
---

## Drush commands for backend -> frontend communication

We have a handful of Drush commands which allow for `dispatcherd` to send data
back to the frontend. These are wrapped in `relayd` tasks which simply relay
the information into the appropriate `drush` command to post data values
(`aegir:input`), operation log data (`aegir:log`) or exit codes
(`aegir:exitcode`) back to an Operation the backend has run:

### `aegir:input`

The [drush
aegir:input](https://gitlab.com/aegir/aegir/-/blob/main/profile/modules/aegir_input_command/src/Command/AegirInputCommand.php)
command is used for updating field values on a given entity. The backend
provides the type and uuid, as well as field name and data value, and this
command will load the entity, set the value, and re-save.

This corresponds with the [relayd aegir_input
task](https://gitlab.com/aegir/aegir/-/blob/main/backend/relayd.py#L45), which
`dispatcherd` calls by placing a task on the queue for `relayd` to run through
`drush aegir:input`.

### `aegir:log`

The [drush aegir:log](https://gitlab.com/aegir/aegir/-/blob/main/profile/modules/aegir_operation/src/Command/AegirLogCommand.php)
command allows the backend to send Operation log data back to the frontend
while an Operation is running. The command requires a `uuid` for the operation,
as well as `timestamp`, `sequence` (to be deprecated), and `data` values. These
are posted as new values on the multi-value log fields attached to the
Operation.

This corresponds to the [relayd aegir_log
task](https://gitlab.com/aegir/aegir/-/blob/main/backend/relayd.py#L52) that
`dispatcherd` calls by placing a task on the queue for `relayd` to run through
`drush aegir:log`.

### `aegir:exitcode`

The [drush
aegir:exitcode](https://gitlab.com/aegir/aegir/-/blob/main/profile/modules/aegir_operation/src/Command/AegirExitCodeCommand.php)
command is the mechanism for the backend to return a final status code for an
Operation it has run. This is a simple numeric code, typically as returned by
the shell command which was run, to indicate whether the Operation succeeded or
not.

The corresponding [relayd
aegir_exitcode](https://gitlab.com/aegir/aegir/-/blob/main/backend/relayd.py#L59)
task is called from `dispatcherd` by placing a task on the queue for `relayd`
to in turn pass to `drush aegir:exitcode`.
