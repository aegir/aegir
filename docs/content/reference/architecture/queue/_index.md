---
title: Queue System
weight: 20
---

## Queue system architecture


The Aegir5 queue is implemented using
[Celery](https://docs.celeryproject.org/en/stable/getting-started/introduction.html),
which is a full-featured task queue written in Python and built atop
[RabbitMQ](https://www.rabbitmq.com/).

The queue system thus consists of the RabbitMQ service on top of which
we have 2 Celery workers:

* [`relayd`](https://gitlab.com/aegir/aegir/-/blob/main/backend/relayd.py)
  runs on the front-end server, and serves primarily to relay data back into
  the front-end Drupal system about backend Operations and Tasks (status, log data, etc).
* [`dispatcherd`](https://gitlab.com/aegir/aegir/-/blob/main/backend/dispatcherd.py)
  runs on the back-end server, and serves primarily to dispatch Tasks to a
  backend plugin (currently Ansible, but eventually
  [anything](https://gitlab.com/aegir/aegir/-/issues/52)), to actually
  provision something (Platform, Site, Server, Service), or *do* something with
  the provisioned resources (run a backup, perform updates, etc).

![](/images/aegir5_queue_architecture.svg)


## 2 Workers, 2 Queue exchanges

Celery uses the concept of different queues or exchanges (what we're thinking
of as "channels" on top of the underlying "bus"), that use AMQP routing to get
tasks to the correct worker. Workers in turn specify which queue or queues they
want to listen to when they start up, and we specify which queue to put things
on when we post a task.

Worker applications seem to need to be able to handle any task that gets
put on the queue they're listening on, and if a task comes in for a worker that
doesn't have a method to handle it, things fail badly.

As such, we've refactored the AbstractTaskQueue and related classes to take an
"exchange" argument, and similarly configured `dispatcherd` and `relayd` to
specify a particular queue/exchange/channel when they start up. This needs to
be better documented, and we probably need to understand the Celery, RabbitMQ,
and AMQP pieces here, or at least point our docs to the relevant docs for those
tools.

See commits
[d05e9bf](https://gitlab.com/aegir/aegir/-/commit/d05e9bf4d067ca68a35202a9c85323c8cf0942e5),
[1fee690](https://gitlab.com/aegir/aegir/-/commit/1fee6905e75101178ae8c272b49eb3adc6ef3153),
[2ed8c72](https://gitlab.com/aegir/aegir/-/commit/2ed8c72af1d73f352d58f137770adad6cfadfa0d)
for the related changes here.

