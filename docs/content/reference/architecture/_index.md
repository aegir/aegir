---
title: Architecture
weight: 1

---

## Summary

Fundamentally, Aegir5 maps a web GUI to CLI commands. It is made up of several components:

1. There is a **[front-end](frontend)** built on Drupal 8. This is the **default Admin UI** and **authoritative Object Model** for the sytem.
1. There is a **[queue system](queue)** based on a RabbitMQ/Celery task queue to **dispatch tasks**.
1. There is a **[back-end](backend)** which consumes queued tasks and uses the data to execute Operations by rendering templates and running commands.
1. The Drupal **REST API** enables any number of **alternate frontends** to manipulate the **Object Model** and trigger **Operations**.

The **front-end** UI allows users to construct the Object Model, creating
Projects, Applications and Deployments to represent the applications and
instances they need to provision, host, and manage.  Users can interact with
Projects and Applications by running Operations, composed of Tasks, which pass
configuration variables into a distributed task queue, built on
[Celery](https://docs.celeryproject.org).

The **back-end**  is intended to be pluggable, and we have the basics of an
Ansible-based backend in place, as well as a standalone Kubernetes-based
backend which we intend to incorporate.

![Architecture diagram](/images/Aegir5-Architecture-Diagram.svg)

This diagram created with [yEd Live](https://www.yworks.com/yed-live/), [source GraphML](/images/Aegir5-Architecture-Diagram.graphml) file can be imported
there, or edited in the [yEd Desktop](https://www.yworks.com/products/yed) app.

The **queue system** Celery worker (`dispatcherd`) receives these tasks, and
runs provisioner commands, based on the variables passed into the task from the
front-end. Currently the only backend provisioner is Ansible, but the system is
designed to support pluggable provisioners such as Terraform, Kubernetes, or
plain shell scripts.

![Queue Architecture](/images/aegir5_queue_architecture.svg)

In previous editions of Aegir, we sometimes saw the split-brain problem arise
as a result of the Frontend and Backend sharing responsibility for the state of
the system.

In Aegir5, the Frontend is intended to be canonical with regards to the data
about the state of a running application. Aegir manages the Environment,
consisting of the application state (files + database).

The Project incorporates the infrastructure Kubernetes resources required to
host the application. Drumkit provides commands to initialize a project with
such resource definitions.

{{< children >}}
