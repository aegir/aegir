---
title: Front-end
weight: 10
---


## Front-end

The front-end is written in Drupal 8. It consists of some base entities and traits, along with basic admin interfaces for creating and managing fields and bundles.

Beyond the low-level [Operations and Tasks](../operations-tasks) which form the
building blocks for the user interface in Aegir5, there are a set of
higher-level entities in the front-end UI:

### Deployment Targets (Clusters, VMs)

In a traditional Aegir3 model, we have Servers representing a location where
sites can be deployed into. In a Kubernetes context, this would be a cluster.
Deployment Targets are the frontend concept that encompasses both of these.

We intend to support provisioning of Deployment Targets in the future. For the
time-being, we will simply be pointing at existing Deployment Targets to which
we will want to deploy our applications.

In [#163: Create a Deployment Target](https://gitlab.com/aegir/aegir/-/issues/163), we introduced the
Deployment Target as an entity type, leveraging [Typed
Entity](https://www.drupal.org/project/typed_entity) as a way to provide custom
business logic wrapping the entity itself. This diverges from the older custom
entities in the code base, but once it's proven itself should allow us to
refactor and shed a lot of boilerplate Entity API code across the codebase.

### Projects (codebase)

Projects represent a codebase at a more abstract level. Whereas Releases (see
below) are fully instantiated codebases, Projects represent the source of the
codebase itself; usually a Git repository.

### Deployments (site instances)

Deployments become the equivalent of Sites in Aegir 3. They capture the
specific configuration required to run an instance of a specific Release of the
Project (or Application), along with the storage required for both the database
and any uploaded files.

On a VM Deployment Target, a Deployment is literally an installed Drupal (or
other web app) with all its storage, database, and site content ready to serve.

In a Kubernetes context, a Deployment would consist of an Environment
(namespace + storage), combined with an Deployment resource (ingress, drupal
pod, etc).

### Releases (image)

Releases are basically the equivalent of Platforms in Aegir 3, representing a
Project (or Application, in future) at a particular point in time or version.
They capture the runtime environment, with a specific version of our codebase
deployed and all dependencies downloaded. In Kubernetes, these are basically
container images, whereas in a VM deployment target it represents a specific
commit/tag of Project, checked out and with all vendor dependencies built (eg.
`composer install`).

### Applications (distribution)

Applications represent a pre-packaged or upstream "distribution" of an
application with a versioned release process. These will extend the concept of
a Project and will be introduced in a future release.

### A note about multisite

Sharing a common codebase across multiple sites can be achieved by leveraging
the same Release. However, unlike Platforms from Aegir3, we are not intending
to support Drupal’s multisite capability in Aegir5.

Multisite adds a lot of complexity, especially when running in a containerized
environment. It also introduces multiple security and stability concerns.

