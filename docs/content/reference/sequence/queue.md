---
title: Queue Sequence
weight: 30
---

This is a more detailed sequence illustrating the interaction between `relayd`
and `dispatcherd` workers.

{{<mermaid>}}
sequenceDiagram
    participant F as frontend (Drupal)
    participant R as relayd (Celery)
    participant M as Message Queue (RabbitMQ)
    participant D as dispatcherd (Celery)
    participant B as backend (Ansible)
    activate M
    activate F
    Note over F: Put task(s) on dispatcherd Queue
    F->>D: Run Operation (Admin UI)
    activate D
    deactivate F
    activate B
    D->>B: Execute provisioner (run_cmd)
    Note over B: Currently this is Ansible, but might be any executor
    loop Until provisioner exits
        D->>B: Poll for output
        B-->>D: Return latest output
        activate R
        D->>R: Post log task for relayd (post_log)
        activate F
        R->>F: Send output to Operation log (aegir:log)
        deactivate R
        deactivate F
        opt If needed
            activate R
            B->>D: Return feedback data (eg. backup location)
            D->>R: Post input task for relayd (relay_aegir_input)
            activate F
            R->>F: Input data back to Operations/Tasks (aegir:input)
            deactivate R
            deactivate F
        end
    end
    B->>D: Return exit code
    activate R
    D->>R: Post exitcode task for relayd (post_exitcode)
    activate F
    Note over F: This triggers an update to Operation status.
    R->>F: Send exitcode for Operation (aegir:exitcode)
    deactivate R
    deactivate F
    deactivate D
    deactivate B
    deactivate M
{{< /mermaid>}}
