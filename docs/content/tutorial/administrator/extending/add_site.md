---
title: Add a new site type
weight: 10

---

First, we'll need a Hugo "site type". This will define what operations you can run to interact with the site in Ægir. So, let's create a site type for Hugo:

1. Go to `Ægir > Sites > Site Types`.
1. Click "Add Ægir site type".
1. Provide a label (e.g. "Hugo").

We'll want to version-control the configuration for our new site type. So next, let's export the Drupal code using the Features module.

