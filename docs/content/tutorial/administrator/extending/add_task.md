---
title: Add a task
weight: 30

---

Deploying a Hugo site basically involves cloning the codebase, running Hugo to compile static code, then pointing a vhost at it. Tasks in Ægir are the procedures used to accomplish an operation. These procedures are made up of tasks. While you can define the scope of a task however you like, we recommend keeping them as small as is reasonable. This allows for easier re-use. Multiple tasks are combined in a specific order to form an operation.

Let's start with deploying the codebase, by creating a new "Clone Git repository" task type.

1. Go to `Ægir > Tasks > Task Types`.
1. Click "Add Ægir task type".
1. Provide a label (e.g. "Clone Git repository").

To clone a Git repo, we'll need (at a minimum) both the repository URL, and the destination directory. So let's add fields to record these values. First, the repo URL:

1. Next to the new "Clone Git repository" task type, on the "Task types" page, select "Manage fields".
1. Click "Add field".
1. Under "Add a new field", select "General > Link".
1. Provide a label (e.g. "Git repository URL").
1. Click "Save and continue".
1. Leave "Allowed number of values" at "Limited -- 1".
1. Click "Save field settings"
1. (Optional) Provide some help text.
1. Check "Required field", as this task will fail if no value is provided.
1. (Optional) Add a default value. [dev note: this can be very handy for testing. Add a URL to the Git repo of an existing Hugo site (`https://gitlab.com/consensus.enterprises/drumkit`). It can be updated or removed later.]
1. Set "Allowed link type" to "External links only".
1. Set "Allow link text" to "Disabled".
1. Click "Save settings".

Now let's provide the destination path to which we'll clone the repo:

1. Click "Add field".
1. Under "Add a new field", select "Text (plain)".
1. Provide a label (e.g. "Destination path").
1. Click "Save and continue".
1. Leave the default field settings.
1. Click "Save field settings"
1. (Optional) Provide some help text.
1. Check "Required field", as this task will fail if no value is provided.
1. (Optional) Add a default value. [dev note: this can be very handy for testing. Add a default path (`/var/aegir/sites/hugo`). It can be updated or removed later.]
1. Click "Save settings".

That should be all that's needed for this task. Later, we can add options, such as a "Clone recursively" checkbox. For now, we'll hard-code that in the backend.

Next, let's add a task to run `hugo`:

1. Go to `Ægir > Tasks > Task Types`.
1. Click "Add Ægir task type".
1. Provide a label (e.g. "Run hugo").

Not all tasks need parameters. That's the case here, for our minimalist example. Later, we can add options, such as allowing Hugo to look in the `docs` directory for the site codebase (as opposed to the root of the repo). For now, we'll hard-code that in the backend too.

