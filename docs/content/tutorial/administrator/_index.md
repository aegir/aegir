---
title: "Aegir Administrator: Getting Started"
menutitle: Aegir Administrator
weight: 20
---

If you are an Administrator of an Aegir system, responsible for creating Site
types for hosted applications, as well as Operations and Tasks associated with
managing them, start here.

{{< children >}}
