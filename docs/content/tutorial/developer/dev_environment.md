---
title: Spin up your Development Environment
menuTitle: "Dev environment"
weight: 10
---


## Getting started

### 1. Install DDEV

The Ægir repo includes configuration files for spinning up a local containerized system using [ddev](https://ddev.readthedocs.io/en/stable/), which makes it very easy to get started.


DDEV installation depends on your development machine OS. You can get the [current instructions](https://ddev.readthedocs.io/en/stable/#docker-installation) at the DDEV documentation site.

### 2. Clone the repository

To get started, clone this repository locally.

```sh
git clone --recursive https://gitlab.com/aegir/aegir.git
cd aegir
```


### 3. Spin up a local instance


1. Bootstrap [Drumkit](http://drumk.it/).
    * `source d`
1. Start containers, build the Aegir codebase, install, enable dev tools, and start the Celery queue workers.
    * `make start build dev workers`
1. With that done, visit [http://aegir.ddev.site](http://aegir.ddev.site), and login using:
    * **Username**: `dev`
    * **Password**: `pwd`

### Check the queues to verify that all components are running

Go to http://aegir.ddev.site/admin/aegir/queue

There are two buttons on this page for checking the status of the queues... Click `Check connection settings` and you should get a green splash at the top of the page that starts `Connection to dispatcherd worker succeeded`

Click on `Check task queue` to ensure the end-to-end connection is working. 

Note: As of late 2021, this test can take a long time to run (up to a minute) on MacOS. The solution is to use NFS to mount the project into the web container, as described in the [performance section of the DDEV site](https://ddev.readthedocs.io/en/latest/users/performance/#macos-nfs-setup). 


### Project Dependencies

The various components of Ægir are written in different languages and frameworks, which entails different methods for managing dependencies and requirements. Here are the most notable mechanisms we use to manage these:

* `build/ansible/requirements.yml`: Ansible roles.
* `build/ansible/roles/aegir.aegir/meta/main.yml`: Ansible dependencies.
* `profile/composer.json`: Drupal profile PHP dependencies.
* `profile/aegir.info.yml`: Drupal dependencies.
* `build/ansible/roles/aegir.aegir/files/platform_build.j2`: Template to generate a composer.json that will build a Drupal platform for Aegir. Includes `drush`, `behat`, `phpcs`, etc.
* `make ansible`: Standardized method for installing Ansible (via Drumkit).
