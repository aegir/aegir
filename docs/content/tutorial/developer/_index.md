---
title: "Ægir Developer: Getting Started"
menuTitle: "Ægir Developer"
date: 2018-01-29T02:58:11-05:00
weight: 30
---

Ægir is built on Ansible and Drupal 8. At this point, it consists mostly of a custom Ansible role, and Drupal modules exported using Features.

The Ægir repo also includes configuration files for spinning up a local containerized system using [ddev](https://ddev.readthedocs.io/en/stable/), which makes it very easy to get started.


### Next Steps
{{< children >}}
