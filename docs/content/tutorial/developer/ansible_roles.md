---
title: HOWTO write Ansible "task" roles
menutitle: Ansible roles

---


@TODO: fill in this page with details about how to write Ansible roles to provide the implementation of a front-end Operation.

@TODO: eventually we're aiming to move to consolidate roles that are not per-Task, so let's bear that in mind :)


## Example


See: https://gitlab.com/aegir/aegir/-/blob/main/profile/modules/examples/aegir_example_site/roles/example_print_debug_data/tasks/main.yml

```
---

- name: Print current debug data.
  debug:
    var: field_example_site_debug_data

- name: Log static field data
  aegir_log:
    data:
      log_output: "STATIC LOG DATA"
      log_timestamp: "123"
      log_sequence: "321"

- name: Log variable site data
  aegir_log:
    data:
      log_output: "Log data: {{ field_example_site_debug_data }}"
      log_timestamp: "124"
      log_sequence: "322"
```
