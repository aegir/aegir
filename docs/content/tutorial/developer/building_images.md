---
title: Build container images
weight: 50
---

## Build container images

The [build/ directory](https://gitlab.com/aegir/aegir/-/tree/main/build) contains the Ægir Docker image build files. These are used to build container images on which we base our localdev environment, but also (via common Ansible roles) environments in which production Aegir instances run.

## Set up

Docker and DDEV are required; [install them if you've not already done
so](https://ddev.readthedocs.io/en/stable/users/install/).

To begin building these, you'll need to install a few things via
[Drumkit](https://drumk.it/):

```shell
. d # If Drumkit is not already bootstrapped.
make ansible-playbook ansible-galaxy packer packer-plugins
```

## Local registry

You can run a private Docker registry on your local machine to iterate quicker
and avoid pushing potentially broken images to the remote registry by running:

```shell
make start-local-registry
```

This will also configure DDEV to use the local registry instead of the remote
one. To remove the local registry and revert DDEV back to pulling from the
remote, run:

```shell
make remove-local-registry
```

Note that this will destroy the local registry including the images in it. To
stop it without destroying, run:

```shell
make stop-local-registry
```

## Building images

It's as easy as:

```shell
make ci-images
```

This will push the images to the local registry if that's active, and only
attempts to push to the remote if the local registry isn't active. If you need
to build just one or two images, you can provide a space-separated list as the
`DOCKER_IMAGES` parameter:

```shell
make ci-images DOCKER_IMAGES='docker/image1 docker/image2'
```

The full list of images can be found in
[`drumkit/mk.d/20_ci.mk`](../drumkit/mk.d/20_ci.mk).
