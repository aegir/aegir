---
title: "HOWTO: Check Aegir Queue system"
menutitle: Check Queue system
weight: 10
---

To manually check the underlying Queue System is running properly:

1. Login to your Aegir5 site https://aegir.ddev.site/user/login
2. Go to Ægir > Task queue (`/admin/aegir/queue`)
3. Click `Check connection settings` - this will verify your credentials are valid and `relayd` is responding.
4. Click `Check task queue` - this will verify that `dispatcherd` on the backend is responding, and a round-trip "echo" returns.

If something goes wrong, both of these buttons will set messages to help you identify where to look next to troubleshoot.
