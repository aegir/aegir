---
title: "Aegir User: Getting Started"
menutitle: Aegir User
weight: 10
---

If you are an end-user of an Aegir system, responsible for managing the
lifecycle of applications hosted by Aegir, start here.

{{< children >}}
