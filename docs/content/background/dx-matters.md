---
title: Developer Experience (DX) Matters
menutitle: DX Matters
weight: 10
---

One of the overarching design goals of Aegir5 is to enable a simpler developer
experience than has been possible in Aegir3. We want to enable someone who
wants to deploy Wordpress on Aegir to do so with minimal knowledge of its
internals. One aspect of this is making clear boundaries in the form of APIs
and a protocol definition for how the different components can interact.

## Which Developers?

We see 3 distinct levels of "developer" that may interact with Aegir, and we
aim to allow each of these roles to operate in a reasonably contained fashion:

### 0. Site Builder

  * Create new sites
  * Manage site workflows: backup/restore, update, migrate, clone, etc.

### 1. System Administrator / DevOps / Site Reliability Engineer (SRE)

  * Pulling custom tasks along with their entities and fields together into operations.
  * Custom tasks might run an Ansible playbook, a Terraform apply command, or similar, in order to achieve a certain coherent provisioning component (e.g. a server, a service, a codebase, a database, or a site).
  * Can configure Drupal's robust Entity system, via its front end, to build out desired Operations (a collection of Tasks) to coordinate provisioning and management of Servers, Services, Codebases, and Types.

### 2. Backend Developer

  * Ordinarly there would be a "reference implementation", a standard way to do X (e.g. deploy Drupal 8+, Wordpress, Hugo).
  * Admin can take that and extend it, to customize for their use case, incorporating other custom tasks:
    * Add a Redis or Solr service into the mix.
    * Require a Terraform aspect.
    * Incorporate S3 buckets for storage of the sites being hosted.
    * Enable a HTTPS certificate (LE, custom, self-signed).
  * Developer should be able to use an Ansible module (or similar) to ignore details of the communications protocol between the front and back ends internally, and just "send this data back to there, for this UUID" type of thing.

### 3. Maintainer

  * Developers of the Aegir system itself.
  * Ideally, the "plumbing" that we are responsible for becomes a minimal system to drive "the protocol" along with a Drupal application coordinating the operation, provisioning, and lifecycle management of the provisioned components.
  * Ultimately, this should give rise to alternative "client" or front-end implementations, with different focal points or feature sets. An Aegir set-up might be used in production and thus require ironclad blue-green deployment and locked down access to the environment, or it might be used to facilitate web application development workflows, spinning up per-branch environments automatically on the basis of whatever conditional trigger we can imagine. 😃

