---
title: Background information
weight: 40
---

This section contains background information and discussion on the technical
architecture behind Aegir.

{{< children >}}

