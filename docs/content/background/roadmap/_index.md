---
title: Aegir5 Roadmap
menutitle: Roadmap
weight: 5

---

## Aegir5 is **pre-alpha** softare

The software is in **pre-alpha** development stage. Having prototyped the [basic
architectural pieces](/reference/architecture) to prove its utility, we are
focused on documenting and consolidating things, so that we can better enable
the Aegir community to participate in the project.

### Community feedback

We are currently working on elaborating Features in terms of User Stories and
broken up into themed [**Releases**](https://docs.aegir.hosting/background/roadmap/releases/).
Together, these describe the core workflows needed in an MVP Aegir5 release.
The sub-epics and issues describe the core behaviour as we understand it. We
are actively seeking [**feedback** directly on these RFC
issues](https://gitlab.com/groups/aegir/-/epics?state=opened&page=1&sort=TITLE_ASC&label_name[]=RFC)
, to validate:

1. We have described a coherent and reasonable set of behaviours for the system
2. What are the priorities of the community, to evaluate what to work on first

## Guiding principles

* Aegir5 encodes "best practices" for applications lifecycle management in a modern software development world.
* Aegir5 supports both the Dev and Ops sides of the Dev/Ops framework with flexibility to define how linked the two are.
* Aegir5 is straightforward to customize and extend without needing extensive knowledge of Aegir internals.

## Developer participation

Aegir 5 is in open development at https://gitlab.com/aegir/aegir. This roadmap
will be regularly updated as we iterate, to reflect current progress and status.

We are currently engaged in an agile [estimation &
planning](https://consensus.enterprises/blog/aegir5-roadmap-update/) cycle, in
an effort to reboot Aegir5 development in the open.

## Historical Trajectory

* [More details on how we got here](/background/roadmap/history)
