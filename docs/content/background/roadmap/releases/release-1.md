---
title: Release 1
weight: 10
---

Theme: Pluggable backends (supporting different [Deployment Targets](../../concepts/#deployment-target)) with stateless [Projects](../../concepts#project)

[Release 1: Pluggable backends with stateless Projects](https://gitlab.com/groups/aegir/-/epics/1) ([milestone](https://gitlab.com/groups/aegir/-/milestones/1))

* [Bootstrap Aegir5](https://gitlab.com/aegir/aegir/-/issues/141)
* [Make a VM Deployment Target available](https://gitlab.com/groups/aegir/-/epics/6)
* [Host a static html site to a VM Deployment Target](https://gitlab.com/groups/aegir/-/epics/5)
* [Make a K8S Deployment Target available](https://gitlab.com/groups/aegir/-/epics/8)
* [Host a static html site on a K8S Deployment Target](https://gitlab.com/groups/aegir/-/epics/7)
