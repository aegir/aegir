---
title: Releases
weight: 9
---

Much of the below is in flux, as we try to re-orient ourselves to a top-level view of Aegir5 from a User Workflows standpoint.

The goal of the Releases and User Stories fleshed out below are to become RFC-style documents
(a set of pages to be patched into docs.aegir.hosting, to form the basis for
our Release Planning), but your feedback is welcome at any time!

* See the [RFC Epics](https://gitlab.com/groups/aegir/-/epics?state=opened&page=1&sort=TITLE_ASC&label_name[]=RFC) for details about the planned feature set we're developing.

The stories listed below and epics/issues in Gitlab attempt to use a common
language in terms of [**Personas**](../personas), and associated systems roles (work in progress).

## Releases

* [**Release 1**](release-1) (Pluggable backends with stateless Projects)
* [**Release 2**](release-2) (Stateful Project support (Drupal))
* [**Release 3**](release-3) (Project lifecycle support)
* [**Release 4**](release-4) (Application workflows)
* [**Future Release**](future) (Infrastructure provisioning)

**See also:** Our open issue regarding [naming
Aegir5](https://gitlab.com/aegir/aegir/-/issues/139) as well as this one about
([categorizing Aegir5](https://gitlab.com/aegir/aegir/-/issues/11)).
