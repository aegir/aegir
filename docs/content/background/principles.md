---
title: Design Principles

---

How we got here
---------------

As of this writing, Aegir has been around for 11.5 years (!), which is pretty ancient in the software world. It was originally targeting Drupal 5, and ran on PHP 4. Drush was in its infancy and the plethora of configuration management tools we see today did not exist.

Challenges inherent in this architecture:

* **[Split brain problem](https://en.wikipedia.org/wiki/Split-brain_(computing))**: We store data in both the front-end and back-end, but neither is considered entirely canonical.
* **Reliance on [Drush](www.drush.org) as a framework**: Don't get me wrong: Drush is great. However, there are times when we appear to have bootstrapped two sites simultaneously, leading to all sorts of shenanigans.
* **Overly complex core mechanics** (eg. [`d()`](https://cgit.drupalcode.org/provision/tree/provision.context.inc)): PHP 4's object-oriented features were... limited. To overcome that, we basically invented a dependency injection system, among other things.
* **Enormous boilerplate required for frontend-backend communication**: Take a look at the Aegir3 example modules. It takes several hundred (!) lines of code to store data from the front-end in the back-end.

Additional challenges built up over time as a result, including:

* **Not-invented-here syndrome**: We do all the heavy lifting (eg. writing vhosts) ourselves. Among other things, this is because dedicated FLOSS config management tools weren't really a thing back then.
* **Technical debt**: The Drupal upgrade cycle is such that we're still managing our own field database tables.
* **Lack of a robust test suite**: Testing is hard. Adding tests to a mature, sprawling system is *really* hard. Unfortunately, this has held us back from more aggressively addressing some of the other challenges listed above.

As a result, some very interesting feature requests weren't feasible without major changes, including:

* Provisioning infrastructure and services
* Deploying and managing platforms other than Drupal
* ...

Another fundamental challenge that Aegir faces is that it tries to serve two constituencies simultaneously, namely sysadmins and developers. The former appreciate the reliability and security-conscious approach that Aegir takes. The latter most appreciate the efficiency and extensibility it offers. These competing interests can lead to design trade-offs that can leave everyone frustrated.

However, Aegir has largely been developed by sysdamins, for sysadmins. As a result, useability hasn't been a priority. One glaring example is the Aegir Drupal update/upgrade process. You won't find a button labelled "update" or "upgrade" anywhere in Aegir core (though it exists as a [contrib module](https://www.drupal.org/project/hosting_distribution)). Instead, we use "migrate", which describes the actual process of upgrading a Drupal site, but is entirely un-intuitive.

Enough complaining already!
---------------------------

That all being said, Aegir does some things really well:

* Aegir distinguishes between important levels of abstraction, such as sites vs. platforms.
* Blue-green deployments ensure stable upgrades.
* Backups are tested regularly.
* Automatic roll-backs clean up cruft from failed operations.
* Logs of all operations are available for easy debugging.
* SSH is the primary method of communicating with remote servers.
* and much more.

Where to go from here
---------------------

Aegir5 is meant to be a thorough re-write in order to address some of these fundamental challenges, and to better leverage modern components.
