Welcome to Ægir
===============

[![pipeline status](https://gitlab.com/aegir/aegir/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/aegir/aegir/pipelines) [![coverage report](https://gitlab.com/aegir/aegir/badges/main/coverage.svg)](http://docs.aegir.hosting/coverage/index.html)

Ægir is a framework for hosting and managing websites and other applications.

Ægir is built as a Drupal distribution, making it both easy to self-host, integrate with other tools and services, and extend with plugins, themes or custom code.

Check out [http://aegir.hosting/](http://aegir.hosting/) for more information. Professional support, training, consulting and other services are offered by the [Ægir Cooperative](http://aegir.coop).

Technical documentation can be found at [http://docs.aegir.hosting](http://docs.aegir.hosting). This documentation is also available in the `docs/` directory.


Getting Started
---------------

Jump directly to the [Development Environment](https://docs.aegir.hosting/getting-started/developer/dev_environment/) section in the documentation (mentioned above).

1. Clone the repository:

```shell
git clone --recursive https://gitlab.com/aegir/aegir.git
cd aegir
```

2. Spin up your local environment:

```shell
. d # If Drumkit is not already bootstrapped.
make start build dev workers
```

3. Visit your aegir environment: https://aegir.ddev.site

Username: dev
Password: pwd


More details here: https://docs.aegir.hosting/getting-started/developer/dev_environment/

## Testing

```shell
make install tests
```
